module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "../next-server/lib/utils":
/*!*****************************************************!*\
  !*** external "next/dist/next-server/lib/utils.js" ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/dist/next-server/lib/utils.js");

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/interopRequireDefault.js":
/*!**********************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/interopRequireDefault.js ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

module.exports = _interopRequireDefault;

/***/ }),

/***/ "./node_modules/next/app.js":
/*!**********************************!*\
  !*** ./node_modules/next/app.js ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./dist/pages/_app */ "./node_modules/next/dist/pages/_app.js")


/***/ }),

/***/ "./node_modules/next/dist/pages/_app.js":
/*!**********************************************!*\
  !*** ./node_modules/next/dist/pages/_app.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(/*! @babel/runtime/helpers/interopRequireDefault */ "./node_modules/@babel/runtime/helpers/interopRequireDefault.js");

exports.__esModule = true;
exports.Container = Container;
exports.createUrl = createUrl;
exports.default = void 0;

var _react = _interopRequireDefault(__webpack_require__(/*! react */ "react"));

var _utils = __webpack_require__(/*! ../next-server/lib/utils */ "../next-server/lib/utils");

exports.AppInitialProps = _utils.AppInitialProps;
exports.NextWebVitalsMetric = _utils.NextWebVitalsMetric;
/**
* `App` component is used for initialize of pages. It allows for overwriting and full control of the `page` initialization.
* This allows for keeping state between navigation, custom error handling, injecting additional data.
*/

async function appGetInitialProps({
  Component,
  ctx
}) {
  const pageProps = await (0, _utils.loadGetInitialProps)(Component, ctx);
  return {
    pageProps
  };
}

class App extends _react.default.Component {
  // Kept here for backwards compatibility.
  // When someone ended App they could call `super.componentDidCatch`.
  // @deprecated This method is no longer needed. Errors are caught at the top level
  componentDidCatch(error, _errorInfo) {
    throw error;
  }

  render() {
    const {
      router,
      Component,
      pageProps,
      __N_SSG,
      __N_SSP
    } = this.props;
    return /*#__PURE__*/_react.default.createElement(Component, Object.assign({}, pageProps, // we don't add the legacy URL prop if it's using non-legacy
    // methods like getStaticProps and getServerSideProps
    !(__N_SSG || __N_SSP) ? {
      url: createUrl(router)
    } : {}));
  }

}

exports.default = App;
App.origGetInitialProps = appGetInitialProps;
App.getInitialProps = appGetInitialProps;
let warnContainer;
let warnUrl;

if (true) {
  warnContainer = (0, _utils.execOnce)(() => {
    console.warn(`Warning: the \`Container\` in \`_app\` has been deprecated and should be removed. https://err.sh/vercel/next.js/app-container-deprecated`);
  });
  warnUrl = (0, _utils.execOnce)(() => {
    console.error(`Warning: the 'url' property is deprecated. https://err.sh/vercel/next.js/url-deprecated`);
  });
} // @deprecated noop for now until removal


function Container(p) {
  if (true) warnContainer();
  return p.children;
}

function createUrl(router) {
  // This is to make sure we don't references the router object at call time
  const {
    pathname,
    asPath,
    query
  } = router;
  return {
    get query() {
      if (true) warnUrl();
      return query;
    },

    get pathname() {
      if (true) warnUrl();
      return pathname;
    },

    get asPath() {
      if (true) warnUrl();
      return asPath;
    },

    back: () => {
      if (true) warnUrl();
      router.back();
    },
    push: (url, as) => {
      if (true) warnUrl();
      return router.push(url, as);
    },
    pushTo: (href, as) => {
      if (true) warnUrl();
      const pushRoute = as ? href : '';
      const pushUrl = as || href;
      return router.push(pushRoute, pushUrl);
    },
    replace: (url, as) => {
      if (true) warnUrl();
      return router.replace(url, as);
    },
    replaceTo: (href, as) => {
      if (true) warnUrl();
      const replaceRoute = as ? href : '';
      const replaceUrl = as || href;
      return router.replace(replaceRoute, replaceUrl);
    }
  };
}

/***/ }),

/***/ "./pages/_app.js":
/*!***********************!*\
  !*** ./pages/_app.js ***!
  \***********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/app */ "./node_modules/next/app.js");
/* harmony import */ var next_app__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_app__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_redux_wrapper__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! next-redux-wrapper */ "next-redux-wrapper");
/* harmony import */ var next_redux_wrapper__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_redux_wrapper__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! redux */ "redux");
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(redux__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var redux_thunk__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! redux-thunk */ "redux-thunk");
/* harmony import */ var redux_thunk__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(redux_thunk__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _store_reducers_TodoAppReducer__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../store/reducers/TodoAppReducer */ "./store/reducers/TodoAppReducer.js");

var _jsxFileName = "D:\\PERSONAL\\Vanita\\React\\test\\todo-next\\pages\\_app.js";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }








const composeEnhancers = redux__WEBPACK_IMPORTED_MODULE_5__["compose"];
const rootReducer = Object(redux__WEBPACK_IMPORTED_MODULE_5__["combineReducers"])({
  todoTask: _store_reducers_TodoAppReducer__WEBPACK_IMPORTED_MODULE_7__["default"]
});
const store = Object(redux__WEBPACK_IMPORTED_MODULE_5__["createStore"])(rootReducer, composeEnhancers(Object(redux__WEBPACK_IMPORTED_MODULE_5__["applyMiddleware"])(redux_thunk__WEBPACK_IMPORTED_MODULE_6___default.a)));

class MyApp extends next_app__WEBPACK_IMPORTED_MODULE_1___default.a {
  static async getInitialProps({
    Component,
    ctx
  }) {
    const pageProps = Component.getInitialProps ? await Component.getInitialProps(ctx) : {}; //Anything returned here can be accessed by the client

    return {
      pageProps: pageProps
    };
  }

  render() {
    //Information that was returned  from 'getInitialProps' are stored in the props i.e. pageProps
    const {
      Component,
      pageProps
    } = this.props;
    return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_redux__WEBPACK_IMPORTED_MODULE_2__["Provider"], {
      store: store,
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(Component, _objectSpread({}, pageProps), void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 36,
        columnNumber: 17
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 35,
      columnNumber: 13
    }, this);
  }

}

/* harmony default export */ __webpack_exports__["default"] = (MyApp);

/***/ }),

/***/ "./store/actions/actionTypes.js":
/*!**************************************!*\
  !*** ./store/actions/actionTypes.js ***!
  \**************************************/
/*! exports provided: ADD_TASK, ADD_TASK_START, ADD_TASK_FAILED, ADD_TASK_SUCESS, FETCH_TASK, FETCH_TASK_FAILED, FETCH_TASK_SUCESS, DELETE_TASK, DELETE_TASK_FAILED, DELETE_TASK_SUCESS, EDIT_TASK, EDIT_TASK_FAILED, EDIT_TASK_SUCESS, ACTIVE_TASK, ACTIVE_TASK_FAILED, ACTIVE_TASK_SUCESS, CHANGE_STATUS_TASK, CHANGE_STATUS_TASK_FAILED, CHANGE_STATUS_TASK_SUCESS, FILTER_TASK, FILTER_TASK_FAILED, FILTER_TASK_SUCESS */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ADD_TASK", function() { return ADD_TASK; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ADD_TASK_START", function() { return ADD_TASK_START; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ADD_TASK_FAILED", function() { return ADD_TASK_FAILED; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ADD_TASK_SUCESS", function() { return ADD_TASK_SUCESS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FETCH_TASK", function() { return FETCH_TASK; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FETCH_TASK_FAILED", function() { return FETCH_TASK_FAILED; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FETCH_TASK_SUCESS", function() { return FETCH_TASK_SUCESS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DELETE_TASK", function() { return DELETE_TASK; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DELETE_TASK_FAILED", function() { return DELETE_TASK_FAILED; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DELETE_TASK_SUCESS", function() { return DELETE_TASK_SUCESS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EDIT_TASK", function() { return EDIT_TASK; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EDIT_TASK_FAILED", function() { return EDIT_TASK_FAILED; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EDIT_TASK_SUCESS", function() { return EDIT_TASK_SUCESS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ACTIVE_TASK", function() { return ACTIVE_TASK; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ACTIVE_TASK_FAILED", function() { return ACTIVE_TASK_FAILED; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ACTIVE_TASK_SUCESS", function() { return ACTIVE_TASK_SUCESS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CHANGE_STATUS_TASK", function() { return CHANGE_STATUS_TASK; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CHANGE_STATUS_TASK_FAILED", function() { return CHANGE_STATUS_TASK_FAILED; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CHANGE_STATUS_TASK_SUCESS", function() { return CHANGE_STATUS_TASK_SUCESS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FILTER_TASK", function() { return FILTER_TASK; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FILTER_TASK_FAILED", function() { return FILTER_TASK_FAILED; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FILTER_TASK_SUCESS", function() { return FILTER_TASK_SUCESS; });
const ADD_TASK = 'ADD_TASK';
const ADD_TASK_START = 'ADD_TASK_START';
const ADD_TASK_FAILED = 'ADD_TASK_FAILED';
const ADD_TASK_SUCESS = 'ADD_TASK_SUCESS';
const FETCH_TASK = 'FETCH_TASK';
const FETCH_TASK_FAILED = 'FETCH_TASK_FAILED';
const FETCH_TASK_SUCESS = 'FETCH_TASK_SUCESS';
const DELETE_TASK = 'DELETE_TASK';
const DELETE_TASK_FAILED = 'DELETE_TASK_FAILED';
const DELETE_TASK_SUCESS = 'DELETE_TASK_SUCESS';
const EDIT_TASK = 'EDIT_TASK';
const EDIT_TASK_FAILED = 'EDIT_TASK_FAILED';
const EDIT_TASK_SUCESS = 'EDIT_TASK_SUCESS';
const ACTIVE_TASK = 'ACTIVE_TASK';
const ACTIVE_TASK_FAILED = 'ACTIVE_TASK_FAILED';
const ACTIVE_TASK_SUCESS = 'ACTIVE_TASK_SUCESS';
const CHANGE_STATUS_TASK = 'CHANGE_STATUS_TASK';
const CHANGE_STATUS_TASK_FAILED = 'CHANGE_STATUS_TASK_FAILED';
const CHANGE_STATUS_TASK_SUCESS = 'CHANGE_STATUS_TASK_SUCESS';
const FILTER_TASK = 'FILTER_TASK';
const FILTER_TASK_FAILED = 'FILTER_TASK_FAILED';
const FILTER_TASK_SUCESS = 'FILTER_TASK_SUCESS';

/***/ }),

/***/ "./store/reducers/TodoAppReducer.js":
/*!******************************************!*\
  !*** ./store/reducers/TodoAppReducer.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _actions_actionTypes__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../actions/actionTypes */ "./store/actions/actionTypes.js");
/* harmony import */ var _utility__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../utility */ "./store/utility.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



const initialState = {
  data: [],
  name: '',
  date: '',
  id: 0,
  added: false,
  loading: false,
  error: false,
  edited: false
};

const taskInit = (state, action) => {
  return Object(_utility__WEBPACK_IMPORTED_MODULE_1__["updateObject"])(state, {
    loading: true
  });
};

const taskAdd = (state, action) => {
  return Object(_utility__WEBPACK_IMPORTED_MODULE_1__["updateObject"])(state, {
    loading: false
  });
};

const taskAddedSucces = (state, action) => {
  return Object(_utility__WEBPACK_IMPORTED_MODULE_1__["updateObject"])(state, {
    loading: false,
    added: true
  });
};

const taskAddedFalied = (state, action) => {
  return Object(_utility__WEBPACK_IMPORTED_MODULE_1__["updateObject"])(state, {
    error: true
  });
};

const fetchtaskStart = (state, action) => {
  return Object(_utility__WEBPACK_IMPORTED_MODULE_1__["updateObject"])(state, {
    loading: true
  });
};

const fetchtaskSuccess = (state, action) => {
  return Object(_utility__WEBPACK_IMPORTED_MODULE_1__["updateObject"])(state, {
    data: action.data,
    loading: false
  });
};

const fetchtaskFail = (state, action) => {
  return Object(_utility__WEBPACK_IMPORTED_MODULE_1__["updateObject"])(state, {
    error: true
  });
};

const reducer = (state = initialState, action) => {
  console.log(action);

  switch (action.type) {
    case _actions_actionTypes__WEBPACK_IMPORTED_MODULE_0__["ADD_TASK_START"]:
      return taskInit(state, action);

    case _actions_actionTypes__WEBPACK_IMPORTED_MODULE_0__["ADD_TASK"]:
      return taskAdd(state, action);

    case _actions_actionTypes__WEBPACK_IMPORTED_MODULE_0__["ADD_TASK_FAILED"]:
      return taskAddedFalied(state, action);

    case _actions_actionTypes__WEBPACK_IMPORTED_MODULE_0__["ADD_TASK_SUCESS"]:
      return taskAddedSucces(state, action);

    case _actions_actionTypes__WEBPACK_IMPORTED_MODULE_0__["FETCH_TASK"]:
      return fetchtaskStart(state, action);

    case _actions_actionTypes__WEBPACK_IMPORTED_MODULE_0__["FETCH_TASK_FAILED"]:
      return fetchtaskFail(state, action);

    case _actions_actionTypes__WEBPACK_IMPORTED_MODULE_0__["FETCH_TASK_SUCESS"]:
      return fetchtaskSuccess(state, action);

    case _actions_actionTypes__WEBPACK_IMPORTED_MODULE_0__["EDIT_TASK"]:
      return _objectSpread(_objectSpread({}, state), {}, {
        loading: true
      });

    case _actions_actionTypes__WEBPACK_IMPORTED_MODULE_0__["EDIT_TASK_FAILED"]:
      return _objectSpread(_objectSpread({}, state), {}, {
        loading: false,
        error: true
      });

    case _actions_actionTypes__WEBPACK_IMPORTED_MODULE_0__["EDIT_TASK_SUCESS"]:
      return _objectSpread(_objectSpread({}, state), {}, {
        name: action.name,
        date: action.date,
        id: action.id,
        loading: false,
        error: false,
        edited: true
      });

    case _actions_actionTypes__WEBPACK_IMPORTED_MODULE_0__["DELETE_TASK"]:
      return _objectSpread(_objectSpread({}, state), {}, {
        loading: true
      });

    case _actions_actionTypes__WEBPACK_IMPORTED_MODULE_0__["DELETE_TASK_FAILED"]:
      return _objectSpread(_objectSpread({}, state), {}, {
        loading: false,
        error: true
      });

    case _actions_actionTypes__WEBPACK_IMPORTED_MODULE_0__["DELETE_TASK_SUCESS"]:
      return _objectSpread(_objectSpread({}, state), {}, {
        loading: false,
        error: false
      });

    case _actions_actionTypes__WEBPACK_IMPORTED_MODULE_0__["CHANGE_STATUS_TASK"]:
      return _objectSpread(_objectSpread({}, state), {}, {
        loading: true
      });

    case _actions_actionTypes__WEBPACK_IMPORTED_MODULE_0__["CHANGE_STATUS_TASK_FAILED"]:
      return _objectSpread(_objectSpread({}, state), {}, {
        loading: false,
        error: true
      });

    case _actions_actionTypes__WEBPACK_IMPORTED_MODULE_0__["CHANGE_STATUS_TASK_SUCESS"]:
      return _objectSpread(_objectSpread({}, state), {}, {
        edited: true,
        loading: false,
        error: false
      });

    case _actions_actionTypes__WEBPACK_IMPORTED_MODULE_0__["FILTER_TASK"]:
      return _objectSpread(_objectSpread({}, state), {}, {
        loading: true
      });

    case _actions_actionTypes__WEBPACK_IMPORTED_MODULE_0__["FILTER_TASK_FAILED"]:
      return _objectSpread(_objectSpread({}, state), {}, {
        loading: false,
        error: true
      });

    case _actions_actionTypes__WEBPACK_IMPORTED_MODULE_0__["FILTER_TASK_SUCESS"]:
      return _objectSpread(_objectSpread({}, state), {}, {
        loading: false,
        error: false,
        data: action.data
      });

    default:
      return state;
  }
};

/* harmony default export */ __webpack_exports__["default"] = (reducer);

/***/ }),

/***/ "./store/utility.js":
/*!**************************!*\
  !*** ./store/utility.js ***!
  \**************************/
/*! exports provided: updateObject */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateObject", function() { return updateObject; });
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const updateObject = (oldObject, updatedProperties) => {
  return _objectSpread(_objectSpread({}, oldObject), updatedProperties);
};

/***/ }),

/***/ 0:
/*!****************************************!*\
  !*** multi private-next-pages/_app.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! private-next-pages/_app.js */"./pages/_app.js");


/***/ }),

/***/ "next-redux-wrapper":
/*!*************************************!*\
  !*** external "next-redux-wrapper" ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next-redux-wrapper");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-redux":
/*!******************************!*\
  !*** external "react-redux" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-redux");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react/jsx-dev-runtime");

/***/ }),

/***/ "redux":
/*!************************!*\
  !*** external "redux" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("redux");

/***/ }),

/***/ "redux-thunk":
/*!******************************!*\
  !*** external "redux-thunk" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("redux-thunk");

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwibmV4dC9kaXN0L25leHQtc2VydmVyL2xpYi91dGlscy5qc1wiIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9AYmFiZWwvcnVudGltZS9oZWxwZXJzL2ludGVyb3BSZXF1aXJlRGVmYXVsdC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbmV4dC9hcHAuanMiLCJ3ZWJwYWNrOi8vLy4uLy4uL3BhZ2VzL19hcHAudHN4Iiwid2VicGFjazovLy8uL3BhZ2VzL19hcHAuanMiLCJ3ZWJwYWNrOi8vLy4vc3RvcmUvYWN0aW9ucy9hY3Rpb25UeXBlcy5qcyIsIndlYnBhY2s6Ly8vLi9zdG9yZS9yZWR1Y2Vycy9Ub2RvQXBwUmVkdWNlci5qcyIsIndlYnBhY2s6Ly8vLi9zdG9yZS91dGlsaXR5LmpzIiwid2VicGFjazovLy9leHRlcm5hbCBcIm5leHQtcmVkdXgtd3JhcHBlclwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlYWN0XCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwicmVhY3QtcmVkdXhcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdC9qc3gtZGV2LXJ1bnRpbWVcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWR1eFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlZHV4LXRodW5rXCIiXSwibmFtZXMiOlsicGFnZVByb3BzIiwiUmVhY3QiLCJDb21wb25lbnQiLCJjb21wb25lbnREaWRDYXRjaCIsInJlbmRlciIsIl9fTl9TU0ciLCJ1cmwiLCJjcmVhdGVVcmwiLCJBcHAiLCJvcmlnR2V0SW5pdGlhbFByb3BzIiwiYXBwR2V0SW5pdGlhbFByb3BzIiwiZ2V0SW5pdGlhbFByb3BzIiwid2FybkNvbnRhaW5lciIsImNvbnNvbGUiLCJ3YXJuVXJsIiwicCIsImJhY2siLCJyb3V0ZXIiLCJwdXNoIiwicHVzaFRvIiwicHVzaFJvdXRlIiwiYXMiLCJwdXNoVXJsIiwicmVwbGFjZSIsInJlcGxhY2VUbyIsInJlcGxhY2VSb3V0ZSIsInJlcGxhY2VVcmwiLCJjb21wb3NlRW5oYW5jZXJzIiwiY29tcG9zZSIsInJvb3RSZWR1Y2VyIiwiY29tYmluZVJlZHVjZXJzIiwidG9kb1Rhc2siLCJUb2RvQXBwUmVkdWNlciIsInN0b3JlIiwiY3JlYXRlU3RvcmUiLCJhcHBseU1pZGRsZXdhcmUiLCJ0aHVuayIsIk15QXBwIiwiY3R4IiwicHJvcHMiLCJBRERfVEFTSyIsIkFERF9UQVNLX1NUQVJUIiwiQUREX1RBU0tfRkFJTEVEIiwiQUREX1RBU0tfU1VDRVNTIiwiRkVUQ0hfVEFTSyIsIkZFVENIX1RBU0tfRkFJTEVEIiwiRkVUQ0hfVEFTS19TVUNFU1MiLCJERUxFVEVfVEFTSyIsIkRFTEVURV9UQVNLX0ZBSUxFRCIsIkRFTEVURV9UQVNLX1NVQ0VTUyIsIkVESVRfVEFTSyIsIkVESVRfVEFTS19GQUlMRUQiLCJFRElUX1RBU0tfU1VDRVNTIiwiQUNUSVZFX1RBU0siLCJBQ1RJVkVfVEFTS19GQUlMRUQiLCJBQ1RJVkVfVEFTS19TVUNFU1MiLCJDSEFOR0VfU1RBVFVTX1RBU0siLCJDSEFOR0VfU1RBVFVTX1RBU0tfRkFJTEVEIiwiQ0hBTkdFX1NUQVRVU19UQVNLX1NVQ0VTUyIsIkZJTFRFUl9UQVNLIiwiRklMVEVSX1RBU0tfRkFJTEVEIiwiRklMVEVSX1RBU0tfU1VDRVNTIiwiaW5pdGlhbFN0YXRlIiwiZGF0YSIsIm5hbWUiLCJkYXRlIiwiaWQiLCJhZGRlZCIsImxvYWRpbmciLCJlcnJvciIsImVkaXRlZCIsInRhc2tJbml0Iiwic3RhdGUiLCJhY3Rpb24iLCJ1cGRhdGVPYmplY3QiLCJ0YXNrQWRkIiwidGFza0FkZGVkU3VjY2VzIiwidGFza0FkZGVkRmFsaWVkIiwiZmV0Y2h0YXNrU3RhcnQiLCJmZXRjaHRhc2tTdWNjZXNzIiwiZmV0Y2h0YXNrRmFpbCIsInJlZHVjZXIiLCJsb2ciLCJ0eXBlIiwiYWN0aW9uVHlwZXMiLCJvbGRPYmplY3QiLCJ1cGRhdGVkUHJvcGVydGllcyJdLCJtYXBwaW5ncyI6Ijs7UUFBQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLElBQUk7UUFDSjtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBOzs7UUFHQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMENBQTBDLGdDQUFnQztRQUMxRTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLHdEQUF3RCxrQkFBa0I7UUFDMUU7UUFDQSxpREFBaUQsY0FBYztRQUMvRDs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EseUNBQXlDLGlDQUFpQztRQUMxRSxnSEFBZ0gsbUJBQW1CLEVBQUU7UUFDckk7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwyQkFBMkIsMEJBQTBCLEVBQUU7UUFDdkQsaUNBQWlDLGVBQWU7UUFDaEQ7UUFDQTtRQUNBOztRQUVBO1FBQ0Esc0RBQXNELCtEQUErRDs7UUFFckg7UUFDQTs7O1FBR0E7UUFDQTs7Ozs7Ozs7Ozs7O0FDeEZBLCtEOzs7Ozs7Ozs7OztBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsd0M7Ozs7Ozs7Ozs7O0FDTkEsaUJBQWlCLG1CQUFPLENBQUMsaUVBQW1COzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDQTVDOztBQUNBOzs7O0FBa0JBO0FBQ0E7QUFDQTtBQUNBOztBQUNBLGtDQUFrQztBQUFBO0FBQWxDO0FBQWtDLENBQWxDLEVBR3lDO0FBQ3ZDLFFBQU1BLFNBQVMsR0FBRyxNQUFNLDJDQUF4QixHQUF3QixDQUF4QjtBQUNBLFNBQU87QUFBUDtBQUFPLEdBQVA7QUFHYTs7QUFBQSxrQkFBMkNDLGVBQU1DLFNBQWpELENBR2I7QUFJQTtBQUNBO0FBQ0E7QUFDQUMsbUJBQWlCLG9CQUE0QztBQUMzRDtBQUdGQzs7QUFBQUEsUUFBTSxHQUFHO0FBQ1AsVUFBTTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxRQUFxRCxLQUEzRDtBQUdBLHdCQUNFLHFFQUdJO0FBQ0E7QUFDSSxNQUFFQyxPQUFPLElBQVQsV0FBd0I7QUFBRUMsU0FBRyxFQUFFQyxTQUFTLENBQXhDLE1BQXdDO0FBQWhCLEtBQXhCLEdBTlYsRUFDRSxFQURGO0FBZkY7O0FBQUE7OztBQUhtQkMsRyxDQUlaQyxtQkFKWUQsR0FJVUUsa0JBSlZGO0FBQUFBLEcsQ0FLWkcsZUFMWUgsR0FLTUUsa0JBTE5GO0FBK0JyQjtBQUNBOztBQUVBLFVBQTJDO0FBQ3pDSSxlQUFhLEdBQUcscUJBQVMsTUFBTTtBQUM3QkMsV0FBTyxDQUFQQTtBQURGRCxHQUFnQixDQUFoQkE7QUFNQUUsU0FBTyxHQUFHLHFCQUFTLE1BQU07QUFDdkJELFdBQU8sQ0FBUEE7QUFERkMsR0FBVSxDQUFWQTtBQU9GLEMsQ0FBQTs7O0FBQ08sc0JBQTJCO0FBQ2hDLFlBQTJDRixhQUFhO0FBQ3hELFNBQU9HLENBQUMsQ0FBUjtBQUdLOztBQUFBLDJCQUFtQztBQUN4QztBQUNBLFFBQU07QUFBQTtBQUFBO0FBQUE7QUFBQSxNQUFOO0FBQ0EsU0FBTztBQUNMLGdCQUFZO0FBQ1YsZ0JBQTJDRCxPQUFPO0FBQ2xEO0FBSEc7O0FBS0wsbUJBQWU7QUFDYixnQkFBMkNBLE9BQU87QUFDbEQ7QUFQRzs7QUFTTCxpQkFBYTtBQUNYLGdCQUEyQ0EsT0FBTztBQUNsRDtBQVhHOztBQWFMRSxRQUFJLEVBQUUsTUFBTTtBQUNWLGdCQUEyQ0YsT0FBTztBQUNsREcsWUFBTSxDQUFOQTtBQWZHO0FBaUJMQyxRQUFJLEVBQUUsYUFBOEI7QUFDbEMsZ0JBQTJDSixPQUFPO0FBQ2xELGFBQU9HLE1BQU0sQ0FBTkEsVUFBUCxFQUFPQSxDQUFQO0FBbkJHO0FBcUJMRSxVQUFNLEVBQUUsY0FBK0I7QUFDckMsZ0JBQTJDTCxPQUFPO0FBQ2xELFlBQU1NLFNBQVMsR0FBR0MsRUFBRSxVQUFwQjtBQUNBLFlBQU1DLE9BQU8sR0FBR0QsRUFBRSxJQUFsQjtBQUVBLGFBQU9KLE1BQU0sQ0FBTkEsZ0JBQVAsT0FBT0EsQ0FBUDtBQTFCRztBQTRCTE0sV0FBTyxFQUFFLGFBQThCO0FBQ3JDLGdCQUEyQ1QsT0FBTztBQUNsRCxhQUFPRyxNQUFNLENBQU5BLGFBQVAsRUFBT0EsQ0FBUDtBQTlCRztBQWdDTE8sYUFBUyxFQUFFLGNBQStCO0FBQ3hDLGdCQUEyQ1YsT0FBTztBQUNsRCxZQUFNVyxZQUFZLEdBQUdKLEVBQUUsVUFBdkI7QUFDQSxZQUFNSyxVQUFVLEdBQUdMLEVBQUUsSUFBckI7QUFFQSxhQUFPSixNQUFNLENBQU5BLHNCQUFQLFVBQU9BLENBQVA7QUFyQ0o7QUFBTyxHQUFQO0FBd0NELEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNoSUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFFQSxNQUFNVSxnQkFBZ0IsR0FBSUMsNkNBQTFCO0FBRUEsTUFBTUMsV0FBVyxHQUFHQyw2REFBZSxDQUFDO0FBQ2hDQyxVQUFRLEVBQUdDLHNFQUFjQTtBQURPLENBQUQsQ0FBbkM7QUFJQSxNQUFNQyxLQUFLLEdBQUdDLHlEQUFXLENBQUNMLFdBQUQsRUFBY0YsZ0JBQWdCLENBQ25EUSw2REFBZSxDQUFDQyxrREFBRCxDQURvQyxDQUE5QixDQUF6Qjs7QUFLQSxNQUFNQyxLQUFOLFNBQW9CN0IsK0NBQXBCLENBQXdCO0FBRXBCLGVBQWFHLGVBQWIsQ0FBNkI7QUFBQ1QsYUFBRDtBQUFZb0M7QUFBWixHQUE3QixFQUErQztBQUMzQyxVQUFNdEMsU0FBUyxHQUFHRSxTQUFTLENBQUNTLGVBQVYsR0FBNEIsTUFBTVQsU0FBUyxDQUFDUyxlQUFWLENBQTBCMkIsR0FBMUIsQ0FBbEMsR0FBbUUsRUFBckYsQ0FEMkMsQ0FHM0M7O0FBQ0EsV0FBTztBQUFDdEMsZUFBUyxFQUFFQTtBQUFaLEtBQVA7QUFDSDs7QUFFREksUUFBTSxHQUFHO0FBQ0w7QUFDQSxVQUFNO0FBQUNGLGVBQUQ7QUFBWUY7QUFBWixRQUF5QixLQUFLdUMsS0FBcEM7QUFFQSx3QkFDSSxxRUFBQyxvREFBRDtBQUFVLFdBQUssRUFBRU4sS0FBakI7QUFBQSw2QkFDSSxxRUFBQyxTQUFELG9CQUFlakMsU0FBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQURKO0FBS0g7O0FBbEJtQjs7QUFzQlRxQyxvRUFBZixFOzs7Ozs7Ozs7Ozs7QUMxQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFPLE1BQU1HLFFBQVEsR0FBRyxVQUFqQjtBQUVBLE1BQU1DLGNBQWMsR0FBRyxnQkFBdkI7QUFDQSxNQUFNQyxlQUFlLEdBQUcsaUJBQXhCO0FBQ0EsTUFBTUMsZUFBZSxHQUFHLGlCQUF4QjtBQUVBLE1BQU1DLFVBQVUsR0FBRyxZQUFuQjtBQUNBLE1BQU1DLGlCQUFpQixHQUFHLG1CQUExQjtBQUNBLE1BQU1DLGlCQUFpQixHQUFHLG1CQUExQjtBQUVBLE1BQU1DLFdBQVcsR0FBRyxhQUFwQjtBQUNBLE1BQU1DLGtCQUFrQixHQUFHLG9CQUEzQjtBQUNBLE1BQU1DLGtCQUFrQixHQUFHLG9CQUEzQjtBQUdBLE1BQU1DLFNBQVMsR0FBRyxXQUFsQjtBQUNBLE1BQU1DLGdCQUFnQixHQUFHLGtCQUF6QjtBQUNBLE1BQU1DLGdCQUFnQixHQUFHLGtCQUF6QjtBQUlBLE1BQU1DLFdBQVcsR0FBRyxhQUFwQjtBQUNBLE1BQU1DLGtCQUFrQixHQUFHLG9CQUEzQjtBQUNBLE1BQU1DLGtCQUFrQixHQUFHLG9CQUEzQjtBQUlBLE1BQU1DLGtCQUFrQixHQUFHLG9CQUEzQjtBQUNBLE1BQU1DLHlCQUF5QixHQUFHLDJCQUFsQztBQUNBLE1BQU1DLHlCQUF5QixHQUFHLDJCQUFsQztBQUdBLE1BQU1DLFdBQVcsR0FBRyxhQUFwQjtBQUNBLE1BQU1DLGtCQUFrQixHQUFHLG9CQUEzQjtBQUNBLE1BQU1DLGtCQUFrQixHQUFHLG9CQUEzQixDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNsQ1A7QUFDQTtBQUVBLE1BQU1DLFlBQVksR0FBRztBQUNqQkMsTUFBSSxFQUFFLEVBRFc7QUFFakJDLE1BQUksRUFBRyxFQUZVO0FBR2pCQyxNQUFJLEVBQUcsRUFIVTtBQUlqQkMsSUFBRSxFQUFFLENBSmE7QUFLakJDLE9BQUssRUFBRyxLQUxTO0FBTWpCQyxTQUFPLEVBQUUsS0FOUTtBQU9qQkMsT0FBSyxFQUFFLEtBUFU7QUFRakJDLFFBQU0sRUFBRztBQVJRLENBQXJCOztBQVdBLE1BQU1DLFFBQVEsR0FBRyxDQUFFQyxLQUFGLEVBQVNDLE1BQVQsS0FBcUI7QUFDbEMsU0FBT0MsNkRBQVksQ0FBRUYsS0FBRixFQUFTO0FBQUVKLFdBQU8sRUFBRTtBQUFYLEdBQVQsQ0FBbkI7QUFDSCxDQUZEOztBQUlBLE1BQU1PLE9BQU8sR0FBRyxDQUFFSCxLQUFGLEVBQVNDLE1BQVQsS0FBcUI7QUFDakMsU0FBT0MsNkRBQVksQ0FBRUYsS0FBRixFQUFTO0FBQUVKLFdBQU8sRUFBRTtBQUFYLEdBQVQsQ0FBbkI7QUFDSCxDQUZEOztBQUtBLE1BQU1RLGVBQWUsR0FBRyxDQUFFSixLQUFGLEVBQVNDLE1BQVQsS0FBcUI7QUFDekMsU0FBT0MsNkRBQVksQ0FBRUYsS0FBRixFQUFTO0FBQ3hCSixXQUFPLEVBQUUsS0FEZTtBQUV4QkQsU0FBSyxFQUFHO0FBRmdCLEdBQVQsQ0FBbkI7QUFJSCxDQUxEOztBQU9BLE1BQU1VLGVBQWUsR0FBRyxDQUFFTCxLQUFGLEVBQVNDLE1BQVQsS0FBcUI7QUFDekMsU0FBT0MsNkRBQVksQ0FBRUYsS0FBRixFQUFTO0FBQUVILFNBQUssRUFBRTtBQUFULEdBQVQsQ0FBbkI7QUFDSCxDQUZEOztBQUtBLE1BQU1TLGNBQWMsR0FBRyxDQUFFTixLQUFGLEVBQVNDLE1BQVQsS0FBcUI7QUFDeEMsU0FBT0MsNkRBQVksQ0FBRUYsS0FBRixFQUFTO0FBQUVKLFdBQU8sRUFBRTtBQUFYLEdBQVQsQ0FBbkI7QUFDSCxDQUZEOztBQUlBLE1BQU1XLGdCQUFnQixHQUFHLENBQUVQLEtBQUYsRUFBU0MsTUFBVCxLQUFxQjtBQUMxQyxTQUFPQyw2REFBWSxDQUFFRixLQUFGLEVBQVM7QUFDeEJULFFBQUksRUFBRVUsTUFBTSxDQUFDVixJQURXO0FBRXhCSyxXQUFPLEVBQUU7QUFGZSxHQUFULENBQW5CO0FBSUgsQ0FMRDs7QUFPQSxNQUFNWSxhQUFhLEdBQUcsQ0FBRVIsS0FBRixFQUFTQyxNQUFULEtBQXFCO0FBQ3ZDLFNBQU9DLDZEQUFZLENBQUVGLEtBQUYsRUFBUztBQUFFSCxTQUFLLEVBQUU7QUFBVCxHQUFULENBQW5CO0FBQ0gsQ0FGRDs7QUFLQSxNQUFNWSxPQUFPLEdBQUcsQ0FBRVQsS0FBSyxHQUFHVixZQUFWLEVBQXdCVyxNQUF4QixLQUFvQztBQUM1QzVELFNBQU8sQ0FBQ3FFLEdBQVIsQ0FBWVQsTUFBWjs7QUFDSixVQUFTQSxNQUFNLENBQUNVLElBQWhCO0FBQ0ksU0FBS0MsbUVBQUw7QUFDSSxhQUFPYixRQUFRLENBQUVDLEtBQUYsRUFBU0MsTUFBVCxDQUFmOztBQUNKLFNBQUtXLDZEQUFMO0FBQ0ksYUFBT1QsT0FBTyxDQUFFSCxLQUFGLEVBQVNDLE1BQVQsQ0FBZDs7QUFDSixTQUFLVyxvRUFBTDtBQUNJLGFBQU9QLGVBQWUsQ0FBRUwsS0FBRixFQUFTQyxNQUFULENBQXRCOztBQUNKLFNBQUtXLG9FQUFMO0FBQ0ksYUFBT1IsZUFBZSxDQUFFSixLQUFGLEVBQVNDLE1BQVQsQ0FBdEI7O0FBRUosU0FBS1csK0RBQUw7QUFDSSxhQUFPTixjQUFjLENBQUVOLEtBQUYsRUFBU0MsTUFBVCxDQUFyQjs7QUFDSixTQUFLVyxzRUFBTDtBQUNJLGFBQU9KLGFBQWEsQ0FBRVIsS0FBRixFQUFTQyxNQUFULENBQXBCOztBQUNKLFNBQUtXLHNFQUFMO0FBQ0ksYUFBT0wsZ0JBQWdCLENBQUVQLEtBQUYsRUFBU0MsTUFBVCxDQUF2Qjs7QUFFSixTQUFLVyw4REFBTDtBQUNJLDZDQUNPWixLQURQO0FBRUlKLGVBQU8sRUFBQztBQUZaOztBQUlKLFNBQUtnQixxRUFBTDtBQUNJLDZDQUNPWixLQURQO0FBRUlKLGVBQU8sRUFBQyxLQUZaO0FBR0lDLGFBQUssRUFBQztBQUhWOztBQUtKLFNBQUtlLHFFQUFMO0FBQ0ksNkNBQ09aLEtBRFA7QUFFSVIsWUFBSSxFQUFHUyxNQUFNLENBQUNULElBRmxCO0FBR0lDLFlBQUksRUFBR1EsTUFBTSxDQUFDUixJQUhsQjtBQUlJQyxVQUFFLEVBQUdPLE1BQU0sQ0FBQ1AsRUFKaEI7QUFLSUUsZUFBTyxFQUFDLEtBTFo7QUFNSUMsYUFBSyxFQUFDLEtBTlY7QUFPSUMsY0FBTSxFQUFFO0FBUFo7O0FBVUosU0FBS2MsZ0VBQUw7QUFDSSw2Q0FDT1osS0FEUDtBQUVJSixlQUFPLEVBQUM7QUFGWjs7QUFLSixTQUFLZ0IsdUVBQUw7QUFDSSw2Q0FDT1osS0FEUDtBQUVJSixlQUFPLEVBQUMsS0FGWjtBQUdJQyxhQUFLLEVBQUM7QUFIVjs7QUFNSixTQUFLZSx1RUFBTDtBQUNJLDZDQUNPWixLQURQO0FBRUlKLGVBQU8sRUFBQyxLQUZaO0FBR0lDLGFBQUssRUFBQztBQUhWOztBQU1KLFNBQUtlLHVFQUFMO0FBQ0ksNkNBQ09aLEtBRFA7QUFFSUosZUFBTyxFQUFDO0FBRlo7O0FBS0osU0FBS2dCLDhFQUFMO0FBQ0ksNkNBQ09aLEtBRFA7QUFFSUosZUFBTyxFQUFDLEtBRlo7QUFHSUMsYUFBSyxFQUFDO0FBSFY7O0FBTUosU0FBS2UsOEVBQUw7QUFDSSw2Q0FDT1osS0FEUDtBQUVJRixjQUFNLEVBQUcsSUFGYjtBQUdJRixlQUFPLEVBQUMsS0FIWjtBQUlJQyxhQUFLLEVBQUM7QUFKVjs7QUFPSixTQUFLZSxnRUFBTDtBQUNJLDZDQUNPWixLQURQO0FBRUlKLGVBQU8sRUFBQztBQUZaOztBQUlKLFNBQUtnQix1RUFBTDtBQUNLLDZDQUNNWixLQUROO0FBRUdKLGVBQU8sRUFBQyxLQUZYO0FBR0dDLGFBQUssRUFBQztBQUhUOztBQUtMLFNBQUtlLHVFQUFMO0FBQ0ksNkNBQ09aLEtBRFA7QUFFSUosZUFBTyxFQUFDLEtBRlo7QUFHSUMsYUFBSyxFQUFHLEtBSFo7QUFJSU4sWUFBSSxFQUFHVSxNQUFNLENBQUNWO0FBSmxCOztBQU9KO0FBQVMsYUFBT1MsS0FBUDtBQW5HYjtBQXFHSCxDQXZHRDs7QUE2R2VTLHNFQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDaEtPLE1BQU1QLFlBQVksR0FBRyxDQUFDVyxTQUFELEVBQVlDLGlCQUFaLEtBQWtDO0FBQzFELHlDQUNPRCxTQURQLEdBRU9DLGlCQUZQO0FBSUgsQ0FMTSxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0FQLCtDOzs7Ozs7Ozs7OztBQ0FBLGtDOzs7Ozs7Ozs7OztBQ0FBLHdDOzs7Ozs7Ozs7OztBQ0FBLGtEOzs7Ozs7Ozs7OztBQ0FBLGtDOzs7Ozs7Ozs7OztBQ0FBLHdDIiwiZmlsZSI6InBhZ2VzL19hcHAuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHJlcXVpcmUoJy4uL3Nzci1tb2R1bGUtY2FjaGUuanMnKTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0dmFyIHRocmV3ID0gdHJ1ZTtcbiBcdFx0dHJ5IHtcbiBcdFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcbiBcdFx0XHR0aHJldyA9IGZhbHNlO1xuIFx0XHR9IGZpbmFsbHkge1xuIFx0XHRcdGlmKHRocmV3KSBkZWxldGUgaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF07XG4gXHRcdH1cblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gMCk7XG4iLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJuZXh0L2Rpc3QvbmV4dC1zZXJ2ZXIvbGliL3V0aWxzLmpzXCIpOyIsImZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7XG4gIHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7XG4gICAgXCJkZWZhdWx0XCI6IG9ialxuICB9O1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQ7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKCcuL2Rpc3QvcGFnZXMvX2FwcCcpXG4iLCJpbXBvcnQgUmVhY3QsIHsgRXJyb3JJbmZvIH0gZnJvbSAncmVhY3QnXG5pbXBvcnQge1xuICBleGVjT25jZSxcbiAgbG9hZEdldEluaXRpYWxQcm9wcyxcbiAgQXBwQ29udGV4dFR5cGUsXG4gIEFwcEluaXRpYWxQcm9wcyxcbiAgQXBwUHJvcHNUeXBlLFxuICBOZXh0V2ViVml0YWxzTWV0cmljLFxufSBmcm9tICcuLi9uZXh0LXNlcnZlci9saWIvdXRpbHMnXG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tICcuLi9jbGllbnQvcm91dGVyJ1xuXG5leHBvcnQgeyBBcHBJbml0aWFsUHJvcHMgfVxuXG5leHBvcnQgeyBOZXh0V2ViVml0YWxzTWV0cmljIH1cblxuZXhwb3J0IHR5cGUgQXBwQ29udGV4dCA9IEFwcENvbnRleHRUeXBlPFJvdXRlcj5cblxuZXhwb3J0IHR5cGUgQXBwUHJvcHM8UCA9IHt9PiA9IEFwcFByb3BzVHlwZTxSb3V0ZXIsIFA+XG5cbi8qKlxuICogYEFwcGAgY29tcG9uZW50IGlzIHVzZWQgZm9yIGluaXRpYWxpemUgb2YgcGFnZXMuIEl0IGFsbG93cyBmb3Igb3ZlcndyaXRpbmcgYW5kIGZ1bGwgY29udHJvbCBvZiB0aGUgYHBhZ2VgIGluaXRpYWxpemF0aW9uLlxuICogVGhpcyBhbGxvd3MgZm9yIGtlZXBpbmcgc3RhdGUgYmV0d2VlbiBuYXZpZ2F0aW9uLCBjdXN0b20gZXJyb3IgaGFuZGxpbmcsIGluamVjdGluZyBhZGRpdGlvbmFsIGRhdGEuXG4gKi9cbmFzeW5jIGZ1bmN0aW9uIGFwcEdldEluaXRpYWxQcm9wcyh7XG4gIENvbXBvbmVudCxcbiAgY3R4LFxufTogQXBwQ29udGV4dCk6IFByb21pc2U8QXBwSW5pdGlhbFByb3BzPiB7XG4gIGNvbnN0IHBhZ2VQcm9wcyA9IGF3YWl0IGxvYWRHZXRJbml0aWFsUHJvcHMoQ29tcG9uZW50LCBjdHgpXG4gIHJldHVybiB7IHBhZ2VQcm9wcyB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEFwcDxQID0ge30sIENQID0ge30sIFMgPSB7fT4gZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQ8XG4gIFAgJiBBcHBQcm9wczxDUD4sXG4gIFNcbj4ge1xuICBzdGF0aWMgb3JpZ0dldEluaXRpYWxQcm9wcyA9IGFwcEdldEluaXRpYWxQcm9wc1xuICBzdGF0aWMgZ2V0SW5pdGlhbFByb3BzID0gYXBwR2V0SW5pdGlhbFByb3BzXG5cbiAgLy8gS2VwdCBoZXJlIGZvciBiYWNrd2FyZHMgY29tcGF0aWJpbGl0eS5cbiAgLy8gV2hlbiBzb21lb25lIGVuZGVkIEFwcCB0aGV5IGNvdWxkIGNhbGwgYHN1cGVyLmNvbXBvbmVudERpZENhdGNoYC5cbiAgLy8gQGRlcHJlY2F0ZWQgVGhpcyBtZXRob2QgaXMgbm8gbG9uZ2VyIG5lZWRlZC4gRXJyb3JzIGFyZSBjYXVnaHQgYXQgdGhlIHRvcCBsZXZlbFxuICBjb21wb25lbnREaWRDYXRjaChlcnJvcjogRXJyb3IsIF9lcnJvckluZm86IEVycm9ySW5mbyk6IHZvaWQge1xuICAgIHRocm93IGVycm9yXG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgeyByb3V0ZXIsIENvbXBvbmVudCwgcGFnZVByb3BzLCBfX05fU1NHLCBfX05fU1NQIH0gPSB0aGlzXG4gICAgICAucHJvcHMgYXMgQXBwUHJvcHM8Q1A+XG5cbiAgICByZXR1cm4gKFxuICAgICAgPENvbXBvbmVudFxuICAgICAgICB7Li4ucGFnZVByb3BzfVxuICAgICAgICB7XG4gICAgICAgICAgLy8gd2UgZG9uJ3QgYWRkIHRoZSBsZWdhY3kgVVJMIHByb3AgaWYgaXQncyB1c2luZyBub24tbGVnYWN5XG4gICAgICAgICAgLy8gbWV0aG9kcyBsaWtlIGdldFN0YXRpY1Byb3BzIGFuZCBnZXRTZXJ2ZXJTaWRlUHJvcHNcbiAgICAgICAgICAuLi4oIShfX05fU1NHIHx8IF9fTl9TU1ApID8geyB1cmw6IGNyZWF0ZVVybChyb3V0ZXIpIH0gOiB7fSlcbiAgICAgICAgfVxuICAgICAgLz5cbiAgICApXG4gIH1cbn1cblxubGV0IHdhcm5Db250YWluZXI6ICgpID0+IHZvaWRcbmxldCB3YXJuVXJsOiAoKSA9PiB2b2lkXG5cbmlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XG4gIHdhcm5Db250YWluZXIgPSBleGVjT25jZSgoKSA9PiB7XG4gICAgY29uc29sZS53YXJuKFxuICAgICAgYFdhcm5pbmc6IHRoZSBcXGBDb250YWluZXJcXGAgaW4gXFxgX2FwcFxcYCBoYXMgYmVlbiBkZXByZWNhdGVkIGFuZCBzaG91bGQgYmUgcmVtb3ZlZC4gaHR0cHM6Ly9lcnIuc2gvdmVyY2VsL25leHQuanMvYXBwLWNvbnRhaW5lci1kZXByZWNhdGVkYFxuICAgIClcbiAgfSlcblxuICB3YXJuVXJsID0gZXhlY09uY2UoKCkgPT4ge1xuICAgIGNvbnNvbGUuZXJyb3IoXG4gICAgICBgV2FybmluZzogdGhlICd1cmwnIHByb3BlcnR5IGlzIGRlcHJlY2F0ZWQuIGh0dHBzOi8vZXJyLnNoL3ZlcmNlbC9uZXh0LmpzL3VybC1kZXByZWNhdGVkYFxuICAgIClcbiAgfSlcbn1cblxuLy8gQGRlcHJlY2F0ZWQgbm9vcCBmb3Igbm93IHVudGlsIHJlbW92YWxcbmV4cG9ydCBmdW5jdGlvbiBDb250YWluZXIocDogYW55KSB7XG4gIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB3YXJuQ29udGFpbmVyKClcbiAgcmV0dXJuIHAuY2hpbGRyZW5cbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGNyZWF0ZVVybChyb3V0ZXI6IFJvdXRlcikge1xuICAvLyBUaGlzIGlzIHRvIG1ha2Ugc3VyZSB3ZSBkb24ndCByZWZlcmVuY2VzIHRoZSByb3V0ZXIgb2JqZWN0IGF0IGNhbGwgdGltZVxuICBjb25zdCB7IHBhdGhuYW1lLCBhc1BhdGgsIHF1ZXJ5IH0gPSByb3V0ZXJcbiAgcmV0dXJuIHtcbiAgICBnZXQgcXVlcnkoKSB7XG4gICAgICBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJykgd2FyblVybCgpXG4gICAgICByZXR1cm4gcXVlcnlcbiAgICB9LFxuICAgIGdldCBwYXRobmFtZSgpIHtcbiAgICAgIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB3YXJuVXJsKClcbiAgICAgIHJldHVybiBwYXRobmFtZVxuICAgIH0sXG4gICAgZ2V0IGFzUGF0aCgpIHtcbiAgICAgIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB3YXJuVXJsKClcbiAgICAgIHJldHVybiBhc1BhdGhcbiAgICB9LFxuICAgIGJhY2s6ICgpID0+IHtcbiAgICAgIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB3YXJuVXJsKClcbiAgICAgIHJvdXRlci5iYWNrKClcbiAgICB9LFxuICAgIHB1c2g6ICh1cmw6IHN0cmluZywgYXM/OiBzdHJpbmcpID0+IHtcbiAgICAgIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB3YXJuVXJsKClcbiAgICAgIHJldHVybiByb3V0ZXIucHVzaCh1cmwsIGFzKVxuICAgIH0sXG4gICAgcHVzaFRvOiAoaHJlZjogc3RyaW5nLCBhcz86IHN0cmluZykgPT4ge1xuICAgICAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHdhcm5VcmwoKVxuICAgICAgY29uc3QgcHVzaFJvdXRlID0gYXMgPyBocmVmIDogJydcbiAgICAgIGNvbnN0IHB1c2hVcmwgPSBhcyB8fCBocmVmXG5cbiAgICAgIHJldHVybiByb3V0ZXIucHVzaChwdXNoUm91dGUsIHB1c2hVcmwpXG4gICAgfSxcbiAgICByZXBsYWNlOiAodXJsOiBzdHJpbmcsIGFzPzogc3RyaW5nKSA9PiB7XG4gICAgICBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJykgd2FyblVybCgpXG4gICAgICByZXR1cm4gcm91dGVyLnJlcGxhY2UodXJsLCBhcylcbiAgICB9LFxuICAgIHJlcGxhY2VUbzogKGhyZWY6IHN0cmluZywgYXM/OiBzdHJpbmcpID0+IHtcbiAgICAgIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB3YXJuVXJsKClcbiAgICAgIGNvbnN0IHJlcGxhY2VSb3V0ZSA9IGFzID8gaHJlZiA6ICcnXG4gICAgICBjb25zdCByZXBsYWNlVXJsID0gYXMgfHwgaHJlZlxuXG4gICAgICByZXR1cm4gcm91dGVyLnJlcGxhY2UocmVwbGFjZVJvdXRlLCByZXBsYWNlVXJsKVxuICAgIH0sXG4gIH1cbn1cbiIsImltcG9ydCBBcHAgZnJvbSAnbmV4dC9hcHAnO1xyXG5pbXBvcnQge1Byb3ZpZGVyfSBmcm9tICdyZWFjdC1yZWR1eCc7XHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCB3aXRoUmVkdXggZnJvbSBcIm5leHQtcmVkdXgtd3JhcHBlclwiO1xyXG5pbXBvcnQgeyBjcmVhdGVTdG9yZSwgY29tYmluZVJlZHVjZXJzLCBhcHBseU1pZGRsZXdhcmUsIGNvbXBvc2UgfSBmcm9tICdyZWR1eCc7XHJcbmltcG9ydCB0aHVuayBmcm9tICdyZWR1eC10aHVuayc7XHJcblxyXG5pbXBvcnQgVG9kb0FwcFJlZHVjZXIgZnJvbSAnLi4vc3RvcmUvcmVkdWNlcnMvVG9kb0FwcFJlZHVjZXInO1xyXG5cclxuY29uc3QgY29tcG9zZUVuaGFuY2VycyA9ICBjb21wb3NlO1xyXG5cclxuY29uc3Qgcm9vdFJlZHVjZXIgPSBjb21iaW5lUmVkdWNlcnMoe1xyXG4gICAgdG9kb1Rhc2sgOiBUb2RvQXBwUmVkdWNlclxyXG59KTtcclxuXHJcbmNvbnN0IHN0b3JlID0gY3JlYXRlU3RvcmUocm9vdFJlZHVjZXIsIGNvbXBvc2VFbmhhbmNlcnMoXHJcbiAgICBhcHBseU1pZGRsZXdhcmUodGh1bmspXHJcbikpO1xyXG5cclxuXHJcbmNsYXNzIE15QXBwIGV4dGVuZHMgQXBwIHtcclxuXHJcbiAgICBzdGF0aWMgYXN5bmMgZ2V0SW5pdGlhbFByb3BzKHtDb21wb25lbnQsIGN0eH0pIHtcclxuICAgICAgICBjb25zdCBwYWdlUHJvcHMgPSBDb21wb25lbnQuZ2V0SW5pdGlhbFByb3BzID8gYXdhaXQgQ29tcG9uZW50LmdldEluaXRpYWxQcm9wcyhjdHgpIDoge307XHJcblxyXG4gICAgICAgIC8vQW55dGhpbmcgcmV0dXJuZWQgaGVyZSBjYW4gYmUgYWNjZXNzZWQgYnkgdGhlIGNsaWVudFxyXG4gICAgICAgIHJldHVybiB7cGFnZVByb3BzOiBwYWdlUHJvcHN9O1xyXG4gICAgfVxyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICAvL0luZm9ybWF0aW9uIHRoYXQgd2FzIHJldHVybmVkICBmcm9tICdnZXRJbml0aWFsUHJvcHMnIGFyZSBzdG9yZWQgaW4gdGhlIHByb3BzIGkuZS4gcGFnZVByb3BzXHJcbiAgICAgICAgY29uc3Qge0NvbXBvbmVudCwgcGFnZVByb3BzfSA9IHRoaXMucHJvcHM7XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxQcm92aWRlciBzdG9yZT17c3RvcmV9PlxyXG4gICAgICAgICAgICAgICAgPENvbXBvbmVudCB7Li4ucGFnZVByb3BzfS8+XHJcbiAgICAgICAgICAgIDwvUHJvdmlkZXI+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IE15QXBwO1xyXG4iLCJleHBvcnQgY29uc3QgQUREX1RBU0sgPSAnQUREX1RBU0snO1xyXG5cclxuZXhwb3J0IGNvbnN0IEFERF9UQVNLX1NUQVJUID0gJ0FERF9UQVNLX1NUQVJUJztcclxuZXhwb3J0IGNvbnN0IEFERF9UQVNLX0ZBSUxFRCA9ICdBRERfVEFTS19GQUlMRUQnO1xyXG5leHBvcnQgY29uc3QgQUREX1RBU0tfU1VDRVNTID0gJ0FERF9UQVNLX1NVQ0VTUyc7XHJcblxyXG5leHBvcnQgY29uc3QgRkVUQ0hfVEFTSyA9ICdGRVRDSF9UQVNLJztcclxuZXhwb3J0IGNvbnN0IEZFVENIX1RBU0tfRkFJTEVEID0gJ0ZFVENIX1RBU0tfRkFJTEVEJztcclxuZXhwb3J0IGNvbnN0IEZFVENIX1RBU0tfU1VDRVNTID0gJ0ZFVENIX1RBU0tfU1VDRVNTJztcclxuXHJcbmV4cG9ydCBjb25zdCBERUxFVEVfVEFTSyA9ICdERUxFVEVfVEFTSyc7XHJcbmV4cG9ydCBjb25zdCBERUxFVEVfVEFTS19GQUlMRUQgPSAnREVMRVRFX1RBU0tfRkFJTEVEJztcclxuZXhwb3J0IGNvbnN0IERFTEVURV9UQVNLX1NVQ0VTUyA9ICdERUxFVEVfVEFTS19TVUNFU1MnO1xyXG5cclxuXHJcbmV4cG9ydCBjb25zdCBFRElUX1RBU0sgPSAnRURJVF9UQVNLJztcclxuZXhwb3J0IGNvbnN0IEVESVRfVEFTS19GQUlMRUQgPSAnRURJVF9UQVNLX0ZBSUxFRCc7XHJcbmV4cG9ydCBjb25zdCBFRElUX1RBU0tfU1VDRVNTID0gJ0VESVRfVEFTS19TVUNFU1MnO1xyXG5cclxuXHJcblxyXG5leHBvcnQgY29uc3QgQUNUSVZFX1RBU0sgPSAnQUNUSVZFX1RBU0snO1xyXG5leHBvcnQgY29uc3QgQUNUSVZFX1RBU0tfRkFJTEVEID0gJ0FDVElWRV9UQVNLX0ZBSUxFRCc7XHJcbmV4cG9ydCBjb25zdCBBQ1RJVkVfVEFTS19TVUNFU1MgPSAnQUNUSVZFX1RBU0tfU1VDRVNTJztcclxuXHJcblxyXG5cclxuZXhwb3J0IGNvbnN0IENIQU5HRV9TVEFUVVNfVEFTSyA9ICdDSEFOR0VfU1RBVFVTX1RBU0snO1xyXG5leHBvcnQgY29uc3QgQ0hBTkdFX1NUQVRVU19UQVNLX0ZBSUxFRCA9ICdDSEFOR0VfU1RBVFVTX1RBU0tfRkFJTEVEJztcclxuZXhwb3J0IGNvbnN0IENIQU5HRV9TVEFUVVNfVEFTS19TVUNFU1MgPSAnQ0hBTkdFX1NUQVRVU19UQVNLX1NVQ0VTUyc7XHJcblxyXG5cclxuZXhwb3J0IGNvbnN0IEZJTFRFUl9UQVNLID0gJ0ZJTFRFUl9UQVNLJztcclxuZXhwb3J0IGNvbnN0IEZJTFRFUl9UQVNLX0ZBSUxFRCA9ICdGSUxURVJfVEFTS19GQUlMRUQnO1xyXG5leHBvcnQgY29uc3QgRklMVEVSX1RBU0tfU1VDRVNTID0gJ0ZJTFRFUl9UQVNLX1NVQ0VTUyc7XHJcblxyXG4iLCJpbXBvcnQgKiBhcyBhY3Rpb25UeXBlcyBmcm9tICcuLi9hY3Rpb25zL2FjdGlvblR5cGVzJztcclxuaW1wb3J0IHsgdXBkYXRlT2JqZWN0IH0gZnJvbSAnLi4vdXRpbGl0eSc7XHJcblxyXG5jb25zdCBpbml0aWFsU3RhdGUgPSB7XHJcbiAgICBkYXRhOiBbXSxcclxuICAgIG5hbWUgOiAnJyxcclxuICAgIGRhdGUgOiAnJyxcclxuICAgIGlkOiAwLFxyXG4gICAgYWRkZWQgOiBmYWxzZSxcclxuICAgIGxvYWRpbmc6IGZhbHNlLFxyXG4gICAgZXJyb3I6IGZhbHNlLFxyXG4gICAgZWRpdGVkIDogZmFsc2VcclxufTtcclxuXHJcbmNvbnN0IHRhc2tJbml0ID0gKCBzdGF0ZSwgYWN0aW9uICkgPT4ge1xyXG4gICAgcmV0dXJuIHVwZGF0ZU9iamVjdCggc3RhdGUsIHsgbG9hZGluZzogdHJ1ZSB9ICk7XHJcbn07XHJcblxyXG5jb25zdCB0YXNrQWRkID0gKCBzdGF0ZSwgYWN0aW9uICkgPT4ge1xyXG4gICAgcmV0dXJuIHVwZGF0ZU9iamVjdCggc3RhdGUsIHsgbG9hZGluZzogZmFsc2UgfSApO1xyXG59O1xyXG5cclxuXHJcbmNvbnN0IHRhc2tBZGRlZFN1Y2NlcyA9ICggc3RhdGUsIGFjdGlvbiApID0+IHtcclxuICAgIHJldHVybiB1cGRhdGVPYmplY3QoIHN0YXRlLCB7XHJcbiAgICAgICAgbG9hZGluZzogZmFsc2UsXHJcbiAgICAgICAgYWRkZWQgOiB0cnVlXHJcbiAgICB9ICk7XHJcbn07XHJcblxyXG5jb25zdCB0YXNrQWRkZWRGYWxpZWQgPSAoIHN0YXRlLCBhY3Rpb24gKSA9PiB7XHJcbiAgICByZXR1cm4gdXBkYXRlT2JqZWN0KCBzdGF0ZSwgeyBlcnJvcjogdHJ1ZSB9ICk7XHJcbn07XHJcblxyXG5cclxuY29uc3QgZmV0Y2h0YXNrU3RhcnQgPSAoIHN0YXRlLCBhY3Rpb24gKSA9PiB7XHJcbiAgICByZXR1cm4gdXBkYXRlT2JqZWN0KCBzdGF0ZSwgeyBsb2FkaW5nOiB0cnVlIH0gKTtcclxufTtcclxuXHJcbmNvbnN0IGZldGNodGFza1N1Y2Nlc3MgPSAoIHN0YXRlLCBhY3Rpb24gKSA9PiB7XHJcbiAgICByZXR1cm4gdXBkYXRlT2JqZWN0KCBzdGF0ZSwge1xyXG4gICAgICAgIGRhdGE6IGFjdGlvbi5kYXRhLFxyXG4gICAgICAgIGxvYWRpbmc6IGZhbHNlXHJcbiAgICB9ICk7XHJcbn07XHJcblxyXG5jb25zdCBmZXRjaHRhc2tGYWlsID0gKCBzdGF0ZSwgYWN0aW9uICkgPT4ge1xyXG4gICAgcmV0dXJuIHVwZGF0ZU9iamVjdCggc3RhdGUsIHsgZXJyb3I6IHRydWUgfSApO1xyXG59O1xyXG5cclxuXHJcbmNvbnN0IHJlZHVjZXIgPSAoIHN0YXRlID0gaW5pdGlhbFN0YXRlLCBhY3Rpb24gKSA9PiB7XHJcbiAgICAgICAgY29uc29sZS5sb2coYWN0aW9uKTtcclxuICAgIHN3aXRjaCAoIGFjdGlvbi50eXBlICkge1xyXG4gICAgICAgIGNhc2UgYWN0aW9uVHlwZXMuQUREX1RBU0tfU1RBUlQ6XHJcbiAgICAgICAgICAgIHJldHVybiB0YXNrSW5pdCggc3RhdGUsIGFjdGlvbiApO1xyXG4gICAgICAgIGNhc2UgYWN0aW9uVHlwZXMuQUREX1RBU0s6XHJcbiAgICAgICAgICAgIHJldHVybiB0YXNrQWRkKCBzdGF0ZSwgYWN0aW9uICk7XHJcbiAgICAgICAgY2FzZSBhY3Rpb25UeXBlcy5BRERfVEFTS19GQUlMRUQ6XHJcbiAgICAgICAgICAgIHJldHVybiB0YXNrQWRkZWRGYWxpZWQoIHN0YXRlLCBhY3Rpb24gKTtcclxuICAgICAgICBjYXNlIGFjdGlvblR5cGVzLkFERF9UQVNLX1NVQ0VTUzpcclxuICAgICAgICAgICAgcmV0dXJuIHRhc2tBZGRlZFN1Y2Nlcyggc3RhdGUsIGFjdGlvbiApO1xyXG5cclxuICAgICAgICBjYXNlIGFjdGlvblR5cGVzLkZFVENIX1RBU0s6XHJcbiAgICAgICAgICAgIHJldHVybiBmZXRjaHRhc2tTdGFydCggc3RhdGUsIGFjdGlvbiApO1xyXG4gICAgICAgIGNhc2UgYWN0aW9uVHlwZXMuRkVUQ0hfVEFTS19GQUlMRUQ6XHJcbiAgICAgICAgICAgIHJldHVybiBmZXRjaHRhc2tGYWlsKCBzdGF0ZSwgYWN0aW9uICk7XHJcbiAgICAgICAgY2FzZSBhY3Rpb25UeXBlcy5GRVRDSF9UQVNLX1NVQ0VTUzpcclxuICAgICAgICAgICAgcmV0dXJuIGZldGNodGFza1N1Y2Nlc3MoIHN0YXRlLCBhY3Rpb24gKTtcclxuXHJcbiAgICAgICAgY2FzZSBhY3Rpb25UeXBlcy5FRElUX1RBU0s6XHJcbiAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAuLi5zdGF0ZSxcclxuICAgICAgICAgICAgICAgIGxvYWRpbmc6dHJ1ZVxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgIGNhc2UgYWN0aW9uVHlwZXMuRURJVF9UQVNLX0ZBSUxFRDpcclxuICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgIC4uLnN0YXRlLFxyXG4gICAgICAgICAgICAgICAgbG9hZGluZzpmYWxzZSxcclxuICAgICAgICAgICAgICAgIGVycm9yOnRydWVcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICBjYXNlIGFjdGlvblR5cGVzLkVESVRfVEFTS19TVUNFU1M6XHJcbiAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAuLi5zdGF0ZSxcclxuICAgICAgICAgICAgICAgIG5hbWUgOiBhY3Rpb24ubmFtZSxcclxuICAgICAgICAgICAgICAgIGRhdGUgOiBhY3Rpb24uZGF0ZSxcclxuICAgICAgICAgICAgICAgIGlkIDogYWN0aW9uLmlkLFxyXG4gICAgICAgICAgICAgICAgbG9hZGluZzpmYWxzZSxcclxuICAgICAgICAgICAgICAgIGVycm9yOmZhbHNlLFxyXG4gICAgICAgICAgICAgICAgZWRpdGVkIDp0cnVlXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgIGNhc2UgYWN0aW9uVHlwZXMuREVMRVRFX1RBU0s6XHJcbiAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAuLi5zdGF0ZSxcclxuICAgICAgICAgICAgICAgIGxvYWRpbmc6dHJ1ZVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICBjYXNlIGFjdGlvblR5cGVzLkRFTEVURV9UQVNLX0ZBSUxFRDpcclxuICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgIC4uLnN0YXRlLFxyXG4gICAgICAgICAgICAgICAgbG9hZGluZzpmYWxzZSxcclxuICAgICAgICAgICAgICAgIGVycm9yOnRydWVcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgY2FzZSBhY3Rpb25UeXBlcy5ERUxFVEVfVEFTS19TVUNFU1M6XHJcbiAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAuLi5zdGF0ZSxcclxuICAgICAgICAgICAgICAgIGxvYWRpbmc6ZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBlcnJvcjpmYWxzZVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICBjYXNlIGFjdGlvblR5cGVzLkNIQU5HRV9TVEFUVVNfVEFTSzpcclxuICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgIC4uLnN0YXRlLFxyXG4gICAgICAgICAgICAgICAgbG9hZGluZzp0cnVlXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgIGNhc2UgYWN0aW9uVHlwZXMuQ0hBTkdFX1NUQVRVU19UQVNLX0ZBSUxFRDpcclxuICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgIC4uLnN0YXRlLFxyXG4gICAgICAgICAgICAgICAgbG9hZGluZzpmYWxzZSxcclxuICAgICAgICAgICAgICAgIGVycm9yOnRydWVcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgY2FzZSBhY3Rpb25UeXBlcy5DSEFOR0VfU1RBVFVTX1RBU0tfU1VDRVNTOlxyXG4gICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgLi4uc3RhdGUsXHJcbiAgICAgICAgICAgICAgICBlZGl0ZWQgOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgbG9hZGluZzpmYWxzZSxcclxuICAgICAgICAgICAgICAgIGVycm9yOmZhbHNlXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgIGNhc2UgYWN0aW9uVHlwZXMuRklMVEVSX1RBU0s6XHJcbiAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAuLi5zdGF0ZSxcclxuICAgICAgICAgICAgICAgIGxvYWRpbmc6dHJ1ZVxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgIGNhc2UgYWN0aW9uVHlwZXMuRklMVEVSX1RBU0tfRkFJTEVEOlxyXG4gICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgIC4uLnN0YXRlLFxyXG4gICAgICAgICAgICAgICAgbG9hZGluZzpmYWxzZSxcclxuICAgICAgICAgICAgICAgIGVycm9yOnRydWVcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICBjYXNlIGFjdGlvblR5cGVzLkZJTFRFUl9UQVNLX1NVQ0VTUzpcclxuICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgIC4uLnN0YXRlLFxyXG4gICAgICAgICAgICAgICAgbG9hZGluZzpmYWxzZSxcclxuICAgICAgICAgICAgICAgIGVycm9yIDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBkYXRhIDogYWN0aW9uLmRhdGFcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgZGVmYXVsdDogcmV0dXJuIHN0YXRlO1xyXG4gICAgfVxyXG59XHJcblxyXG5cclxuXHJcblxyXG5cclxuZXhwb3J0IGRlZmF1bHQgcmVkdWNlcjtcclxuIiwiZXhwb3J0IGNvbnN0IHVwZGF0ZU9iamVjdCA9IChvbGRPYmplY3QsIHVwZGF0ZWRQcm9wZXJ0aWVzKSA9PiB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICAgIC4uLm9sZE9iamVjdCxcclxuICAgICAgICAuLi51cGRhdGVkUHJvcGVydGllc1xyXG4gICAgfTtcclxufTtcclxuIiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwibmV4dC1yZWR1eC13cmFwcGVyXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0XCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0LXJlZHV4XCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0L2pzeC1kZXYtcnVudGltZVwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWR1eFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWR1eC10aHVua1wiKTsiXSwic291cmNlUm9vdCI6IiJ9