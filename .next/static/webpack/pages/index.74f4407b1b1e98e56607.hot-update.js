webpackHotUpdate_N_E("pages/index",{

/***/ "./components/TodoForm.js":
/*!********************************!*\
  !*** ./components/TodoForm.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_bootstrap_Row__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap/Row */ "./node_modules/react-bootstrap/esm/Row.js");
/* harmony import */ var react_bootstrap_Col__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-bootstrap/Col */ "./node_modules/react-bootstrap/esm/Col.js");
/* harmony import */ var react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-bootstrap/Form */ "./node_modules/react-bootstrap/esm/Form.js");
/* harmony import */ var react_bootstrap_Button__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-bootstrap/Button */ "./node_modules/react-bootstrap/esm/Button.js");


var _jsxFileName = "D:\\PERSONAL\\Vanita\\React\\test\\todo-next\\components\\TodoForm.js",
    _this = undefined;







var TodoForm = function TodoForm(props) {
  console.log(props);
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_4__["default"], {
    id: "todo-form",
    onSubmit: props.handleSubmit,
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_4__["default"].Group, {
      controlId: "fromTodo",
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_4__["default"].Label, {
        children: "Add Task :"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 12,
        columnNumber: 13
      }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap_Row__WEBPACK_IMPORTED_MODULE_2__["default"], {
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap_Col__WEBPACK_IMPORTED_MODULE_3__["default"], {
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("input", {
            type: "hidden",
            value: props.data.id
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 15,
            columnNumber: 13
          }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_4__["default"].Control, {
            type: "text",
            required: true,
            id: "name",
            defaultValue: typeof props.data.name === 'undefined' ? '' : props.data.name,
            placeholder: "Task Name",
            onChange: props.handleChange
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 16,
            columnNumber: 15
          }, _this), props.validate.length > 0 && /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
            className: "error",
            children: props.validate
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 18,
            columnNumber: 17
          }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_4__["default"].Text, {
            id: "taskHelp",
            muted: true,
            children: "Task name should contains only alphabets"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 19,
            columnNumber: 15
          }, _this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 14,
          columnNumber: 13
        }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap_Col__WEBPACK_IMPORTED_MODULE_3__["default"], {
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_4__["default"].Control, {
            type: "date",
            required: true,
            id: "date",
            defaultValue: typeof props.data.date === 'undefined' ? '' : props.data.date,
            onChange: props.handleChange
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 24,
            columnNumber: 15
          }, _this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 23,
          columnNumber: 13
        }, _this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 13,
        columnNumber: 11
      }, _this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 11,
      columnNumber: 9
    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap_Row__WEBPACK_IMPORTED_MODULE_2__["default"], {
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap_Button__WEBPACK_IMPORTED_MODULE_5__["default"], {
        variant: "primary",
        type: "submit",
        className: "ml-3",
        children: "Submit"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 29,
        columnNumber: 13
      }, _this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 28,
      columnNumber: 11
    }, _this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 10,
    columnNumber: 9
  }, _this);
};

_c = TodoForm;
/* harmony default export */ __webpack_exports__["default"] = (TodoForm);

var _c;

$RefreshReg$(_c, "TodoForm");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vY29tcG9uZW50cy9Ub2RvRm9ybS5qcyJdLCJuYW1lcyI6WyJUb2RvRm9ybSIsInByb3BzIiwiY29uc29sZSIsImxvZyIsImhhbmRsZVN1Ym1pdCIsImRhdGEiLCJpZCIsIm5hbWUiLCJoYW5kbGVDaGFuZ2UiLCJ2YWxpZGF0ZSIsImxlbmd0aCIsImRhdGUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsSUFBTUEsUUFBUSxHQUFHLFNBQVhBLFFBQVcsQ0FBQ0MsS0FBRCxFQUFXO0FBQ3hCQyxTQUFPLENBQUNDLEdBQVIsQ0FBWUYsS0FBWjtBQUNBLHNCQUNJLHFFQUFDLDREQUFEO0FBQU0sTUFBRSxFQUFDLFdBQVQ7QUFBcUIsWUFBUSxFQUFFQSxLQUFLLENBQUNHLFlBQXJDO0FBQUEsNEJBQ0EscUVBQUMsNERBQUQsQ0FBTSxLQUFOO0FBQVksZUFBUyxFQUFDLFVBQXRCO0FBQUEsOEJBQ0kscUVBQUMsNERBQUQsQ0FBTSxLQUFOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBREosZUFFRSxxRUFBQywyREFBRDtBQUFBLGdDQUNFLHFFQUFDLDJEQUFEO0FBQUEsa0NBQ0E7QUFBTyxnQkFBSSxFQUFDLFFBQVo7QUFBc0IsaUJBQUssRUFBRUgsS0FBSyxDQUFDSSxJQUFOLENBQVdDO0FBQXhDO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBREEsZUFFRSxxRUFBQyw0REFBRCxDQUFNLE9BQU47QUFBYyxnQkFBSSxFQUFDLE1BQW5CO0FBQTJCLG9CQUFRLE1BQW5DO0FBQW9DLGNBQUUsRUFBQyxNQUF2QztBQUE4Qyx3QkFBWSxFQUFFLE9BQU9MLEtBQUssQ0FBQ0ksSUFBTixDQUFXRSxJQUFsQixLQUE0QixXQUE1QixHQUEwQyxFQUExQyxHQUErQ04sS0FBSyxDQUFDSSxJQUFOLENBQVdFLElBQXRIO0FBQTRILHVCQUFXLEVBQUMsV0FBeEk7QUFBb0osb0JBQVEsRUFBRU4sS0FBSyxDQUFDTztBQUFwSztBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQUZGLEVBR0tQLEtBQUssQ0FBQ1EsUUFBTixDQUFlQyxNQUFmLEdBQXdCLENBQXhCLGlCQUNEO0FBQU0scUJBQVMsRUFBQyxPQUFoQjtBQUFBLHNCQUF5QlQsS0FBSyxDQUFDUTtBQUEvQjtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQUpKLGVBS0UscUVBQUMsNERBQUQsQ0FBTSxJQUFOO0FBQVcsY0FBRSxFQUFDLFVBQWQ7QUFBeUIsaUJBQUssTUFBOUI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBTEY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQURGLGVBVUUscUVBQUMsMkRBQUQ7QUFBQSxpQ0FDRSxxRUFBQyw0REFBRCxDQUFNLE9BQU47QUFBYyxnQkFBSSxFQUFDLE1BQW5CO0FBQTBCLG9CQUFRLE1BQWxDO0FBQW1DLGNBQUUsRUFBQyxNQUF0QztBQUE2Qyx3QkFBWSxFQUFFLE9BQU9SLEtBQUssQ0FBQ0ksSUFBTixDQUFXTSxJQUFsQixLQUE0QixXQUE1QixHQUEwQyxFQUExQyxHQUErQ1YsS0FBSyxDQUFDSSxJQUFOLENBQVdNLElBQXJIO0FBQTJILG9CQUFRLEVBQUVWLEtBQUssQ0FBQ087QUFBM0k7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBVkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGFBREEsZUFrQkUscUVBQUMsMkRBQUQ7QUFBQSw2QkFDRSxxRUFBQyw4REFBRDtBQUFRLGVBQU8sRUFBQyxTQUFoQjtBQUEwQixZQUFJLEVBQUMsUUFBL0I7QUFBd0MsaUJBQVMsRUFBQyxNQUFsRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsYUFsQkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFdBREo7QUEwQkgsQ0E1QkQ7O0tBQU1SLFE7QUE4QlNBLHVFQUFmIiwiZmlsZSI6InN0YXRpYy93ZWJwYWNrL3BhZ2VzL2luZGV4Ljc0ZjQ0MDdiMWIxZTk4ZTU2NjA3LmhvdC11cGRhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QgIGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IFJvdyBmcm9tICdyZWFjdC1ib290c3RyYXAvUm93JztcclxuaW1wb3J0IENvbCBmcm9tICdyZWFjdC1ib290c3RyYXAvQ29sJztcclxuaW1wb3J0IEZvcm0gZnJvbSAncmVhY3QtYm9vdHN0cmFwL0Zvcm0nO1xyXG5pbXBvcnQgQnV0dG9uIGZyb20gJ3JlYWN0LWJvb3RzdHJhcC9CdXR0b24nO1xyXG5cclxuY29uc3QgVG9kb0Zvcm0gPSAocHJvcHMpID0+IHtcclxuICAgIGNvbnNvbGUubG9nKHByb3BzKTtcclxuICAgIHJldHVybihcclxuICAgICAgICA8Rm9ybSBpZD1cInRvZG8tZm9ybVwiIG9uU3VibWl0PXtwcm9wcy5oYW5kbGVTdWJtaXR9PlxyXG4gICAgICAgIDxGb3JtLkdyb3VwIGNvbnRyb2xJZD1cImZyb21Ub2RvXCI+XHJcbiAgICAgICAgICAgIDxGb3JtLkxhYmVsPkFkZCBUYXNrIDo8L0Zvcm0uTGFiZWw+XHJcbiAgICAgICAgICA8Um93PlxyXG4gICAgICAgICAgICA8Q29sPlxyXG4gICAgICAgICAgICA8aW5wdXQgdHlwZT1cImhpZGRlblwiICB2YWx1ZT17cHJvcHMuZGF0YS5pZH0gLz5cclxuICAgICAgICAgICAgICA8Rm9ybS5Db250cm9sIHR5cGU9XCJ0ZXh0XCIgIHJlcXVpcmVkIGlkPVwibmFtZVwiIGRlZmF1bHRWYWx1ZT17dHlwZW9mIHByb3BzLmRhdGEubmFtZSA9PT0gICd1bmRlZmluZWQnID8gJycgOiBwcm9wcy5kYXRhLm5hbWV9IHBsYWNlaG9sZGVyPVwiVGFzayBOYW1lXCIgb25DaGFuZ2U9e3Byb3BzLmhhbmRsZUNoYW5nZX0gLz5cclxuICAgICAgICAgICAgICAgIHtwcm9wcy52YWxpZGF0ZS5sZW5ndGggPiAwICYmXHJcbiAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9J2Vycm9yJz57cHJvcHMudmFsaWRhdGV9PC9zcGFuPn1cclxuICAgICAgICAgICAgICA8Rm9ybS5UZXh0IGlkPVwidGFza0hlbHBcIiBtdXRlZD5cclxuICAgICAgICAgICAgICAgIFRhc2sgbmFtZSBzaG91bGQgY29udGFpbnMgb25seSBhbHBoYWJldHNcclxuICAgICAgICAgICAgICA8L0Zvcm0uVGV4dD5cclxuICAgICAgICAgICAgPC9Db2w+XHJcbiAgICAgICAgICAgIDxDb2w+XHJcbiAgICAgICAgICAgICAgPEZvcm0uQ29udHJvbCB0eXBlPVwiZGF0ZVwiIHJlcXVpcmVkIGlkPVwiZGF0ZVwiIGRlZmF1bHRWYWx1ZT17dHlwZW9mIHByb3BzLmRhdGEuZGF0ZSA9PT0gICd1bmRlZmluZWQnID8gJycgOiBwcm9wcy5kYXRhLmRhdGV9IG9uQ2hhbmdlPXtwcm9wcy5oYW5kbGVDaGFuZ2V9IC8+XHJcbiAgICAgICAgICAgIDwvQ29sPlxyXG4gICAgICAgICAgPC9Sb3c+XHJcbiAgICAgICAgPC9Gb3JtLkdyb3VwPlxyXG4gICAgICAgICAgPFJvdz5cclxuICAgICAgICAgICAgPEJ1dHRvbiB2YXJpYW50PVwicHJpbWFyeVwiIHR5cGU9XCJzdWJtaXRcIiBjbGFzc05hbWU9XCJtbC0zXCI+XHJcbiAgICAgICAgICAgICAgICBTdWJtaXRcclxuICAgICAgICAgICAgPC9CdXR0b24+XHJcbiAgICAgICAgICA8L1Jvdz5cclxuICAgICAgICA8L0Zvcm0+XHJcbiAgICApO1xyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBUb2RvRm9ybTtcclxuIl0sInNvdXJjZVJvb3QiOiIifQ==