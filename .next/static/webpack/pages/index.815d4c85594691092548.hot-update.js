webpackHotUpdate_N_E("pages/index",{

/***/ "./components/TodoForm.js":
/*!********************************!*\
  !*** ./components/TodoForm.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_bootstrap_Row__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap/Row */ "./node_modules/react-bootstrap/esm/Row.js");
/* harmony import */ var react_bootstrap_Col__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-bootstrap/Col */ "./node_modules/react-bootstrap/esm/Col.js");
/* harmony import */ var react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-bootstrap/Form */ "./node_modules/react-bootstrap/esm/Form.js");
/* harmony import */ var react_bootstrap_Button__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-bootstrap/Button */ "./node_modules/react-bootstrap/esm/Button.js");


var _jsxFileName = "D:\\PERSONAL\\Vanita\\React\\test\\todo-next\\components\\TodoForm.js",
    _this = undefined;







var TodoForm = function TodoForm(props) {
  console.log(props);
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_4__["default"], {
    id: "todo-form",
    onSubmit: props.handleSubmit,
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_4__["default"].Group, {
      controlId: "fromTodo",
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_4__["default"].Label, {
        children: "Add Task :"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 12,
        columnNumber: 13
      }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap_Row__WEBPACK_IMPORTED_MODULE_2__["default"], {
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap_Col__WEBPACK_IMPORTED_MODULE_3__["default"], {
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("input", {
            type: "hidden",
            id: "taskId",
            value: props.data.id
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 15,
            columnNumber: 13
          }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_4__["default"].Control, {
            type: "text",
            required: true,
            id: "name",
            defaultValue: typeof props.data.name === 'undefined' ? '' : props.data.name,
            placeholder: "Task Name",
            onChange: props.handleChange
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 16,
            columnNumber: 15
          }, _this), props.validate.length > 0 && /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
            className: "error",
            children: props.validate
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 18,
            columnNumber: 17
          }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_4__["default"].Text, {
            id: "taskHelp",
            muted: true,
            children: "Task name should contains only alphabets"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 19,
            columnNumber: 15
          }, _this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 14,
          columnNumber: 13
        }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap_Col__WEBPACK_IMPORTED_MODULE_3__["default"], {
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_4__["default"].Control, {
            type: "date",
            required: true,
            id: "date",
            defaultValue: typeof props.data.date === 'undefined' ? '' : props.data.date,
            onChange: props.handleChange
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 24,
            columnNumber: 15
          }, _this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 23,
          columnNumber: 13
        }, _this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 13,
        columnNumber: 11
      }, _this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 11,
      columnNumber: 9
    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap_Row__WEBPACK_IMPORTED_MODULE_2__["default"], {
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap_Button__WEBPACK_IMPORTED_MODULE_5__["default"], {
        variant: "primary",
        type: "submit",
        className: "ml-3",
        children: "Submit"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 29,
        columnNumber: 13
      }, _this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 28,
      columnNumber: 11
    }, _this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 10,
    columnNumber: 9
  }, _this);
};

_c = TodoForm;
/* harmony default export */ __webpack_exports__["default"] = (TodoForm);

var _c;

$RefreshReg$(_c, "TodoForm");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vY29tcG9uZW50cy9Ub2RvRm9ybS5qcyJdLCJuYW1lcyI6WyJUb2RvRm9ybSIsInByb3BzIiwiY29uc29sZSIsImxvZyIsImhhbmRsZVN1Ym1pdCIsImRhdGEiLCJpZCIsIm5hbWUiLCJoYW5kbGVDaGFuZ2UiLCJ2YWxpZGF0ZSIsImxlbmd0aCIsImRhdGUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsSUFBTUEsUUFBUSxHQUFHLFNBQVhBLFFBQVcsQ0FBQ0MsS0FBRCxFQUFXO0FBQ3hCQyxTQUFPLENBQUNDLEdBQVIsQ0FBWUYsS0FBWjtBQUNBLHNCQUNJLHFFQUFDLDREQUFEO0FBQU0sTUFBRSxFQUFDLFdBQVQ7QUFBcUIsWUFBUSxFQUFFQSxLQUFLLENBQUNHLFlBQXJDO0FBQUEsNEJBQ0EscUVBQUMsNERBQUQsQ0FBTSxLQUFOO0FBQVksZUFBUyxFQUFDLFVBQXRCO0FBQUEsOEJBQ0kscUVBQUMsNERBQUQsQ0FBTSxLQUFOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBREosZUFFRSxxRUFBQywyREFBRDtBQUFBLGdDQUNFLHFFQUFDLDJEQUFEO0FBQUEsa0NBQ0E7QUFBTyxnQkFBSSxFQUFDLFFBQVo7QUFBcUIsY0FBRSxFQUFDLFFBQXhCO0FBQWlDLGlCQUFLLEVBQUVILEtBQUssQ0FBQ0ksSUFBTixDQUFXQztBQUFuRDtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQURBLGVBRUUscUVBQUMsNERBQUQsQ0FBTSxPQUFOO0FBQWMsZ0JBQUksRUFBQyxNQUFuQjtBQUEyQixvQkFBUSxNQUFuQztBQUFvQyxjQUFFLEVBQUMsTUFBdkM7QUFBOEMsd0JBQVksRUFBRSxPQUFPTCxLQUFLLENBQUNJLElBQU4sQ0FBV0UsSUFBbEIsS0FBNEIsV0FBNUIsR0FBMEMsRUFBMUMsR0FBK0NOLEtBQUssQ0FBQ0ksSUFBTixDQUFXRSxJQUF0SDtBQUE0SCx1QkFBVyxFQUFDLFdBQXhJO0FBQW9KLG9CQUFRLEVBQUVOLEtBQUssQ0FBQ087QUFBcEs7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFGRixFQUdLUCxLQUFLLENBQUNRLFFBQU4sQ0FBZUMsTUFBZixHQUF3QixDQUF4QixpQkFDRDtBQUFNLHFCQUFTLEVBQUMsT0FBaEI7QUFBQSxzQkFBeUJULEtBQUssQ0FBQ1E7QUFBL0I7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFKSixlQUtFLHFFQUFDLDREQUFELENBQU0sSUFBTjtBQUFXLGNBQUUsRUFBQyxVQUFkO0FBQXlCLGlCQUFLLE1BQTlCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQUxGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFERixlQVVFLHFFQUFDLDJEQUFEO0FBQUEsaUNBQ0UscUVBQUMsNERBQUQsQ0FBTSxPQUFOO0FBQWMsZ0JBQUksRUFBQyxNQUFuQjtBQUEwQixvQkFBUSxNQUFsQztBQUFtQyxjQUFFLEVBQUMsTUFBdEM7QUFBNkMsd0JBQVksRUFBRSxPQUFPUixLQUFLLENBQUNJLElBQU4sQ0FBV00sSUFBbEIsS0FBNEIsV0FBNUIsR0FBMEMsRUFBMUMsR0FBK0NWLEtBQUssQ0FBQ0ksSUFBTixDQUFXTSxJQUFySDtBQUEySCxvQkFBUSxFQUFFVixLQUFLLENBQUNPO0FBQTNJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxhQURBLGVBa0JFLHFFQUFDLDJEQUFEO0FBQUEsNkJBQ0UscUVBQUMsOERBQUQ7QUFBUSxlQUFPLEVBQUMsU0FBaEI7QUFBMEIsWUFBSSxFQUFDLFFBQS9CO0FBQXdDLGlCQUFTLEVBQUMsTUFBbEQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGFBbEJGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxXQURKO0FBMEJILENBNUJEOztLQUFNUixRO0FBOEJTQSx1RUFBZiIsImZpbGUiOiJzdGF0aWMvd2VicGFjay9wYWdlcy9pbmRleC44MTVkNGM4NTU5NDY5MTA5MjU0OC5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0ICBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBSb3cgZnJvbSAncmVhY3QtYm9vdHN0cmFwL1Jvdyc7XHJcbmltcG9ydCBDb2wgZnJvbSAncmVhY3QtYm9vdHN0cmFwL0NvbCc7XHJcbmltcG9ydCBGb3JtIGZyb20gJ3JlYWN0LWJvb3RzdHJhcC9Gb3JtJztcclxuaW1wb3J0IEJ1dHRvbiBmcm9tICdyZWFjdC1ib290c3RyYXAvQnV0dG9uJztcclxuXHJcbmNvbnN0IFRvZG9Gb3JtID0gKHByb3BzKSA9PiB7XHJcbiAgICBjb25zb2xlLmxvZyhwcm9wcyk7XHJcbiAgICByZXR1cm4oXHJcbiAgICAgICAgPEZvcm0gaWQ9XCJ0b2RvLWZvcm1cIiBvblN1Ym1pdD17cHJvcHMuaGFuZGxlU3VibWl0fT5cclxuICAgICAgICA8Rm9ybS5Hcm91cCBjb250cm9sSWQ9XCJmcm9tVG9kb1wiPlxyXG4gICAgICAgICAgICA8Rm9ybS5MYWJlbD5BZGQgVGFzayA6PC9Gb3JtLkxhYmVsPlxyXG4gICAgICAgICAgPFJvdz5cclxuICAgICAgICAgICAgPENvbD5cclxuICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJoaWRkZW5cIiBpZD1cInRhc2tJZFwiIHZhbHVlPXtwcm9wcy5kYXRhLmlkfSAvPlxyXG4gICAgICAgICAgICAgIDxGb3JtLkNvbnRyb2wgdHlwZT1cInRleHRcIiAgcmVxdWlyZWQgaWQ9XCJuYW1lXCIgZGVmYXVsdFZhbHVlPXt0eXBlb2YgcHJvcHMuZGF0YS5uYW1lID09PSAgJ3VuZGVmaW5lZCcgPyAnJyA6IHByb3BzLmRhdGEubmFtZX0gcGxhY2Vob2xkZXI9XCJUYXNrIE5hbWVcIiBvbkNoYW5nZT17cHJvcHMuaGFuZGxlQ2hhbmdlfSAvPlxyXG4gICAgICAgICAgICAgICAge3Byb3BzLnZhbGlkYXRlLmxlbmd0aCA+IDAgJiZcclxuICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT0nZXJyb3InPntwcm9wcy52YWxpZGF0ZX08L3NwYW4+fVxyXG4gICAgICAgICAgICAgIDxGb3JtLlRleHQgaWQ9XCJ0YXNrSGVscFwiIG11dGVkPlxyXG4gICAgICAgICAgICAgICAgVGFzayBuYW1lIHNob3VsZCBjb250YWlucyBvbmx5IGFscGhhYmV0c1xyXG4gICAgICAgICAgICAgIDwvRm9ybS5UZXh0PlxyXG4gICAgICAgICAgICA8L0NvbD5cclxuICAgICAgICAgICAgPENvbD5cclxuICAgICAgICAgICAgICA8Rm9ybS5Db250cm9sIHR5cGU9XCJkYXRlXCIgcmVxdWlyZWQgaWQ9XCJkYXRlXCIgZGVmYXVsdFZhbHVlPXt0eXBlb2YgcHJvcHMuZGF0YS5kYXRlID09PSAgJ3VuZGVmaW5lZCcgPyAnJyA6IHByb3BzLmRhdGEuZGF0ZX0gb25DaGFuZ2U9e3Byb3BzLmhhbmRsZUNoYW5nZX0gLz5cclxuICAgICAgICAgICAgPC9Db2w+XHJcbiAgICAgICAgICA8L1Jvdz5cclxuICAgICAgICA8L0Zvcm0uR3JvdXA+XHJcbiAgICAgICAgICA8Um93PlxyXG4gICAgICAgICAgICA8QnV0dG9uIHZhcmlhbnQ9XCJwcmltYXJ5XCIgdHlwZT1cInN1Ym1pdFwiIGNsYXNzTmFtZT1cIm1sLTNcIj5cclxuICAgICAgICAgICAgICAgIFN1Ym1pdFxyXG4gICAgICAgICAgICA8L0J1dHRvbj5cclxuICAgICAgICAgIDwvUm93PlxyXG4gICAgICAgIDwvRm9ybT5cclxuICAgICk7XHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IFRvZG9Gb3JtO1xyXG4iXSwic291cmNlUm9vdCI6IiJ9