webpackHotUpdate_N_E("pages/index",{

/***/ "./containers/TodoAppBuilder.js":
/*!**************************************!*\
  !*** ./containers/TodoAppBuilder.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var _babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/esm/classCallCheck */ "./node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/esm/createClass */ "./node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/esm/assertThisInitialized */ "./node_modules/@babel/runtime/helpers/esm/assertThisInitialized.js");
/* harmony import */ var _babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/esm/inherits */ "./node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime/helpers/esm/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _components_TodoForm__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../components/TodoForm */ "./components/TodoForm.js");
/* harmony import */ var _components_Message__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../components/Message */ "./components/Message.js");
/* harmony import */ var _store_actions_index__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../store/actions/index */ "./store/actions/index.js");








var _jsxFileName = "D:\\PERSONAL\\Vanita\\React\\test\\todo-next\\containers\\TodoAppBuilder.js";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return Object(_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }







var TodoAppBuilder = /*#__PURE__*/function (_Component) {
  Object(_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_3__["default"])(TodoAppBuilder, _Component);

  var _super = _createSuper(TodoAppBuilder);

  function TodoAppBuilder() {
    var _this;

    Object(_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, TodoAppBuilder);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "state", {
      name: '',
      date: '',
      loading: false,
      error: false,
      showMsg: true,
      validate: false,
      errorNameMsg: ''
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleSubmit", function (event) {
      console.log('== here in Builder===');
      event.preventDefault();
      var task = {
        name: _this.state.name,
        date: _this.state.date,
        complete: false,
        active: true
      };

      if (document.getElementById("taskId").value != '') {
        task = _objectSpread(_objectSpread({}, task), {}, {
          id: document.getElementById("taskId").value
        });
      }

      _this.setState(_objectSpread(_objectSpread({}, _this.state), {}, {
        showMsg: true
      }));

      if (_this.state.validate) {
        _this.props.onSubmitTask(task);
      }
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleChange", function (event) {
      event.preventDefault();
      var _event$target = event.target,
          type = _event$target.type,
          value = _event$target.value;
      var letters = /^[A-Za-z]+$/; // if(event.target.type == 'text') {
      //     if(!value.match(letters)){
      //         this.setState({
      //              ...this.state,
      //             validate : false,
      //             errorNameMsg : 'Please Enter Letters Only.'
      //         });
      //     }else{
      //         this.setState({
      //             ...this.state,
      //             validate : true,
      //             name : event.target.value
      //         });
      //     }
      // }
      // if(event.target.type === 'date'){
      //     this.setState({
      //         ...this.state,
      //         date : event.target.value
      //     });
      // }
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleMsg", function () {
      _this.setState(_objectSpread(_objectSpread({}, _this.state), {}, {
        showMsg: false
      }));

      document.getElementById("todo-form").reset();
    });

    return _this;
  }

  Object(_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(TodoAppBuilder, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (this.props.edited) {
        this.setState(_objectSpread(_objectSpread({}, this.state), {}, {
          name: this.props.name,
          date: this.props.date,
          showMsg: true
        }));
      }
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate() {
      if (this.props.edited) {
        document.body.scrollTop = 0;
      }
    }
  }, {
    key: "render",
    value: function render() {
      var msg = this.props.added ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(_components_Message__WEBPACK_IMPORTED_MODULE_11__["default"], {
        show: this.state.showMsg,
        msg: "Task Added Successfully ... !!!",
        setShow: this.handleMsg
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 97,
        columnNumber: 42
      }, this) : '';
      var data = {};

      if (this.props.id !== 0) {
        data = {
          name: this.props.name,
          date: this.props.date,
          id: this.props.id
        };
      }

      return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(react__WEBPACK_IMPORTED_MODULE_8___default.a.Fragment, {
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(_components_TodoForm__WEBPACK_IMPORTED_MODULE_10__["default"], {
          data: data,
          validate: this.state.errorNameMsg,
          handleSubmit: this.handleSubmit,
          handleChange: this.handleChange
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 110,
          columnNumber: 13
        }, this), msg]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 108,
        columnNumber: 13
      }, this);
    }
  }]);

  return TodoAppBuilder;
}(react__WEBPACK_IMPORTED_MODULE_8__["Component"]);

var mapStateToProps = function mapStateToProps(state) {
  console.log(state);
  return {
    name: state.todoTask.name,
    date: state.todoTask.date,
    id: state.todoTask.id,
    added: state.todoTask.added,
    edited: state.todoTask.edited,
    error: state.todoTask.error,
    loading: state.todoTask.loading
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    onSubmitTask: function onSubmitTask(task) {
      return dispatch(_store_actions_index__WEBPACK_IMPORTED_MODULE_12__["initAddTask"](task));
    }
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_9__["connect"])(mapStateToProps, mapDispatchToProps)(TodoAppBuilder)); // export default (TodoAppBuilder);

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vY29udGFpbmVycy9Ub2RvQXBwQnVpbGRlci5qcyJdLCJuYW1lcyI6WyJUb2RvQXBwQnVpbGRlciIsIm5hbWUiLCJkYXRlIiwibG9hZGluZyIsImVycm9yIiwic2hvd01zZyIsInZhbGlkYXRlIiwiZXJyb3JOYW1lTXNnIiwiZXZlbnQiLCJjb25zb2xlIiwibG9nIiwicHJldmVudERlZmF1bHQiLCJ0YXNrIiwic3RhdGUiLCJjb21wbGV0ZSIsImFjdGl2ZSIsImRvY3VtZW50IiwiZ2V0RWxlbWVudEJ5SWQiLCJ2YWx1ZSIsImlkIiwic2V0U3RhdGUiLCJwcm9wcyIsIm9uU3VibWl0VGFzayIsInRhcmdldCIsInR5cGUiLCJsZXR0ZXJzIiwicmVzZXQiLCJlZGl0ZWQiLCJib2R5Iiwic2Nyb2xsVG9wIiwibXNnIiwiYWRkZWQiLCJoYW5kbGVNc2ciLCJkYXRhIiwiaGFuZGxlU3VibWl0IiwiaGFuZGxlQ2hhbmdlIiwiQ29tcG9uZW50IiwibWFwU3RhdGVUb1Byb3BzIiwidG9kb1Rhc2siLCJtYXBEaXNwYXRjaFRvUHJvcHMiLCJkaXNwYXRjaCIsImFjdGlvbnMiLCJjb25uZWN0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0lBSU1BLGM7Ozs7Ozs7Ozs7Ozs7Ozs7Z05BQ007QUFDSkMsVUFBSSxFQUFHLEVBREg7QUFFSkMsVUFBSSxFQUFHLEVBRkg7QUFHSkMsYUFBTyxFQUFHLEtBSE47QUFJSkMsV0FBSyxFQUFHLEtBSko7QUFLSkMsYUFBTyxFQUFHLElBTE47QUFNSkMsY0FBUSxFQUFHLEtBTlA7QUFPSkMsa0JBQVksRUFBRTtBQVBWLEs7O3VOQTJCTyxVQUFDQyxLQUFELEVBQVc7QUFDdEJDLGFBQU8sQ0FBQ0MsR0FBUixDQUFZLHVCQUFaO0FBQ0FGLFdBQUssQ0FBQ0csY0FBTjtBQUVBLFVBQUlDLElBQUksR0FBRztBQUNQWCxZQUFJLEVBQUcsTUFBS1ksS0FBTCxDQUFXWixJQURYO0FBRVBDLFlBQUksRUFBRyxNQUFLVyxLQUFMLENBQVdYLElBRlg7QUFHUFksZ0JBQVEsRUFBRyxLQUhKO0FBSVBDLGNBQU0sRUFBRTtBQUpELE9BQVg7O0FBTUEsVUFBR0MsUUFBUSxDQUFDQyxjQUFULENBQXdCLFFBQXhCLEVBQWtDQyxLQUFsQyxJQUEyQyxFQUE5QyxFQUFpRDtBQUM3Q04sWUFBSSxtQ0FBT0EsSUFBUDtBQUFZTyxZQUFFLEVBQUdILFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixRQUF4QixFQUFrQ0M7QUFBbkQsVUFBSjtBQUNIOztBQUNBLFlBQUtFLFFBQUwsaUNBQ1UsTUFBS1AsS0FEZjtBQUVPUixlQUFPLEVBQUc7QUFGakI7O0FBSUEsVUFBRyxNQUFLUSxLQUFMLENBQVdQLFFBQWQsRUFBdUI7QUFDcEIsY0FBS2UsS0FBTCxDQUFXQyxZQUFYLENBQXdCVixJQUF4QjtBQUNGO0FBRUwsSzs7dU5BRWMsVUFBQ0osS0FBRCxFQUFXO0FBQ3RCQSxXQUFLLENBQUNHLGNBQU47QUFEc0IsMEJBRUVILEtBQUssQ0FBQ2UsTUFGUjtBQUFBLFVBRWRDLElBRmMsaUJBRWRBLElBRmM7QUFBQSxVQUVSTixLQUZRLGlCQUVSQSxLQUZRO0FBR3RCLFVBQUlPLE9BQU8sR0FBRyxhQUFkLENBSHNCLENBS3RCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNILEs7O29OQUVXLFlBQU07QUFDZCxZQUFLTCxRQUFMLGlDQUNXLE1BQUtQLEtBRGhCO0FBRVFSLGVBQU8sRUFBRztBQUZsQjs7QUFJQVcsY0FBUSxDQUFDQyxjQUFULENBQXdCLFdBQXhCLEVBQXFDUyxLQUFyQztBQUNILEs7Ozs7Ozs7d0NBMUVvQjtBQUNqQixVQUFHLEtBQUtMLEtBQUwsQ0FBV00sTUFBZCxFQUFxQjtBQUNqQixhQUFLUCxRQUFMLGlDQUNPLEtBQUtQLEtBRFo7QUFFSVosY0FBSSxFQUFHLEtBQUtvQixLQUFMLENBQVdwQixJQUZ0QjtBQUdJQyxjQUFJLEVBQUcsS0FBS21CLEtBQUwsQ0FBV25CLElBSHRCO0FBSUlHLGlCQUFPLEVBQUc7QUFKZDtBQU1IO0FBQ0o7Ozt5Q0FFcUI7QUFDbEIsVUFBRyxLQUFLZ0IsS0FBTCxDQUFXTSxNQUFkLEVBQXFCO0FBQ2pCWCxnQkFBUSxDQUFDWSxJQUFULENBQWNDLFNBQWQsR0FBMEIsQ0FBMUI7QUFDSDtBQUNKOzs7NkJBNkRPO0FBQ0osVUFBSUMsR0FBRyxHQUFJLEtBQUtULEtBQUwsQ0FBV1UsS0FBWixnQkFBdUIscUVBQUMsNERBQUQ7QUFBUyxZQUFJLEVBQUUsS0FBS2xCLEtBQUwsQ0FBV1IsT0FBMUI7QUFBbUMsV0FBRyxFQUFDLGlDQUF2QztBQUF5RSxlQUFPLEVBQUUsS0FBSzJCO0FBQXZGO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FBdkIsR0FBK0gsRUFBekk7QUFDQSxVQUFJQyxJQUFJLEdBQUcsRUFBWDs7QUFDQSxVQUFHLEtBQUtaLEtBQUwsQ0FBV0YsRUFBWCxLQUFrQixDQUFyQixFQUF1QjtBQUNuQmMsWUFBSSxHQUFHO0FBQ0hoQyxjQUFJLEVBQUcsS0FBS29CLEtBQUwsQ0FBV3BCLElBRGY7QUFFSEMsY0FBSSxFQUFHLEtBQUttQixLQUFMLENBQVduQixJQUZmO0FBR0hpQixZQUFFLEVBQUcsS0FBS0UsS0FBTCxDQUFXRjtBQUhiLFNBQVA7QUFLSDs7QUFFRCwwQkFDSSxxRUFBQyw0Q0FBRCxDQUFPLFFBQVA7QUFBQSxnQ0FFQSxxRUFBQyw2REFBRDtBQUFVLGNBQUksRUFBRWMsSUFBaEI7QUFBc0Isa0JBQVEsRUFBRSxLQUFLcEIsS0FBTCxDQUFXTixZQUEzQztBQUF5RCxzQkFBWSxFQUFFLEtBQUsyQixZQUE1RTtBQUEwRixzQkFBWSxFQUFFLEtBQUtDO0FBQTdHO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBRkEsRUFHQ0wsR0FIRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FESjtBQU9IOzs7O0VBekd3Qk0sK0M7O0FBNkc3QixJQUFNQyxlQUFlLEdBQUcsU0FBbEJBLGVBQWtCLENBQUF4QixLQUFLLEVBQUk7QUFDN0JKLFNBQU8sQ0FBQ0MsR0FBUixDQUFZRyxLQUFaO0FBQ0EsU0FBTztBQUNIWixRQUFJLEVBQUVZLEtBQUssQ0FBQ3lCLFFBQU4sQ0FBZXJDLElBRGxCO0FBRUhDLFFBQUksRUFBR1csS0FBSyxDQUFDeUIsUUFBTixDQUFlcEMsSUFGbkI7QUFHSGlCLE1BQUUsRUFBR04sS0FBSyxDQUFDeUIsUUFBTixDQUFlbkIsRUFIakI7QUFJSFksU0FBSyxFQUFHbEIsS0FBSyxDQUFDeUIsUUFBTixDQUFlUCxLQUpwQjtBQUtISixVQUFNLEVBQUdkLEtBQUssQ0FBQ3lCLFFBQU4sQ0FBZVgsTUFMckI7QUFNSHZCLFNBQUssRUFBRVMsS0FBSyxDQUFDeUIsUUFBTixDQUFlbEMsS0FObkI7QUFPSEQsV0FBTyxFQUFFVSxLQUFLLENBQUN5QixRQUFOLENBQWVuQztBQVByQixHQUFQO0FBU0gsQ0FYRDs7QUFhQSxJQUFNb0Msa0JBQWtCLEdBQUcsU0FBckJBLGtCQUFxQixDQUFBQyxRQUFRLEVBQUk7QUFDbkMsU0FBTztBQUNIbEIsZ0JBQVksRUFBRSxzQkFBQ1YsSUFBRDtBQUFBLGFBQVU0QixRQUFRLENBQUNDLGlFQUFBLENBQW9CN0IsSUFBcEIsQ0FBRCxDQUFsQjtBQUFBO0FBRFgsR0FBUDtBQUdILENBSkQ7O0FBT2U4QiwwSEFBTyxDQUFDTCxlQUFELEVBQWtCRSxrQkFBbEIsQ0FBUCxDQUE2Q3ZDLGNBQTdDLENBQWYsRSxDQUNBIiwiZmlsZSI6InN0YXRpYy93ZWJwYWNrL3BhZ2VzL2luZGV4LjkwZTM0ZjMyMDNmNjRiNTNmOTRmLmhvdC11cGRhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QgLCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IHsgY29ubmVjdCB9IGZyb20gJ3JlYWN0LXJlZHV4JztcclxuaW1wb3J0IFRvZG9Gb3JtIGZyb20gJy4uL2NvbXBvbmVudHMvVG9kb0Zvcm0nO1xyXG5pbXBvcnQgTWVzc2FnZSBmcm9tICcuLi9jb21wb25lbnRzL01lc3NhZ2UnO1xyXG5pbXBvcnQgKiBhcyBhY3Rpb25zIGZyb20gJy4uL3N0b3JlL2FjdGlvbnMvaW5kZXgnO1xyXG5cclxuXHJcblxyXG5jbGFzcyBUb2RvQXBwQnVpbGRlciBleHRlbmRzIENvbXBvbmVudCB7XHJcbiAgICBzdGF0ZSA9IHtcclxuICAgICAgICBuYW1lIDogJycsXHJcbiAgICAgICAgZGF0ZSA6ICcnLFxyXG4gICAgICAgIGxvYWRpbmcgOiBmYWxzZSxcclxuICAgICAgICBlcnJvciA6IGZhbHNlLFxyXG4gICAgICAgIHNob3dNc2cgOiB0cnVlLFxyXG4gICAgICAgIHZhbGlkYXRlIDogZmFsc2UsXHJcbiAgICAgICAgZXJyb3JOYW1lTXNnIDonJ1xyXG4gICAgfVxyXG5cclxuICAgIGNvbXBvbmVudERpZE1vdW50ICgpIHtcclxuICAgICAgICBpZih0aGlzLnByb3BzLmVkaXRlZCl7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgLi4udGhpcy5zdGF0ZSxcclxuICAgICAgICAgICAgICAgIG5hbWUgOiB0aGlzLnByb3BzLm5hbWUsXHJcbiAgICAgICAgICAgICAgICBkYXRlIDogdGhpcy5wcm9wcy5kYXRlLFxyXG4gICAgICAgICAgICAgICAgc2hvd01zZyA6IHRydWVcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGNvbXBvbmVudERpZFVwZGF0ZSAoKSB7XHJcbiAgICAgICAgaWYodGhpcy5wcm9wcy5lZGl0ZWQpe1xyXG4gICAgICAgICAgICBkb2N1bWVudC5ib2R5LnNjcm9sbFRvcCA9IDA7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGhhbmRsZVN1Ym1pdCA9IChldmVudCkgPT4ge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKCc9PSBoZXJlIGluIEJ1aWxkZXI9PT0nKTtcclxuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuICAgICAgICBsZXQgdGFzayA9IHtcclxuICAgICAgICAgICAgbmFtZSA6IHRoaXMuc3RhdGUubmFtZSxcclxuICAgICAgICAgICAgZGF0ZSA6IHRoaXMuc3RhdGUuZGF0ZSxcclxuICAgICAgICAgICAgY29tcGxldGUgOiBmYWxzZSxcclxuICAgICAgICAgICAgYWN0aXZlOiB0cnVlXHJcbiAgICAgICAgfTtcclxuICAgICAgICBpZihkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInRhc2tJZFwiKS52YWx1ZSAhPSAnJyl7XHJcbiAgICAgICAgICAgIHRhc2sgPSB7Li4udGFzayxpZCA6IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwidGFza0lkXCIpLnZhbHVlfTtcclxuICAgICAgICB9XHJcbiAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgLi4udGhpcy5zdGF0ZSxcclxuICAgICAgICAgICAgICAgIHNob3dNc2cgOiB0cnVlXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICBpZih0aGlzLnN0YXRlLnZhbGlkYXRlKXtcclxuICAgICAgICAgICAgdGhpcy5wcm9wcy5vblN1Ym1pdFRhc2sodGFzayk7XHJcbiAgICAgICAgIH1cclxuXHJcbiAgICB9XHJcblxyXG4gICAgaGFuZGxlQ2hhbmdlID0gKGV2ZW50KSA9PiB7XHJcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICBjb25zdCB7IHR5cGUsIHZhbHVlIH0gPSBldmVudC50YXJnZXQ7XHJcbiAgICAgICAgbGV0IGxldHRlcnMgPSAvXltBLVphLXpdKyQvO1xyXG5cclxuICAgICAgICAvLyBpZihldmVudC50YXJnZXQudHlwZSA9PSAndGV4dCcpIHtcclxuICAgICAgICAvLyAgICAgaWYoIXZhbHVlLm1hdGNoKGxldHRlcnMpKXtcclxuICAgICAgICAvLyAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgIC8vICAgICAgICAgICAgICAuLi50aGlzLnN0YXRlLFxyXG4gICAgICAgIC8vICAgICAgICAgICAgIHZhbGlkYXRlIDogZmFsc2UsXHJcbiAgICAgICAgLy8gICAgICAgICAgICAgZXJyb3JOYW1lTXNnIDogJ1BsZWFzZSBFbnRlciBMZXR0ZXJzIE9ubHkuJ1xyXG4gICAgICAgIC8vICAgICAgICAgfSk7XHJcbiAgICAgICAgLy8gICAgIH1lbHNle1xyXG4gICAgICAgIC8vICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgLy8gICAgICAgICAgICAgLi4udGhpcy5zdGF0ZSxcclxuICAgICAgICAvLyAgICAgICAgICAgICB2YWxpZGF0ZSA6IHRydWUsXHJcbiAgICAgICAgLy8gICAgICAgICAgICAgbmFtZSA6IGV2ZW50LnRhcmdldC52YWx1ZVxyXG4gICAgICAgIC8vICAgICAgICAgfSk7XHJcbiAgICAgICAgLy8gICAgIH1cclxuICAgICAgICAvLyB9XHJcbiAgICAgICAgLy8gaWYoZXZlbnQudGFyZ2V0LnR5cGUgPT09ICdkYXRlJyl7XHJcbiAgICAgICAgLy8gICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgIC8vICAgICAgICAgLi4udGhpcy5zdGF0ZSxcclxuICAgICAgICAvLyAgICAgICAgIGRhdGUgOiBldmVudC50YXJnZXQudmFsdWVcclxuICAgICAgICAvLyAgICAgfSk7XHJcbiAgICAgICAgLy8gfVxyXG4gICAgfVxyXG5cclxuICAgIGhhbmRsZU1zZyA9ICgpID0+IHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIC4uLnRoaXMuc3RhdGUsXHJcbiAgICAgICAgICAgICAgICBzaG93TXNnIDogZmFsc2VcclxuICAgICAgICB9KTtcclxuICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInRvZG8tZm9ybVwiKS5yZXNldCgpO1xyXG4gICAgfVxyXG5cclxuICAgIHJlbmRlcigpe1xyXG4gICAgICAgIGxldCBtc2cgPSAodGhpcy5wcm9wcy5hZGRlZCApID8gKDxNZXNzYWdlIHNob3c9e3RoaXMuc3RhdGUuc2hvd01zZ30gbXNnPVwiVGFzayBBZGRlZCBTdWNjZXNzZnVsbHkgLi4uICEhIVwiIHNldFNob3c9e3RoaXMuaGFuZGxlTXNnfSAvPikgOiAnJztcclxuICAgICAgICBsZXQgZGF0YSA9IHt9O1xyXG4gICAgICAgIGlmKHRoaXMucHJvcHMuaWQgIT09IDApe1xyXG4gICAgICAgICAgICBkYXRhID0ge1xyXG4gICAgICAgICAgICAgICAgbmFtZSA6IHRoaXMucHJvcHMubmFtZSxcclxuICAgICAgICAgICAgICAgIGRhdGUgOiB0aGlzLnByb3BzLmRhdGUsXHJcbiAgICAgICAgICAgICAgICBpZCA6IHRoaXMucHJvcHMuaWRcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPFJlYWN0LkZyYWdtZW50PlxyXG5cclxuICAgICAgICAgICAgPFRvZG9Gb3JtIGRhdGE9e2RhdGF9IHZhbGlkYXRlPXt0aGlzLnN0YXRlLmVycm9yTmFtZU1zZ30gaGFuZGxlU3VibWl0PXt0aGlzLmhhbmRsZVN1Ym1pdH0gaGFuZGxlQ2hhbmdlPXt0aGlzLmhhbmRsZUNoYW5nZX0vPlxyXG4gICAgICAgICAgICB7bXNnfVxyXG4gICAgICAgICAgICA8L1JlYWN0LkZyYWdtZW50PlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbn1cclxuXHJcblxyXG5jb25zdCBtYXBTdGF0ZVRvUHJvcHMgPSBzdGF0ZSA9PiB7XHJcbiAgICBjb25zb2xlLmxvZyhzdGF0ZSk7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICAgIG5hbWU6IHN0YXRlLnRvZG9UYXNrLm5hbWUsXHJcbiAgICAgICAgZGF0ZSA6IHN0YXRlLnRvZG9UYXNrLmRhdGUsXHJcbiAgICAgICAgaWQgOiBzdGF0ZS50b2RvVGFzay5pZCxcclxuICAgICAgICBhZGRlZCA6IHN0YXRlLnRvZG9UYXNrLmFkZGVkLFxyXG4gICAgICAgIGVkaXRlZCA6IHN0YXRlLnRvZG9UYXNrLmVkaXRlZCxcclxuICAgICAgICBlcnJvcjogc3RhdGUudG9kb1Rhc2suZXJyb3IsXHJcbiAgICAgICAgbG9hZGluZzogc3RhdGUudG9kb1Rhc2subG9hZGluZ1xyXG4gICAgfVxyXG59O1xyXG5cclxuY29uc3QgbWFwRGlzcGF0Y2hUb1Byb3BzID0gZGlzcGF0Y2ggPT4ge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgICBvblN1Ym1pdFRhc2s6ICh0YXNrKSA9PiBkaXNwYXRjaChhY3Rpb25zLmluaXRBZGRUYXNrKHRhc2spKVxyXG4gICAgfTtcclxufTtcclxuXHJcblxyXG5leHBvcnQgZGVmYXVsdCBjb25uZWN0KG1hcFN0YXRlVG9Qcm9wcywgbWFwRGlzcGF0Y2hUb1Byb3BzKShUb2RvQXBwQnVpbGRlcik7XHJcbi8vIGV4cG9ydCBkZWZhdWx0IChUb2RvQXBwQnVpbGRlcik7XHJcbiJdLCJzb3VyY2VSb290IjoiIn0=