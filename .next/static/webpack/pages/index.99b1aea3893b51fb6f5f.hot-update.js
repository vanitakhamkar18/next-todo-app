webpackHotUpdate_N_E("pages/index",{

/***/ "./containers/TodoListBuilder.js":
/*!***************************************!*\
  !*** ./containers/TodoListBuilder.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var _babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/esm/classCallCheck */ "./node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/esm/createClass */ "./node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/esm/assertThisInitialized */ "./node_modules/@babel/runtime/helpers/esm/assertThisInitialized.js");
/* harmony import */ var _babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/esm/inherits */ "./node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime/helpers/esm/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _components_TodoList__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../components/TodoList */ "./components/TodoList.js");
/* harmony import */ var _hoc_ErrorBoundary__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../hoc/ErrorBoundary */ "./hoc/ErrorBoundary.js");
/* harmony import */ var react_bootstrap_Spinner__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! react-bootstrap/Spinner */ "./node_modules/react-bootstrap/esm/Spinner.js");
/* harmony import */ var react_bootstrap_Alert__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! react-bootstrap/Alert */ "./node_modules/react-bootstrap/esm/Alert.js");
/* harmony import */ var _store_actions_index__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../store/actions/index */ "./store/actions/index.js");








var _jsxFileName = "D:\\PERSONAL\\Vanita\\React\\test\\todo-next\\containers\\TodoListBuilder.js";

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return Object(_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }









var TodoListBuilder = /*#__PURE__*/function (_Component) {
  Object(_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_3__["default"])(TodoListBuilder, _Component);

  var _super = _createSuper(TodoListBuilder);

  function TodoListBuilder() {
    var _this;

    Object(_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, TodoListBuilder);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleSubmit", function (event) {
      event.preventDefault();
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleChange", function (id, status, val) {
      _this.props.onChangeStatusTask(id, status, val);
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleFilter", function (event) {
      _this.props.onFilterTask(event.target.value);
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleEdit", function (id) {
      _this.props.onEditTask(id);
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleDelete", function (id) {
      _this.props.onDeleteTask(id);
    });

    return _this;
  }

  Object(_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(TodoListBuilder, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.props.onFetchTask();
    }
  }, {
    key: "render",
    value: function render() {
      var todoList = /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(react_bootstrap_Alert__WEBPACK_IMPORTED_MODULE_13__["default"], {
        variant: "info",
        className: "mt-3",
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])("p", {
          children: "No Task to show, Please add task to show list of the task added. ..!!"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 38,
          columnNumber: 27
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 37,
        columnNumber: 24
      }, this);

      if (this.props.error) {
        todoList = /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(react_bootstrap_Alert__WEBPACK_IMPORTED_MODULE_13__["default"], {
          variant: "info",
          className: "mt-3",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])("p", {
            children: "SomeThing Went Wrong. Please Try Again Later ..!!"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 44,
            columnNumber: 27
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 43,
          columnNumber: 24
        }, this);
      } else if (this.props.data && this.props.data.length > 0) {
        todoList = /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(_components_TodoList__WEBPACK_IMPORTED_MODULE_10__["default"], {
          data: this.props.data,
          edit: this.handleEdit,
          "delete": this.handleDelete,
          change: this.handleChange,
          filter: this.handleFilter
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 49,
          columnNumber: 24
        }, this);
      }

      var tpl = this.props.loading ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(react_bootstrap_Spinner__WEBPACK_IMPORTED_MODULE_12__["default"], {
        className: "mt-60",
        animation: "border",
        variant: "info"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 57,
        columnNumber: 42
      }, this) : todoList;
      return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(_hoc_ErrorBoundary__WEBPACK_IMPORTED_MODULE_11__["default"], {
        children: tpl
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 60,
        columnNumber: 9
      }, this);
    }
  }]);

  return TodoListBuilder;
}(react__WEBPACK_IMPORTED_MODULE_8__["Component"]);

var mapStateToProps = function mapStateToProps(state) {
  console.log(state);
  return {
    data: state.todoTask.data,
    error: state.todoTask.error,
    loading: state.todoTask.loading
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    onFetchTask: function onFetchTask() {
      return dispatch(_store_actions_index__WEBPACK_IMPORTED_MODULE_14__["fetchTask"]());
    },
    onDeleteTask: function onDeleteTask(id) {
      return dispatch(_store_actions_index__WEBPACK_IMPORTED_MODULE_14__["initDeleteTask"](id));
    },
    onEditTask: function onEditTask(id) {
      return dispatch(_store_actions_index__WEBPACK_IMPORTED_MODULE_14__["initEditTask"](id));
    },
    onChangeStatusTask: function onChangeStatusTask(id, status, val) {
      return dispatch(_store_actions_index__WEBPACK_IMPORTED_MODULE_14__["initChangeStatusTask"](id, status, val));
    },
    onFilterTask: function onFilterTask(filter) {
      return dispatch(_store_actions_index__WEBPACK_IMPORTED_MODULE_14__["initFilterTask"](filter));
    }
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_9__["connect"])(mapStateToProps, mapDispatchToProps)(TodoListBuilder)); //export default (TodoListBuilder);

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vY29udGFpbmVycy9Ub2RvTGlzdEJ1aWxkZXIuanMiXSwibmFtZXMiOlsiVG9kb0xpc3RCdWlsZGVyIiwiZXZlbnQiLCJwcmV2ZW50RGVmYXVsdCIsImlkIiwic3RhdHVzIiwidmFsIiwicHJvcHMiLCJvbkNoYW5nZVN0YXR1c1Rhc2siLCJvbkZpbHRlclRhc2siLCJ0YXJnZXQiLCJ2YWx1ZSIsIm9uRWRpdFRhc2siLCJvbkRlbGV0ZVRhc2siLCJvbkZldGNoVGFzayIsInRvZG9MaXN0IiwiZXJyb3IiLCJkYXRhIiwibGVuZ3RoIiwiaGFuZGxlRWRpdCIsImhhbmRsZURlbGV0ZSIsImhhbmRsZUNoYW5nZSIsImhhbmRsZUZpbHRlciIsInRwbCIsImxvYWRpbmciLCJDb21wb25lbnQiLCJtYXBTdGF0ZVRvUHJvcHMiLCJzdGF0ZSIsImNvbnNvbGUiLCJsb2ciLCJ0b2RvVGFzayIsIm1hcERpc3BhdGNoVG9Qcm9wcyIsImRpc3BhdGNoIiwiYWN0aW9ucyIsImZpbHRlciIsImNvbm5lY3QiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztJQUdNQSxlOzs7Ozs7Ozs7Ozs7Ozs7O3VOQU1hLFVBQUNDLEtBQUQsRUFBVztBQUN0QkEsV0FBSyxDQUFDQyxjQUFOO0FBQ0gsSzs7dU5BRWMsVUFBQ0MsRUFBRCxFQUFJQyxNQUFKLEVBQVdDLEdBQVgsRUFBbUI7QUFDOUIsWUFBS0MsS0FBTCxDQUFXQyxrQkFBWCxDQUE4QkosRUFBOUIsRUFBaUNDLE1BQWpDLEVBQXdDQyxHQUF4QztBQUNILEs7O3VOQUVjLFVBQUNKLEtBQUQsRUFBVztBQUN0QixZQUFLSyxLQUFMLENBQVdFLFlBQVgsQ0FBd0JQLEtBQUssQ0FBQ1EsTUFBTixDQUFhQyxLQUFyQztBQUNILEs7O3FOQUVZLFVBQUNQLEVBQUQsRUFBUTtBQUNqQixZQUFLRyxLQUFMLENBQVdLLFVBQVgsQ0FBc0JSLEVBQXRCO0FBQ0gsSzs7dU5BRWMsVUFBQ0EsRUFBRCxFQUFRO0FBQ25CLFlBQUtHLEtBQUwsQ0FBV00sWUFBWCxDQUF3QlQsRUFBeEI7QUFDSCxLOzs7Ozs7O3dDQXRCb0I7QUFDakIsV0FBS0csS0FBTCxDQUFXTyxXQUFYO0FBQ0g7Ozs2QkFzQk87QUFDSixVQUFJQyxRQUFRLGdCQUFHLHFFQUFDLDhEQUFEO0FBQU8sZUFBTyxFQUFDLE1BQWY7QUFBc0IsaUJBQVMsRUFBQyxNQUFoQztBQUFBLCtCQUNHO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREg7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQUFmOztBQUtBLFVBQUcsS0FBS1IsS0FBTCxDQUFXUyxLQUFkLEVBQW9CO0FBQ2hCRCxnQkFBUSxnQkFBRyxxRUFBQyw4REFBRDtBQUFPLGlCQUFPLEVBQUMsTUFBZjtBQUFzQixtQkFBUyxFQUFDLE1BQWhDO0FBQUEsaUNBQ0c7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESDtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUFYO0FBS0gsT0FORCxNQU1NLElBQUcsS0FBS1IsS0FBTCxDQUFXVSxJQUFYLElBQW1CLEtBQUtWLEtBQUwsQ0FBV1UsSUFBWCxDQUFnQkMsTUFBaEIsR0FBeUIsQ0FBL0MsRUFBaUQ7QUFDbkRILGdCQUFRLGdCQUFHLHFFQUFDLDZEQUFEO0FBQ0MsY0FBSSxFQUFFLEtBQUtSLEtBQUwsQ0FBV1UsSUFEbEI7QUFFQyxjQUFJLEVBQUUsS0FBS0UsVUFGWjtBQUdDLG9CQUFRLEtBQUtDLFlBSGQ7QUFJQyxnQkFBTSxFQUFFLEtBQUtDLFlBSmQ7QUFLQyxnQkFBTSxFQUFFLEtBQUtDO0FBTGQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFBWDtBQU9IOztBQUNELFVBQUlDLEdBQUcsR0FBRyxLQUFLaEIsS0FBTCxDQUFXaUIsT0FBWCxnQkFBdUIscUVBQUMsZ0VBQUQ7QUFBUyxpQkFBUyxFQUFDLE9BQW5CO0FBQTJCLGlCQUFTLEVBQUMsUUFBckM7QUFBOEMsZUFBTyxFQUFDO0FBQXREO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FBdkIsR0FBMEZULFFBQXBHO0FBRUEsMEJBQ0EscUVBQUMsMkRBQUQ7QUFBQSxrQkFDS1E7QUFETDtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBREE7QUFLSDs7OztFQXREeUJFLCtDOztBQXlEOUIsSUFBTUMsZUFBZSxHQUFHLFNBQWxCQSxlQUFrQixDQUFBQyxLQUFLLEVBQUk7QUFDN0JDLFNBQU8sQ0FBQ0MsR0FBUixDQUFZRixLQUFaO0FBQ0EsU0FBTztBQUNIVixRQUFJLEVBQUdVLEtBQUssQ0FBQ0csUUFBTixDQUFlYixJQURuQjtBQUVIRCxTQUFLLEVBQUVXLEtBQUssQ0FBQ0csUUFBTixDQUFlZCxLQUZuQjtBQUdIUSxXQUFPLEVBQUVHLEtBQUssQ0FBQ0csUUFBTixDQUFlTjtBQUhyQixHQUFQO0FBS0gsQ0FQRDs7QUFTQSxJQUFNTyxrQkFBa0IsR0FBRyxTQUFyQkEsa0JBQXFCLENBQUFDLFFBQVEsRUFBSTtBQUNuQyxTQUFPO0FBQ0hsQixlQUFXLEVBQUc7QUFBQSxhQUFNa0IsUUFBUSxDQUFDQywrREFBQSxFQUFELENBQWQ7QUFBQSxLQURYO0FBRUhwQixnQkFBWSxFQUFHLHNCQUFDVCxFQUFEO0FBQUEsYUFBUTRCLFFBQVEsQ0FBQ0Msb0VBQUEsQ0FBdUI3QixFQUF2QixDQUFELENBQWhCO0FBQUEsS0FGWjtBQUdIUSxjQUFVLEVBQUcsb0JBQUNSLEVBQUQ7QUFBQSxhQUFRNEIsUUFBUSxDQUFDQyxrRUFBQSxDQUFxQjdCLEVBQXJCLENBQUQsQ0FBaEI7QUFBQSxLQUhWO0FBSUhJLHNCQUFrQixFQUFHLDRCQUFDSixFQUFELEVBQUlDLE1BQUosRUFBV0MsR0FBWDtBQUFBLGFBQW1CMEIsUUFBUSxDQUFDQywwRUFBQSxDQUE2QjdCLEVBQTdCLEVBQWdDQyxNQUFoQyxFQUF1Q0MsR0FBdkMsQ0FBRCxDQUEzQjtBQUFBLEtBSmxCO0FBS0hHLGdCQUFZLEVBQUcsc0JBQUN5QixNQUFEO0FBQUEsYUFBWUYsUUFBUSxDQUFDQyxvRUFBQSxDQUF1QkMsTUFBdkIsQ0FBRCxDQUFwQjtBQUFBO0FBTFosR0FBUDtBQU9ILENBUkQ7O0FBVWVDLDBIQUFPLENBQUNULGVBQUQsRUFBaUJLLGtCQUFqQixDQUFQLENBQTRDOUIsZUFBNUMsQ0FBZixFLENBQ0EiLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvaW5kZXguOTliMWFlYTM4OTNiNTFmYjZmNWYuaG90LXVwZGF0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCAsIHsgQ29tcG9uZW50IH0gZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgeyBjb25uZWN0IH0gZnJvbSAncmVhY3QtcmVkdXgnO1xyXG5pbXBvcnQgVG9kb0xpc3QgZnJvbSAnLi4vY29tcG9uZW50cy9Ub2RvTGlzdCc7XHJcbmltcG9ydCBFcnJvckJvdW5kYXJ5IGZyb20gJy4uL2hvYy9FcnJvckJvdW5kYXJ5JztcclxuaW1wb3J0IFNwaW5uZXIgZnJvbSAncmVhY3QtYm9vdHN0cmFwL1NwaW5uZXInO1xyXG5pbXBvcnQgQWxlcnQgZnJvbSAncmVhY3QtYm9vdHN0cmFwL0FsZXJ0JztcclxuaW1wb3J0ICogYXMgYWN0aW9ucyBmcm9tICcuLi9zdG9yZS9hY3Rpb25zL2luZGV4JztcclxuXHJcblxyXG5jbGFzcyBUb2RvTGlzdEJ1aWxkZXIgZXh0ZW5kcyBDb21wb25lbnQge1xyXG5cclxuICAgIGNvbXBvbmVudERpZE1vdW50ICgpIHtcclxuICAgICAgICB0aGlzLnByb3BzLm9uRmV0Y2hUYXNrKCk7XHJcbiAgICB9XHJcblxyXG4gICAgaGFuZGxlU3VibWl0ID0gKGV2ZW50KSA9PiB7XHJcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgIH1cclxuXHJcbiAgICBoYW5kbGVDaGFuZ2UgPSAoaWQsc3RhdHVzLHZhbCkgPT4ge1xyXG4gICAgICAgIHRoaXMucHJvcHMub25DaGFuZ2VTdGF0dXNUYXNrKGlkLHN0YXR1cyx2YWwpO1xyXG4gICAgfVxyXG5cclxuICAgIGhhbmRsZUZpbHRlciA9IChldmVudCkgPT4ge1xyXG4gICAgICAgIHRoaXMucHJvcHMub25GaWx0ZXJUYXNrKGV2ZW50LnRhcmdldC52YWx1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgaGFuZGxlRWRpdCA9IChpZCkgPT4ge1xyXG4gICAgICAgIHRoaXMucHJvcHMub25FZGl0VGFzayhpZCk7XHJcbiAgICB9XHJcblxyXG4gICAgaGFuZGxlRGVsZXRlID0gKGlkKSA9PiB7XHJcbiAgICAgICAgdGhpcy5wcm9wcy5vbkRlbGV0ZVRhc2soaWQpO1xyXG4gICAgfVxyXG5cclxuICAgIHJlbmRlcigpe1xyXG4gICAgICAgIGxldCB0b2RvTGlzdCA9IDxBbGVydCB2YXJpYW50PVwiaW5mb1wiIGNsYXNzTmFtZT1cIm10LTNcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8cD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIE5vIFRhc2sgdG8gc2hvdywgUGxlYXNlIGFkZCB0YXNrIHRvIHNob3cgbGlzdCBvZiB0aGUgdGFzayBhZGRlZC4gLi4hIVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvcD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9BbGVydD47XHJcbiAgICAgICAgaWYodGhpcy5wcm9wcy5lcnJvcil7XHJcbiAgICAgICAgICAgIHRvZG9MaXN0ID0gPEFsZXJ0IHZhcmlhbnQ9XCJpbmZvXCIgY2xhc3NOYW1lPVwibXQtM1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxwPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICBTb21lVGhpbmcgV2VudCBXcm9uZy4gUGxlYXNlIFRyeSBBZ2FpbiBMYXRlciAuLiEhXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9wPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L0FsZXJ0PjtcclxuICAgICAgICB9ZWxzZSBpZih0aGlzLnByb3BzLmRhdGEgJiYgdGhpcy5wcm9wcy5kYXRhLmxlbmd0aCA+IDApe1xyXG4gICAgICAgICAgICB0b2RvTGlzdCA9IDxUb2RvTGlzdFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkYXRhPXt0aGlzLnByb3BzLmRhdGF9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGVkaXQ9e3RoaXMuaGFuZGxlRWRpdH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgZGVsZXRlPXt0aGlzLmhhbmRsZURlbGV0ZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgY2hhbmdlPXt0aGlzLmhhbmRsZUNoYW5nZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgZmlsdGVyPXt0aGlzLmhhbmRsZUZpbHRlcn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgLz47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxldCB0cGwgPSB0aGlzLnByb3BzLmxvYWRpbmcgPyAgKDxTcGlubmVyIGNsYXNzTmFtZT1cIm10LTYwXCIgYW5pbWF0aW9uPVwiYm9yZGVyXCIgdmFyaWFudD1cImluZm9cIiAvPikgOiB0b2RvTGlzdCA7XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgPEVycm9yQm91bmRhcnk+XHJcbiAgICAgICAgICAgIHt0cGx9XHJcbiAgICAgICAgPC9FcnJvckJvdW5kYXJ5PlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbn1cclxuXHJcbmNvbnN0IG1hcFN0YXRlVG9Qcm9wcyA9IHN0YXRlID0+IHtcclxuICAgIGNvbnNvbGUubG9nKHN0YXRlKTtcclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgZGF0YSA6IHN0YXRlLnRvZG9UYXNrLmRhdGEsXHJcbiAgICAgICAgZXJyb3I6IHN0YXRlLnRvZG9UYXNrLmVycm9yLFxyXG4gICAgICAgIGxvYWRpbmc6IHN0YXRlLnRvZG9UYXNrLmxvYWRpbmdcclxuICAgIH1cclxufTtcclxuXHJcbmNvbnN0IG1hcERpc3BhdGNoVG9Qcm9wcyA9IGRpc3BhdGNoID0+IHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgb25GZXRjaFRhc2sgOiAoKSA9PiBkaXNwYXRjaChhY3Rpb25zLmZldGNoVGFzaygpKSxcclxuICAgICAgICBvbkRlbGV0ZVRhc2sgOiAoaWQpID0+IGRpc3BhdGNoKGFjdGlvbnMuaW5pdERlbGV0ZVRhc2soaWQpKSxcclxuICAgICAgICBvbkVkaXRUYXNrIDogKGlkKSA9PiBkaXNwYXRjaChhY3Rpb25zLmluaXRFZGl0VGFzayhpZCkpLFxyXG4gICAgICAgIG9uQ2hhbmdlU3RhdHVzVGFzayA6IChpZCxzdGF0dXMsdmFsKSA9PiBkaXNwYXRjaChhY3Rpb25zLmluaXRDaGFuZ2VTdGF0dXNUYXNrKGlkLHN0YXR1cyx2YWwpKSxcclxuICAgICAgICBvbkZpbHRlclRhc2sgOiAoZmlsdGVyKSA9PiBkaXNwYXRjaChhY3Rpb25zLmluaXRGaWx0ZXJUYXNrKGZpbHRlcikpLFxyXG4gICAgfTtcclxufTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNvbm5lY3QobWFwU3RhdGVUb1Byb3BzLG1hcERpc3BhdGNoVG9Qcm9wcykoVG9kb0xpc3RCdWlsZGVyKTtcclxuLy9leHBvcnQgZGVmYXVsdCAoVG9kb0xpc3RCdWlsZGVyKTtcclxuIl0sInNvdXJjZVJvb3QiOiIifQ==