webpackHotUpdate_N_E("pages/index",{

/***/ "./containers/TodoAppBuilder.js":
/*!**************************************!*\
  !*** ./containers/TodoAppBuilder.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var _babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/esm/classCallCheck */ "./node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/esm/createClass */ "./node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/esm/assertThisInitialized */ "./node_modules/@babel/runtime/helpers/esm/assertThisInitialized.js");
/* harmony import */ var _babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/esm/inherits */ "./node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime/helpers/esm/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _components_TodoForm__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../components/TodoForm */ "./components/TodoForm.js");
/* harmony import */ var _components_Message__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../components/Message */ "./components/Message.js");
/* harmony import */ var _store_actions_index__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../store/actions/index */ "./store/actions/index.js");








var _jsxFileName = "D:\\PERSONAL\\Vanita\\React\\test\\todo-next\\containers\\TodoAppBuilder.js";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return Object(_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }







var TodoAppBuilder = /*#__PURE__*/function (_Component) {
  Object(_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_3__["default"])(TodoAppBuilder, _Component);

  var _super = _createSuper(TodoAppBuilder);

  function TodoAppBuilder() {
    var _this;

    Object(_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, TodoAppBuilder);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "state", {
      name: '',
      date: '',
      loading: false,
      error: false,
      showMsg: true,
      validate: false,
      errorNameMsg: ''
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleSubmit", function (event) {
      console.log('== here in Builder===');
      event.preventDefault();
      var task = {
        name: _this.state.name,
        date: _this.state.date,
        complete: false,
        active: true,
        validate: true
      };

      if (document.getElementById("taskId").value != '') {
        task = _objectSpread(_objectSpread({}, task), {}, {
          id: document.getElementById("taskId").value
        });
      }

      _this.setState(_objectSpread(_objectSpread({}, _this.state), {}, {
        showMsg: true
      }));

      if (_this.state.validate) {
        _this.props.onSubmitTask(task);
      }
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleChange", function (event) {
      event.preventDefault();
      var _event$target = event.target,
          type = _event$target.type,
          value = _event$target.value;
      var letters = /^[A-Za-z]+$/; // if(event.target.type == 'text') {
      //     if(!value.match(letters)){
      //         this.setState({
      //              ...this.state,
      //             validate : false,
      //             errorNameMsg : 'Please Enter Letters Only.'
      //         });
      //     }else{
      //         this.setState({
      //             ...this.state,
      //             validate : true,
      //             name : event.target.value
      //         });
      //     }
      // }
      // if(event.target.type === 'date'){
      //     this.setState({
      //         ...this.state,
      //         date : event.target.value
      //     });
      // }
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleMsg", function () {
      _this.setState(_objectSpread(_objectSpread({}, _this.state), {}, {
        showMsg: false
      }));

      document.getElementById("todo-form").reset();
    });

    return _this;
  }

  Object(_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(TodoAppBuilder, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (this.props.edited) {
        this.setState(_objectSpread(_objectSpread({}, this.state), {}, {
          name: this.props.name,
          date: this.props.date,
          showMsg: true
        }));
      }
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate() {
      if (this.props.edited) {
        document.body.scrollTop = 0;
      }
    }
  }, {
    key: "render",
    value: function render() {
      var msg = this.props.added ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(_components_Message__WEBPACK_IMPORTED_MODULE_11__["default"], {
        show: this.state.showMsg,
        msg: "Task Added Successfully ... !!!",
        setShow: this.handleMsg
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 98,
        columnNumber: 42
      }, this) : '';
      var data = {};

      if (this.props.id !== 0) {
        data = {
          name: this.props.name,
          date: this.props.date,
          id: this.props.id
        };
      }

      return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(react__WEBPACK_IMPORTED_MODULE_8___default.a.Fragment, {
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(_components_TodoForm__WEBPACK_IMPORTED_MODULE_10__["default"], {
          data: data,
          validate: this.state.errorNameMsg,
          handleSubmit: this.handleSubmit,
          handleChange: this.handleChange
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 111,
          columnNumber: 13
        }, this), msg]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 109,
        columnNumber: 13
      }, this);
    }
  }]);

  return TodoAppBuilder;
}(react__WEBPACK_IMPORTED_MODULE_8__["Component"]);

var mapStateToProps = function mapStateToProps(state) {
  console.log(state);
  return {
    name: state.todoTask.name,
    date: state.todoTask.date,
    id: state.todoTask.id,
    added: state.todoTask.added,
    edited: state.todoTask.edited,
    error: state.todoTask.error,
    loading: state.todoTask.loading
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    onSubmitTask: function onSubmitTask(task) {
      return dispatch(_store_actions_index__WEBPACK_IMPORTED_MODULE_12__["initAddTask"](task));
    }
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_9__["connect"])(mapStateToProps, mapDispatchToProps)(TodoAppBuilder)); // export default (TodoAppBuilder);

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vY29udGFpbmVycy9Ub2RvQXBwQnVpbGRlci5qcyJdLCJuYW1lcyI6WyJUb2RvQXBwQnVpbGRlciIsIm5hbWUiLCJkYXRlIiwibG9hZGluZyIsImVycm9yIiwic2hvd01zZyIsInZhbGlkYXRlIiwiZXJyb3JOYW1lTXNnIiwiZXZlbnQiLCJjb25zb2xlIiwibG9nIiwicHJldmVudERlZmF1bHQiLCJ0YXNrIiwic3RhdGUiLCJjb21wbGV0ZSIsImFjdGl2ZSIsImRvY3VtZW50IiwiZ2V0RWxlbWVudEJ5SWQiLCJ2YWx1ZSIsImlkIiwic2V0U3RhdGUiLCJwcm9wcyIsIm9uU3VibWl0VGFzayIsInRhcmdldCIsInR5cGUiLCJsZXR0ZXJzIiwicmVzZXQiLCJlZGl0ZWQiLCJib2R5Iiwic2Nyb2xsVG9wIiwibXNnIiwiYWRkZWQiLCJoYW5kbGVNc2ciLCJkYXRhIiwiaGFuZGxlU3VibWl0IiwiaGFuZGxlQ2hhbmdlIiwiQ29tcG9uZW50IiwibWFwU3RhdGVUb1Byb3BzIiwidG9kb1Rhc2siLCJtYXBEaXNwYXRjaFRvUHJvcHMiLCJkaXNwYXRjaCIsImFjdGlvbnMiLCJjb25uZWN0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0lBSU1BLGM7Ozs7Ozs7Ozs7Ozs7Ozs7Z05BQ007QUFDSkMsVUFBSSxFQUFHLEVBREg7QUFFSkMsVUFBSSxFQUFHLEVBRkg7QUFHSkMsYUFBTyxFQUFHLEtBSE47QUFJSkMsV0FBSyxFQUFHLEtBSko7QUFLSkMsYUFBTyxFQUFHLElBTE47QUFNSkMsY0FBUSxFQUFHLEtBTlA7QUFPSkMsa0JBQVksRUFBRTtBQVBWLEs7O3VOQTJCTyxVQUFDQyxLQUFELEVBQVc7QUFDdEJDLGFBQU8sQ0FBQ0MsR0FBUixDQUFZLHVCQUFaO0FBQ0FGLFdBQUssQ0FBQ0csY0FBTjtBQUVBLFVBQUlDLElBQUksR0FBRztBQUNQWCxZQUFJLEVBQUcsTUFBS1ksS0FBTCxDQUFXWixJQURYO0FBRVBDLFlBQUksRUFBRyxNQUFLVyxLQUFMLENBQVdYLElBRlg7QUFHUFksZ0JBQVEsRUFBRyxLQUhKO0FBSVBDLGNBQU0sRUFBRSxJQUpEO0FBS1BULGdCQUFRLEVBQUM7QUFMRixPQUFYOztBQU9BLFVBQUdVLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixRQUF4QixFQUFrQ0MsS0FBbEMsSUFBMkMsRUFBOUMsRUFBaUQ7QUFDN0NOLFlBQUksbUNBQU9BLElBQVA7QUFBWU8sWUFBRSxFQUFHSCxRQUFRLENBQUNDLGNBQVQsQ0FBd0IsUUFBeEIsRUFBa0NDO0FBQW5ELFVBQUo7QUFDSDs7QUFDQSxZQUFLRSxRQUFMLGlDQUNVLE1BQUtQLEtBRGY7QUFFT1IsZUFBTyxFQUFHO0FBRmpCOztBQUlBLFVBQUcsTUFBS1EsS0FBTCxDQUFXUCxRQUFkLEVBQXVCO0FBQ3BCLGNBQUtlLEtBQUwsQ0FBV0MsWUFBWCxDQUF3QlYsSUFBeEI7QUFDRjtBQUVMLEs7O3VOQUVjLFVBQUNKLEtBQUQsRUFBVztBQUN0QkEsV0FBSyxDQUFDRyxjQUFOO0FBRHNCLDBCQUVFSCxLQUFLLENBQUNlLE1BRlI7QUFBQSxVQUVkQyxJQUZjLGlCQUVkQSxJQUZjO0FBQUEsVUFFUk4sS0FGUSxpQkFFUkEsS0FGUTtBQUd0QixVQUFJTyxPQUFPLEdBQUcsYUFBZCxDQUhzQixDQUt0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDSCxLOztvTkFFVyxZQUFNO0FBQ2QsWUFBS0wsUUFBTCxpQ0FDVyxNQUFLUCxLQURoQjtBQUVRUixlQUFPLEVBQUc7QUFGbEI7O0FBSUFXLGNBQVEsQ0FBQ0MsY0FBVCxDQUF3QixXQUF4QixFQUFxQ1MsS0FBckM7QUFDSCxLOzs7Ozs7O3dDQTNFb0I7QUFDakIsVUFBRyxLQUFLTCxLQUFMLENBQVdNLE1BQWQsRUFBcUI7QUFDakIsYUFBS1AsUUFBTCxpQ0FDTyxLQUFLUCxLQURaO0FBRUlaLGNBQUksRUFBRyxLQUFLb0IsS0FBTCxDQUFXcEIsSUFGdEI7QUFHSUMsY0FBSSxFQUFHLEtBQUttQixLQUFMLENBQVduQixJQUh0QjtBQUlJRyxpQkFBTyxFQUFHO0FBSmQ7QUFNSDtBQUNKOzs7eUNBRXFCO0FBQ2xCLFVBQUcsS0FBS2dCLEtBQUwsQ0FBV00sTUFBZCxFQUFxQjtBQUNqQlgsZ0JBQVEsQ0FBQ1ksSUFBVCxDQUFjQyxTQUFkLEdBQTBCLENBQTFCO0FBQ0g7QUFDSjs7OzZCQThETztBQUNKLFVBQUlDLEdBQUcsR0FBSSxLQUFLVCxLQUFMLENBQVdVLEtBQVosZ0JBQXVCLHFFQUFDLDREQUFEO0FBQVMsWUFBSSxFQUFFLEtBQUtsQixLQUFMLENBQVdSLE9BQTFCO0FBQW1DLFdBQUcsRUFBQyxpQ0FBdkM7QUFBeUUsZUFBTyxFQUFFLEtBQUsyQjtBQUF2RjtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBQXZCLEdBQStILEVBQXpJO0FBQ0EsVUFBSUMsSUFBSSxHQUFHLEVBQVg7O0FBQ0EsVUFBRyxLQUFLWixLQUFMLENBQVdGLEVBQVgsS0FBa0IsQ0FBckIsRUFBdUI7QUFDbkJjLFlBQUksR0FBRztBQUNIaEMsY0FBSSxFQUFHLEtBQUtvQixLQUFMLENBQVdwQixJQURmO0FBRUhDLGNBQUksRUFBRyxLQUFLbUIsS0FBTCxDQUFXbkIsSUFGZjtBQUdIaUIsWUFBRSxFQUFHLEtBQUtFLEtBQUwsQ0FBV0Y7QUFIYixTQUFQO0FBS0g7O0FBRUQsMEJBQ0kscUVBQUMsNENBQUQsQ0FBTyxRQUFQO0FBQUEsZ0NBRUEscUVBQUMsNkRBQUQ7QUFBVSxjQUFJLEVBQUVjLElBQWhCO0FBQXNCLGtCQUFRLEVBQUUsS0FBS3BCLEtBQUwsQ0FBV04sWUFBM0M7QUFBeUQsc0JBQVksRUFBRSxLQUFLMkIsWUFBNUU7QUFBMEYsc0JBQVksRUFBRSxLQUFLQztBQUE3RztBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUZBLEVBR0NMLEdBSEQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBREo7QUFPSDs7OztFQTFHd0JNLCtDOztBQThHN0IsSUFBTUMsZUFBZSxHQUFHLFNBQWxCQSxlQUFrQixDQUFBeEIsS0FBSyxFQUFJO0FBQzdCSixTQUFPLENBQUNDLEdBQVIsQ0FBWUcsS0FBWjtBQUNBLFNBQU87QUFDSFosUUFBSSxFQUFFWSxLQUFLLENBQUN5QixRQUFOLENBQWVyQyxJQURsQjtBQUVIQyxRQUFJLEVBQUdXLEtBQUssQ0FBQ3lCLFFBQU4sQ0FBZXBDLElBRm5CO0FBR0hpQixNQUFFLEVBQUdOLEtBQUssQ0FBQ3lCLFFBQU4sQ0FBZW5CLEVBSGpCO0FBSUhZLFNBQUssRUFBR2xCLEtBQUssQ0FBQ3lCLFFBQU4sQ0FBZVAsS0FKcEI7QUFLSEosVUFBTSxFQUFHZCxLQUFLLENBQUN5QixRQUFOLENBQWVYLE1BTHJCO0FBTUh2QixTQUFLLEVBQUVTLEtBQUssQ0FBQ3lCLFFBQU4sQ0FBZWxDLEtBTm5CO0FBT0hELFdBQU8sRUFBRVUsS0FBSyxDQUFDeUIsUUFBTixDQUFlbkM7QUFQckIsR0FBUDtBQVNILENBWEQ7O0FBYUEsSUFBTW9DLGtCQUFrQixHQUFHLFNBQXJCQSxrQkFBcUIsQ0FBQUMsUUFBUSxFQUFJO0FBQ25DLFNBQU87QUFDSGxCLGdCQUFZLEVBQUUsc0JBQUNWLElBQUQ7QUFBQSxhQUFVNEIsUUFBUSxDQUFDQyxpRUFBQSxDQUFvQjdCLElBQXBCLENBQUQsQ0FBbEI7QUFBQTtBQURYLEdBQVA7QUFHSCxDQUpEOztBQU9lOEIsMEhBQU8sQ0FBQ0wsZUFBRCxFQUFrQkUsa0JBQWxCLENBQVAsQ0FBNkN2QyxjQUE3QyxDQUFmLEUsQ0FDQSIsImZpbGUiOiJzdGF0aWMvd2VicGFjay9wYWdlcy9pbmRleC45ZDhiNTkzZjhhYzQ4N2RhMDAzMy5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0ICwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCB7IGNvbm5lY3QgfSBmcm9tICdyZWFjdC1yZWR1eCc7XHJcbmltcG9ydCBUb2RvRm9ybSBmcm9tICcuLi9jb21wb25lbnRzL1RvZG9Gb3JtJztcclxuaW1wb3J0IE1lc3NhZ2UgZnJvbSAnLi4vY29tcG9uZW50cy9NZXNzYWdlJztcclxuaW1wb3J0ICogYXMgYWN0aW9ucyBmcm9tICcuLi9zdG9yZS9hY3Rpb25zL2luZGV4JztcclxuXHJcblxyXG5cclxuY2xhc3MgVG9kb0FwcEJ1aWxkZXIgZXh0ZW5kcyBDb21wb25lbnQge1xyXG4gICAgc3RhdGUgPSB7XHJcbiAgICAgICAgbmFtZSA6ICcnLFxyXG4gICAgICAgIGRhdGUgOiAnJyxcclxuICAgICAgICBsb2FkaW5nIDogZmFsc2UsXHJcbiAgICAgICAgZXJyb3IgOiBmYWxzZSxcclxuICAgICAgICBzaG93TXNnIDogdHJ1ZSxcclxuICAgICAgICB2YWxpZGF0ZSA6IGZhbHNlLFxyXG4gICAgICAgIGVycm9yTmFtZU1zZyA6JydcclxuICAgIH1cclxuXHJcbiAgICBjb21wb25lbnREaWRNb3VudCAoKSB7XHJcbiAgICAgICAgaWYodGhpcy5wcm9wcy5lZGl0ZWQpe1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIC4uLnRoaXMuc3RhdGUsXHJcbiAgICAgICAgICAgICAgICBuYW1lIDogdGhpcy5wcm9wcy5uYW1lLFxyXG4gICAgICAgICAgICAgICAgZGF0ZSA6IHRoaXMucHJvcHMuZGF0ZSxcclxuICAgICAgICAgICAgICAgIHNob3dNc2cgOiB0cnVlXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBjb21wb25lbnREaWRVcGRhdGUgKCkge1xyXG4gICAgICAgIGlmKHRoaXMucHJvcHMuZWRpdGVkKXtcclxuICAgICAgICAgICAgZG9jdW1lbnQuYm9keS5zY3JvbGxUb3AgPSAwO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBoYW5kbGVTdWJtaXQgPSAoZXZlbnQpID0+IHtcclxuICAgICAgICBjb25zb2xlLmxvZygnPT0gaGVyZSBpbiBCdWlsZGVyPT09Jyk7XHJcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuXHJcbiAgICAgICAgbGV0IHRhc2sgPSB7XHJcbiAgICAgICAgICAgIG5hbWUgOiB0aGlzLnN0YXRlLm5hbWUsXHJcbiAgICAgICAgICAgIGRhdGUgOiB0aGlzLnN0YXRlLmRhdGUsXHJcbiAgICAgICAgICAgIGNvbXBsZXRlIDogZmFsc2UsXHJcbiAgICAgICAgICAgIGFjdGl2ZTogdHJ1ZSxcclxuICAgICAgICAgICAgdmFsaWRhdGU6dHJ1ZVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgaWYoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJ0YXNrSWRcIikudmFsdWUgIT0gJycpe1xyXG4gICAgICAgICAgICB0YXNrID0gey4uLnRhc2ssaWQgOiBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInRhc2tJZFwiKS52YWx1ZX07XHJcbiAgICAgICAgfVxyXG4gICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIC4uLnRoaXMuc3RhdGUsXHJcbiAgICAgICAgICAgICAgICBzaG93TXNnIDogdHJ1ZVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgaWYodGhpcy5zdGF0ZS52YWxpZGF0ZSl7XHJcbiAgICAgICAgICAgIHRoaXMucHJvcHMub25TdWJtaXRUYXNrKHRhc2spO1xyXG4gICAgICAgICB9XHJcblxyXG4gICAgfVxyXG5cclxuICAgIGhhbmRsZUNoYW5nZSA9IChldmVudCkgPT4ge1xyXG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgY29uc3QgeyB0eXBlLCB2YWx1ZSB9ID0gZXZlbnQudGFyZ2V0O1xyXG4gICAgICAgIGxldCBsZXR0ZXJzID0gL15bQS1aYS16XSskLztcclxuXHJcbiAgICAgICAgLy8gaWYoZXZlbnQudGFyZ2V0LnR5cGUgPT0gJ3RleHQnKSB7XHJcbiAgICAgICAgLy8gICAgIGlmKCF2YWx1ZS5tYXRjaChsZXR0ZXJzKSl7XHJcbiAgICAgICAgLy8gICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAvLyAgICAgICAgICAgICAgLi4udGhpcy5zdGF0ZSxcclxuICAgICAgICAvLyAgICAgICAgICAgICB2YWxpZGF0ZSA6IGZhbHNlLFxyXG4gICAgICAgIC8vICAgICAgICAgICAgIGVycm9yTmFtZU1zZyA6ICdQbGVhc2UgRW50ZXIgTGV0dGVycyBPbmx5LidcclxuICAgICAgICAvLyAgICAgICAgIH0pO1xyXG4gICAgICAgIC8vICAgICB9ZWxzZXtcclxuICAgICAgICAvLyAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgIC8vICAgICAgICAgICAgIC4uLnRoaXMuc3RhdGUsXHJcbiAgICAgICAgLy8gICAgICAgICAgICAgdmFsaWRhdGUgOiB0cnVlLFxyXG4gICAgICAgIC8vICAgICAgICAgICAgIG5hbWUgOiBldmVudC50YXJnZXQudmFsdWVcclxuICAgICAgICAvLyAgICAgICAgIH0pO1xyXG4gICAgICAgIC8vICAgICB9XHJcbiAgICAgICAgLy8gfVxyXG4gICAgICAgIC8vIGlmKGV2ZW50LnRhcmdldC50eXBlID09PSAnZGF0ZScpe1xyXG4gICAgICAgIC8vICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAvLyAgICAgICAgIC4uLnRoaXMuc3RhdGUsXHJcbiAgICAgICAgLy8gICAgICAgICBkYXRlIDogZXZlbnQudGFyZ2V0LnZhbHVlXHJcbiAgICAgICAgLy8gICAgIH0pO1xyXG4gICAgICAgIC8vIH1cclxuICAgIH1cclxuXHJcbiAgICBoYW5kbGVNc2cgPSAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICAuLi50aGlzLnN0YXRlLFxyXG4gICAgICAgICAgICAgICAgc2hvd01zZyA6IGZhbHNlXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJ0b2RvLWZvcm1cIikucmVzZXQoKTtcclxuICAgIH1cclxuXHJcbiAgICByZW5kZXIoKXtcclxuICAgICAgICBsZXQgbXNnID0gKHRoaXMucHJvcHMuYWRkZWQgKSA/ICg8TWVzc2FnZSBzaG93PXt0aGlzLnN0YXRlLnNob3dNc2d9IG1zZz1cIlRhc2sgQWRkZWQgU3VjY2Vzc2Z1bGx5IC4uLiAhISFcIiBzZXRTaG93PXt0aGlzLmhhbmRsZU1zZ30gLz4pIDogJyc7XHJcbiAgICAgICAgbGV0IGRhdGEgPSB7fTtcclxuICAgICAgICBpZih0aGlzLnByb3BzLmlkICE9PSAwKXtcclxuICAgICAgICAgICAgZGF0YSA9IHtcclxuICAgICAgICAgICAgICAgIG5hbWUgOiB0aGlzLnByb3BzLm5hbWUsXHJcbiAgICAgICAgICAgICAgICBkYXRlIDogdGhpcy5wcm9wcy5kYXRlLFxyXG4gICAgICAgICAgICAgICAgaWQgOiB0aGlzLnByb3BzLmlkXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxSZWFjdC5GcmFnbWVudD5cclxuXHJcbiAgICAgICAgICAgIDxUb2RvRm9ybSBkYXRhPXtkYXRhfSB2YWxpZGF0ZT17dGhpcy5zdGF0ZS5lcnJvck5hbWVNc2d9IGhhbmRsZVN1Ym1pdD17dGhpcy5oYW5kbGVTdWJtaXR9IGhhbmRsZUNoYW5nZT17dGhpcy5oYW5kbGVDaGFuZ2V9Lz5cclxuICAgICAgICAgICAge21zZ31cclxuICAgICAgICAgICAgPC9SZWFjdC5GcmFnbWVudD5cclxuICAgICAgICApO1xyXG4gICAgfVxyXG59XHJcblxyXG5cclxuY29uc3QgbWFwU3RhdGVUb1Byb3BzID0gc3RhdGUgPT4ge1xyXG4gICAgY29uc29sZS5sb2coc3RhdGUpO1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgICBuYW1lOiBzdGF0ZS50b2RvVGFzay5uYW1lLFxyXG4gICAgICAgIGRhdGUgOiBzdGF0ZS50b2RvVGFzay5kYXRlLFxyXG4gICAgICAgIGlkIDogc3RhdGUudG9kb1Rhc2suaWQsXHJcbiAgICAgICAgYWRkZWQgOiBzdGF0ZS50b2RvVGFzay5hZGRlZCxcclxuICAgICAgICBlZGl0ZWQgOiBzdGF0ZS50b2RvVGFzay5lZGl0ZWQsXHJcbiAgICAgICAgZXJyb3I6IHN0YXRlLnRvZG9UYXNrLmVycm9yLFxyXG4gICAgICAgIGxvYWRpbmc6IHN0YXRlLnRvZG9UYXNrLmxvYWRpbmdcclxuICAgIH1cclxufTtcclxuXHJcbmNvbnN0IG1hcERpc3BhdGNoVG9Qcm9wcyA9IGRpc3BhdGNoID0+IHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgb25TdWJtaXRUYXNrOiAodGFzaykgPT4gZGlzcGF0Y2goYWN0aW9ucy5pbml0QWRkVGFzayh0YXNrKSlcclxuICAgIH07XHJcbn07XHJcblxyXG5cclxuZXhwb3J0IGRlZmF1bHQgY29ubmVjdChtYXBTdGF0ZVRvUHJvcHMsIG1hcERpc3BhdGNoVG9Qcm9wcykoVG9kb0FwcEJ1aWxkZXIpO1xyXG4vLyBleHBvcnQgZGVmYXVsdCAoVG9kb0FwcEJ1aWxkZXIpO1xyXG4iXSwic291cmNlUm9vdCI6IiJ9