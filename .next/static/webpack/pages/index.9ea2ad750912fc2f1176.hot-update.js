webpackHotUpdate_N_E("pages/index",{

/***/ "./containers/TodoAppBuilder.js":
/*!**************************************!*\
  !*** ./containers/TodoAppBuilder.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var _babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/esm/classCallCheck */ "./node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/esm/createClass */ "./node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/esm/assertThisInitialized */ "./node_modules/@babel/runtime/helpers/esm/assertThisInitialized.js");
/* harmony import */ var _babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/esm/inherits */ "./node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime/helpers/esm/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _components_TodoForm__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../components/TodoForm */ "./components/TodoForm.js");
/* harmony import */ var _components_Message__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../components/Message */ "./components/Message.js");
/* harmony import */ var _store_actions_index__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../store/actions/index */ "./store/actions/index.js");








var _jsxFileName = "D:\\PERSONAL\\Vanita\\React\\test\\todo-next\\containers\\TodoAppBuilder.js";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return Object(_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }







var TodoAppBuilder = /*#__PURE__*/function (_Component) {
  Object(_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_3__["default"])(TodoAppBuilder, _Component);

  var _super = _createSuper(TodoAppBuilder);

  function TodoAppBuilder() {
    var _this;

    Object(_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, TodoAppBuilder);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "state", {
      name: '',
      date: '',
      loading: false,
      error: false,
      showMsg: true,
      validate: false,
      errorNameMsg: ''
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleSubmit", function (event) {
      event.preventDefault();
      var task = {
        name: _this.state.name,
        date: _this.state.date,
        complete: false,
        active: true
      };

      if (document.getElementById("taskId").value != '') {
        task = _objectSpread(_objectSpread({}, task), {}, {
          id: document.getElementById("taskId").value
        });
      }

      _this.setState(_objectSpread(_objectSpread({}, _this.state), {}, {
        showMsg: true
      }));

      if (_this.state.validate) {
        _this.props.onSubmitTask(task);
      }
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleChange", function (event) {
      event.preventDefault();
      var _event$target = event.target,
          type = _event$target.type,
          value = _event$target.value;
      var letters = /^[A-Za-z]+$/;

      if (event.target.type == 'text') {
        if (!value.match(letters)) {
          _this.setState(_objectSpread(_objectSpread({}, _this.state), {}, {
            validate: false,
            errorNameMsg: 'Please Enter Letters Only.'
          }));
        } else {
          _this.setState(_objectSpread(_objectSpread({}, _this.state), {}, {
            validate: true,
            name: event.target.value
          }));
        }
      }

      if (event.target.type === 'date') {
        _this.setState(_objectSpread(_objectSpread({}, _this.state), {}, {
          date: event.target.value
        }));
      }
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleMsg", function () {
      _this.setState(_objectSpread(_objectSpread({}, _this.state), {}, {
        showMsg: false
      }));

      document.getElementById("todo-form").reset();
    });

    return _this;
  }

  Object(_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(TodoAppBuilder, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (this.props.edited) {
        this.setState(_objectSpread(_objectSpread({}, this.state), {}, {
          name: this.props.name,
          date: this.props.date,
          showMsg: true
        }));
      }
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate() {
      if (this.props.edited) {
        document.body.scrollTop = 0;
      }
    }
  }, {
    key: "render",
    value: function render() {
      var msg = this.props.added ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(_components_Message__WEBPACK_IMPORTED_MODULE_11__["default"], {
        show: this.state.showMsg,
        msg: "Task Added Successfully ... !!!",
        setShow: this.handleMsg
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 96,
        columnNumber: 42
      }, this) : '';
      var data = {};

      if (this.props.id !== 0) {
        data = {
          name: this.props.name,
          date: this.props.date,
          id: this.props.id
        };
      }

      return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(react__WEBPACK_IMPORTED_MODULE_8___default.a.Fragment, {
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(_components_TodoForm__WEBPACK_IMPORTED_MODULE_10__["default"], {
          data: data,
          validate: this.state.errorNameMsg,
          handleSubmit: this.handleSubmit,
          handleChange: this.handleChange
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 109,
          columnNumber: 13
        }, this), msg]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 107,
        columnNumber: 13
      }, this);
    }
  }]);

  return TodoAppBuilder;
}(react__WEBPACK_IMPORTED_MODULE_8__["Component"]);

var mapStateToProps = function mapStateToProps(state) {
  console.log(state);
  return {
    name: state.todoTask.name,
    date: state.todoTask.date,
    id: state.todoTask.id,
    added: state.todoTask.added,
    edited: state.todoTask.edited,
    error: state.todoTask.error,
    loading: state.todoTask.loading
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    onSubmitTask: function onSubmitTask(task) {
      return dispatch(_store_actions_index__WEBPACK_IMPORTED_MODULE_12__["initAddTask"](task));
    }
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_9__["connect"])(mapStateToProps, mapDispatchToProps)(TodoAppBuilder)); // export default (TodoAppBuilder);

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vY29udGFpbmVycy9Ub2RvQXBwQnVpbGRlci5qcyJdLCJuYW1lcyI6WyJUb2RvQXBwQnVpbGRlciIsIm5hbWUiLCJkYXRlIiwibG9hZGluZyIsImVycm9yIiwic2hvd01zZyIsInZhbGlkYXRlIiwiZXJyb3JOYW1lTXNnIiwiZXZlbnQiLCJwcmV2ZW50RGVmYXVsdCIsInRhc2siLCJzdGF0ZSIsImNvbXBsZXRlIiwiYWN0aXZlIiwiZG9jdW1lbnQiLCJnZXRFbGVtZW50QnlJZCIsInZhbHVlIiwiaWQiLCJzZXRTdGF0ZSIsInByb3BzIiwib25TdWJtaXRUYXNrIiwidGFyZ2V0IiwidHlwZSIsImxldHRlcnMiLCJtYXRjaCIsInJlc2V0IiwiZWRpdGVkIiwiYm9keSIsInNjcm9sbFRvcCIsIm1zZyIsImFkZGVkIiwiaGFuZGxlTXNnIiwiZGF0YSIsImhhbmRsZVN1Ym1pdCIsImhhbmRsZUNoYW5nZSIsIkNvbXBvbmVudCIsIm1hcFN0YXRlVG9Qcm9wcyIsImNvbnNvbGUiLCJsb2ciLCJ0b2RvVGFzayIsIm1hcERpc3BhdGNoVG9Qcm9wcyIsImRpc3BhdGNoIiwiYWN0aW9ucyIsImNvbm5lY3QiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7SUFJTUEsYzs7Ozs7Ozs7Ozs7Ozs7OztnTkFDTTtBQUNKQyxVQUFJLEVBQUcsRUFESDtBQUVKQyxVQUFJLEVBQUcsRUFGSDtBQUdKQyxhQUFPLEVBQUcsS0FITjtBQUlKQyxXQUFLLEVBQUcsS0FKSjtBQUtKQyxhQUFPLEVBQUcsSUFMTjtBQU1KQyxjQUFRLEVBQUcsS0FOUDtBQU9KQyxrQkFBWSxFQUFFO0FBUFYsSzs7dU5BMkJPLFVBQUNDLEtBQUQsRUFBVztBQUN0QkEsV0FBSyxDQUFDQyxjQUFOO0FBRUEsVUFBSUMsSUFBSSxHQUFHO0FBQ1BULFlBQUksRUFBRyxNQUFLVSxLQUFMLENBQVdWLElBRFg7QUFFUEMsWUFBSSxFQUFHLE1BQUtTLEtBQUwsQ0FBV1QsSUFGWDtBQUdQVSxnQkFBUSxFQUFHLEtBSEo7QUFJUEMsY0FBTSxFQUFFO0FBSkQsT0FBWDs7QUFNQSxVQUFHQyxRQUFRLENBQUNDLGNBQVQsQ0FBd0IsUUFBeEIsRUFBa0NDLEtBQWxDLElBQTJDLEVBQTlDLEVBQWlEO0FBQzdDTixZQUFJLG1DQUFPQSxJQUFQO0FBQVlPLFlBQUUsRUFBR0gsUUFBUSxDQUFDQyxjQUFULENBQXdCLFFBQXhCLEVBQWtDQztBQUFuRCxVQUFKO0FBQ0g7O0FBQ0EsWUFBS0UsUUFBTCxpQ0FDVSxNQUFLUCxLQURmO0FBRU9OLGVBQU8sRUFBRztBQUZqQjs7QUFJQSxVQUFHLE1BQUtNLEtBQUwsQ0FBV0wsUUFBZCxFQUF1QjtBQUNwQixjQUFLYSxLQUFMLENBQVdDLFlBQVgsQ0FBd0JWLElBQXhCO0FBQ0Y7QUFFTCxLOzt1TkFFYyxVQUFDRixLQUFELEVBQVc7QUFDdEJBLFdBQUssQ0FBQ0MsY0FBTjtBQURzQiwwQkFFRUQsS0FBSyxDQUFDYSxNQUZSO0FBQUEsVUFFZEMsSUFGYyxpQkFFZEEsSUFGYztBQUFBLFVBRVJOLEtBRlEsaUJBRVJBLEtBRlE7QUFHdEIsVUFBSU8sT0FBTyxHQUFHLGFBQWQ7O0FBRUEsVUFBR2YsS0FBSyxDQUFDYSxNQUFOLENBQWFDLElBQWIsSUFBcUIsTUFBeEIsRUFBZ0M7QUFDNUIsWUFBRyxDQUFDTixLQUFLLENBQUNRLEtBQU4sQ0FBWUQsT0FBWixDQUFKLEVBQXlCO0FBQ3JCLGdCQUFLTCxRQUFMLGlDQUNRLE1BQUtQLEtBRGI7QUFFSUwsb0JBQVEsRUFBRyxLQUZmO0FBR0lDLHdCQUFZLEVBQUc7QUFIbkI7QUFLSCxTQU5ELE1BTUs7QUFDRCxnQkFBS1csUUFBTCxpQ0FDTyxNQUFLUCxLQURaO0FBRUlMLG9CQUFRLEVBQUcsSUFGZjtBQUdJTCxnQkFBSSxFQUFHTyxLQUFLLENBQUNhLE1BQU4sQ0FBYUw7QUFIeEI7QUFLSDtBQUNKOztBQUNELFVBQUdSLEtBQUssQ0FBQ2EsTUFBTixDQUFhQyxJQUFiLEtBQXNCLE1BQXpCLEVBQWdDO0FBQzVCLGNBQUtKLFFBQUwsaUNBQ08sTUFBS1AsS0FEWjtBQUVJVCxjQUFJLEVBQUdNLEtBQUssQ0FBQ2EsTUFBTixDQUFhTDtBQUZ4QjtBQUlIO0FBQ0osSzs7b05BRVcsWUFBTTtBQUNkLFlBQUtFLFFBQUwsaUNBQ1csTUFBS1AsS0FEaEI7QUFFUU4sZUFBTyxFQUFHO0FBRmxCOztBQUlBUyxjQUFRLENBQUNDLGNBQVQsQ0FBd0IsV0FBeEIsRUFBcUNVLEtBQXJDO0FBQ0gsSzs7Ozs7Ozt3Q0F6RW9CO0FBQ2pCLFVBQUcsS0FBS04sS0FBTCxDQUFXTyxNQUFkLEVBQXFCO0FBQ2pCLGFBQUtSLFFBQUwsaUNBQ08sS0FBS1AsS0FEWjtBQUVJVixjQUFJLEVBQUcsS0FBS2tCLEtBQUwsQ0FBV2xCLElBRnRCO0FBR0lDLGNBQUksRUFBRyxLQUFLaUIsS0FBTCxDQUFXakIsSUFIdEI7QUFJSUcsaUJBQU8sRUFBRztBQUpkO0FBTUg7QUFDSjs7O3lDQUVxQjtBQUNsQixVQUFHLEtBQUtjLEtBQUwsQ0FBV08sTUFBZCxFQUFxQjtBQUNqQlosZ0JBQVEsQ0FBQ2EsSUFBVCxDQUFjQyxTQUFkLEdBQTBCLENBQTFCO0FBQ0g7QUFDSjs7OzZCQTRETztBQUNKLFVBQUlDLEdBQUcsR0FBSSxLQUFLVixLQUFMLENBQVdXLEtBQVosZ0JBQXVCLHFFQUFDLDREQUFEO0FBQVMsWUFBSSxFQUFFLEtBQUtuQixLQUFMLENBQVdOLE9BQTFCO0FBQW1DLFdBQUcsRUFBQyxpQ0FBdkM7QUFBeUUsZUFBTyxFQUFFLEtBQUswQjtBQUF2RjtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBQXZCLEdBQStILEVBQXpJO0FBQ0EsVUFBSUMsSUFBSSxHQUFHLEVBQVg7O0FBQ0EsVUFBRyxLQUFLYixLQUFMLENBQVdGLEVBQVgsS0FBa0IsQ0FBckIsRUFBdUI7QUFDbkJlLFlBQUksR0FBRztBQUNIL0IsY0FBSSxFQUFHLEtBQUtrQixLQUFMLENBQVdsQixJQURmO0FBRUhDLGNBQUksRUFBRyxLQUFLaUIsS0FBTCxDQUFXakIsSUFGZjtBQUdIZSxZQUFFLEVBQUcsS0FBS0UsS0FBTCxDQUFXRjtBQUhiLFNBQVA7QUFLSDs7QUFFRCwwQkFDSSxxRUFBQyw0Q0FBRCxDQUFPLFFBQVA7QUFBQSxnQ0FFQSxxRUFBQyw2REFBRDtBQUFVLGNBQUksRUFBRWUsSUFBaEI7QUFBc0Isa0JBQVEsRUFBRSxLQUFLckIsS0FBTCxDQUFXSixZQUEzQztBQUF5RCxzQkFBWSxFQUFFLEtBQUswQixZQUE1RTtBQUEwRixzQkFBWSxFQUFFLEtBQUtDO0FBQTdHO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBRkEsRUFHQ0wsR0FIRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FESjtBQU9IOzs7O0VBeEd3Qk0sK0M7O0FBNEc3QixJQUFNQyxlQUFlLEdBQUcsU0FBbEJBLGVBQWtCLENBQUF6QixLQUFLLEVBQUk7QUFDN0IwQixTQUFPLENBQUNDLEdBQVIsQ0FBWTNCLEtBQVo7QUFDQSxTQUFPO0FBQ0hWLFFBQUksRUFBRVUsS0FBSyxDQUFDNEIsUUFBTixDQUFldEMsSUFEbEI7QUFFSEMsUUFBSSxFQUFHUyxLQUFLLENBQUM0QixRQUFOLENBQWVyQyxJQUZuQjtBQUdIZSxNQUFFLEVBQUdOLEtBQUssQ0FBQzRCLFFBQU4sQ0FBZXRCLEVBSGpCO0FBSUhhLFNBQUssRUFBR25CLEtBQUssQ0FBQzRCLFFBQU4sQ0FBZVQsS0FKcEI7QUFLSEosVUFBTSxFQUFHZixLQUFLLENBQUM0QixRQUFOLENBQWViLE1BTHJCO0FBTUh0QixTQUFLLEVBQUVPLEtBQUssQ0FBQzRCLFFBQU4sQ0FBZW5DLEtBTm5CO0FBT0hELFdBQU8sRUFBRVEsS0FBSyxDQUFDNEIsUUFBTixDQUFlcEM7QUFQckIsR0FBUDtBQVNILENBWEQ7O0FBYUEsSUFBTXFDLGtCQUFrQixHQUFHLFNBQXJCQSxrQkFBcUIsQ0FBQUMsUUFBUSxFQUFJO0FBQ25DLFNBQU87QUFDSHJCLGdCQUFZLEVBQUUsc0JBQUNWLElBQUQ7QUFBQSxhQUFVK0IsUUFBUSxDQUFDQyxpRUFBQSxDQUFvQmhDLElBQXBCLENBQUQsQ0FBbEI7QUFBQTtBQURYLEdBQVA7QUFHSCxDQUpEOztBQU9laUMsMEhBQU8sQ0FBQ1AsZUFBRCxFQUFrQkksa0JBQWxCLENBQVAsQ0FBNkN4QyxjQUE3QyxDQUFmLEUsQ0FDQSIsImZpbGUiOiJzdGF0aWMvd2VicGFjay9wYWdlcy9pbmRleC45ZWEyYWQ3NTA5MTJmYzJmMTE3Ni5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0ICwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCB7IGNvbm5lY3QgfSBmcm9tICdyZWFjdC1yZWR1eCc7XHJcbmltcG9ydCBUb2RvRm9ybSBmcm9tICcuLi9jb21wb25lbnRzL1RvZG9Gb3JtJztcclxuaW1wb3J0IE1lc3NhZ2UgZnJvbSAnLi4vY29tcG9uZW50cy9NZXNzYWdlJztcclxuaW1wb3J0ICogYXMgYWN0aW9ucyBmcm9tICcuLi9zdG9yZS9hY3Rpb25zL2luZGV4JztcclxuXHJcblxyXG5cclxuY2xhc3MgVG9kb0FwcEJ1aWxkZXIgZXh0ZW5kcyBDb21wb25lbnQge1xyXG4gICAgc3RhdGUgPSB7XHJcbiAgICAgICAgbmFtZSA6ICcnLFxyXG4gICAgICAgIGRhdGUgOiAnJyxcclxuICAgICAgICBsb2FkaW5nIDogZmFsc2UsXHJcbiAgICAgICAgZXJyb3IgOiBmYWxzZSxcclxuICAgICAgICBzaG93TXNnIDogdHJ1ZSxcclxuICAgICAgICB2YWxpZGF0ZSA6IGZhbHNlLFxyXG4gICAgICAgIGVycm9yTmFtZU1zZyA6JydcclxuICAgIH1cclxuXHJcbiAgICBjb21wb25lbnREaWRNb3VudCAoKSB7XHJcbiAgICAgICAgaWYodGhpcy5wcm9wcy5lZGl0ZWQpe1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIC4uLnRoaXMuc3RhdGUsXHJcbiAgICAgICAgICAgICAgICBuYW1lIDogdGhpcy5wcm9wcy5uYW1lLFxyXG4gICAgICAgICAgICAgICAgZGF0ZSA6IHRoaXMucHJvcHMuZGF0ZSxcclxuICAgICAgICAgICAgICAgIHNob3dNc2cgOiB0cnVlXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBjb21wb25lbnREaWRVcGRhdGUgKCkge1xyXG4gICAgICAgIGlmKHRoaXMucHJvcHMuZWRpdGVkKXtcclxuICAgICAgICAgICAgZG9jdW1lbnQuYm9keS5zY3JvbGxUb3AgPSAwO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBoYW5kbGVTdWJtaXQgPSAoZXZlbnQpID0+IHtcclxuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuICAgICAgICBsZXQgdGFzayA9IHtcclxuICAgICAgICAgICAgbmFtZSA6IHRoaXMuc3RhdGUubmFtZSxcclxuICAgICAgICAgICAgZGF0ZSA6IHRoaXMuc3RhdGUuZGF0ZSxcclxuICAgICAgICAgICAgY29tcGxldGUgOiBmYWxzZSxcclxuICAgICAgICAgICAgYWN0aXZlOiB0cnVlXHJcbiAgICAgICAgfTtcclxuICAgICAgICBpZihkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInRhc2tJZFwiKS52YWx1ZSAhPSAnJyl7XHJcbiAgICAgICAgICAgIHRhc2sgPSB7Li4udGFzayxpZCA6IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwidGFza0lkXCIpLnZhbHVlfTtcclxuICAgICAgICB9XHJcbiAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgLi4udGhpcy5zdGF0ZSxcclxuICAgICAgICAgICAgICAgIHNob3dNc2cgOiB0cnVlXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICBpZih0aGlzLnN0YXRlLnZhbGlkYXRlKXtcclxuICAgICAgICAgICAgdGhpcy5wcm9wcy5vblN1Ym1pdFRhc2sodGFzayk7XHJcbiAgICAgICAgIH1cclxuXHJcbiAgICB9XHJcblxyXG4gICAgaGFuZGxlQ2hhbmdlID0gKGV2ZW50KSA9PiB7XHJcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICBjb25zdCB7IHR5cGUsIHZhbHVlIH0gPSBldmVudC50YXJnZXQ7XHJcbiAgICAgICAgbGV0IGxldHRlcnMgPSAvXltBLVphLXpdKyQvO1xyXG5cclxuICAgICAgICBpZihldmVudC50YXJnZXQudHlwZSA9PSAndGV4dCcpIHtcclxuICAgICAgICAgICAgaWYoIXZhbHVlLm1hdGNoKGxldHRlcnMpKXtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgICAgICAuLi50aGlzLnN0YXRlLFxyXG4gICAgICAgICAgICAgICAgICAgIHZhbGlkYXRlIDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgZXJyb3JOYW1lTXNnIDogJ1BsZWFzZSBFbnRlciBMZXR0ZXJzIE9ubHkuJ1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgLi4udGhpcy5zdGF0ZSxcclxuICAgICAgICAgICAgICAgICAgICB2YWxpZGF0ZSA6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgbmFtZSA6IGV2ZW50LnRhcmdldC52YWx1ZVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgaWYoZXZlbnQudGFyZ2V0LnR5cGUgPT09ICdkYXRlJyl7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgLi4udGhpcy5zdGF0ZSxcclxuICAgICAgICAgICAgICAgIGRhdGUgOiBldmVudC50YXJnZXQudmFsdWVcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGhhbmRsZU1zZyA9ICgpID0+IHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIC4uLnRoaXMuc3RhdGUsXHJcbiAgICAgICAgICAgICAgICBzaG93TXNnIDogZmFsc2VcclxuICAgICAgICB9KTtcclxuICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInRvZG8tZm9ybVwiKS5yZXNldCgpO1xyXG4gICAgfVxyXG5cclxuICAgIHJlbmRlcigpe1xyXG4gICAgICAgIGxldCBtc2cgPSAodGhpcy5wcm9wcy5hZGRlZCApID8gKDxNZXNzYWdlIHNob3c9e3RoaXMuc3RhdGUuc2hvd01zZ30gbXNnPVwiVGFzayBBZGRlZCBTdWNjZXNzZnVsbHkgLi4uICEhIVwiIHNldFNob3c9e3RoaXMuaGFuZGxlTXNnfSAvPikgOiAnJztcclxuICAgICAgICBsZXQgZGF0YSA9IHt9O1xyXG4gICAgICAgIGlmKHRoaXMucHJvcHMuaWQgIT09IDApe1xyXG4gICAgICAgICAgICBkYXRhID0ge1xyXG4gICAgICAgICAgICAgICAgbmFtZSA6IHRoaXMucHJvcHMubmFtZSxcclxuICAgICAgICAgICAgICAgIGRhdGUgOiB0aGlzLnByb3BzLmRhdGUsXHJcbiAgICAgICAgICAgICAgICBpZCA6IHRoaXMucHJvcHMuaWRcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPFJlYWN0LkZyYWdtZW50PlxyXG5cclxuICAgICAgICAgICAgPFRvZG9Gb3JtIGRhdGE9e2RhdGF9IHZhbGlkYXRlPXt0aGlzLnN0YXRlLmVycm9yTmFtZU1zZ30gaGFuZGxlU3VibWl0PXt0aGlzLmhhbmRsZVN1Ym1pdH0gaGFuZGxlQ2hhbmdlPXt0aGlzLmhhbmRsZUNoYW5nZX0vPlxyXG4gICAgICAgICAgICB7bXNnfVxyXG4gICAgICAgICAgICA8L1JlYWN0LkZyYWdtZW50PlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbn1cclxuXHJcblxyXG5jb25zdCBtYXBTdGF0ZVRvUHJvcHMgPSBzdGF0ZSA9PiB7XHJcbiAgICBjb25zb2xlLmxvZyhzdGF0ZSk7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICAgIG5hbWU6IHN0YXRlLnRvZG9UYXNrLm5hbWUsXHJcbiAgICAgICAgZGF0ZSA6IHN0YXRlLnRvZG9UYXNrLmRhdGUsXHJcbiAgICAgICAgaWQgOiBzdGF0ZS50b2RvVGFzay5pZCxcclxuICAgICAgICBhZGRlZCA6IHN0YXRlLnRvZG9UYXNrLmFkZGVkLFxyXG4gICAgICAgIGVkaXRlZCA6IHN0YXRlLnRvZG9UYXNrLmVkaXRlZCxcclxuICAgICAgICBlcnJvcjogc3RhdGUudG9kb1Rhc2suZXJyb3IsXHJcbiAgICAgICAgbG9hZGluZzogc3RhdGUudG9kb1Rhc2subG9hZGluZ1xyXG4gICAgfVxyXG59O1xyXG5cclxuY29uc3QgbWFwRGlzcGF0Y2hUb1Byb3BzID0gZGlzcGF0Y2ggPT4ge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgICBvblN1Ym1pdFRhc2s6ICh0YXNrKSA9PiBkaXNwYXRjaChhY3Rpb25zLmluaXRBZGRUYXNrKHRhc2spKVxyXG4gICAgfTtcclxufTtcclxuXHJcblxyXG5leHBvcnQgZGVmYXVsdCBjb25uZWN0KG1hcFN0YXRlVG9Qcm9wcywgbWFwRGlzcGF0Y2hUb1Byb3BzKShUb2RvQXBwQnVpbGRlcik7XHJcbi8vIGV4cG9ydCBkZWZhdWx0IChUb2RvQXBwQnVpbGRlcik7XHJcbiJdLCJzb3VyY2VSb290IjoiIn0=