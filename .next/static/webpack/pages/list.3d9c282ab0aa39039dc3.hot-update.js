webpackHotUpdate_N_E("pages/list",{

/***/ "./store/actions/todoAction.js":
/*!*************************************!*\
  !*** ./store/actions/todoAction.js ***!
  \*************************************/
/*! exports provided: addTask, addingTaskStart, addingTaskSucess, addingTaskFailed, initAddTask, fetchTaskSuccess, fetchTaskFail, fetchTaskStart, fetchTask, deleteTask, deleteTaskSuccess, deleteTaskFail, initDeleteTask, editTask, editTaskSuccess, editTaskFail, initEditTask, changeStatusTask, changeStatusTaskSuccess, changeStatusTaskFail, initChangeStatusTask, filterTask, filterTaskSuccess, filterTaskFail, initFilterTask */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addTask", function() { return addTask; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addingTaskStart", function() { return addingTaskStart; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addingTaskSucess", function() { return addingTaskSucess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addingTaskFailed", function() { return addingTaskFailed; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initAddTask", function() { return initAddTask; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchTaskSuccess", function() { return fetchTaskSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchTaskFail", function() { return fetchTaskFail; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchTaskStart", function() { return fetchTaskStart; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchTask", function() { return fetchTask; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteTask", function() { return deleteTask; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteTaskSuccess", function() { return deleteTaskSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteTaskFail", function() { return deleteTaskFail; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initDeleteTask", function() { return initDeleteTask; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "editTask", function() { return editTask; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "editTaskSuccess", function() { return editTaskSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "editTaskFail", function() { return editTaskFail; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initEditTask", function() { return initEditTask; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "changeStatusTask", function() { return changeStatusTask; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "changeStatusTaskSuccess", function() { return changeStatusTaskSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "changeStatusTaskFail", function() { return changeStatusTaskFail; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initChangeStatusTask", function() { return initChangeStatusTask; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "filterTask", function() { return filterTask; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "filterTaskSuccess", function() { return filterTaskSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "filterTaskFail", function() { return filterTaskFail; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initFilterTask", function() { return initFilterTask; });
/* harmony import */ var _babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/esm/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var _actionTypes__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./actionTypes */ "./store/actions/actionTypes.js");
/* harmony import */ var _firebase__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../firebase */ "./firebase.js");


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }


 // add task

var addTask = function addTask(name, date) {
  return {
    type: _actionTypes__WEBPACK_IMPORTED_MODULE_1__["ADD_TASK"],
    name: name,
    date: date
  };
};
var addingTaskStart = function addingTaskStart() {
  return {
    type: _actionTypes__WEBPACK_IMPORTED_MODULE_1__["ADD_TASK_START"]
  };
};
var addingTaskSucess = function addingTaskSucess(task) {
  return {
    type: _actionTypes__WEBPACK_IMPORTED_MODULE_1__["ADD_TASK_SUCESS"]
  };
};
var addingTaskFailed = function addingTaskFailed() {
  return {
    type: _actionTypes__WEBPACK_IMPORTED_MODULE_1__["ADD_TASK_FAILED"]
  };
};
var initAddTask = function initAddTask(task) {
  return function (dispatch) {
    dispatch(addingTaskStart());

    try {
      if (task.id !== undefined && task.id !== 0 && task.id != '') {
        var todoRef = _firebase__WEBPACK_IMPORTED_MODULE_2__["default"].database().ref('todo').child(task.id);
        todoRef.update({
          name: task.name,
          date: task.date
        });
        var data = {
          id: '',
          name: '',
          date: ''
        };
        dispatch(editTaskSuccess(data));
        dispatch(addingTaskSucess(data));
      } else {
        dispatch(addTask(task.name, task.date));

        var _todoRef = _firebase__WEBPACK_IMPORTED_MODULE_2__["default"].database().ref('todo');

        var addData = _todoRef.push(task);

        var _data = {
          id: addData.key,
          name: task.name,
          date: task.date
        };
        dispatch(addingTaskSucess(_data));
      }
    } catch (err) {
      dispatch(addingTaskFailed());
    }
  };
}; // get task list

var fetchTaskSuccess = function fetchTaskSuccess(tasks) {
  return {
    type: _actionTypes__WEBPACK_IMPORTED_MODULE_1__["FETCH_TASK_SUCESS"],
    data: tasks
  };
};
var fetchTaskFail = function fetchTaskFail() {
  return {
    type: _actionTypes__WEBPACK_IMPORTED_MODULE_1__["FETCH_TASK_FAILED"]
  };
};
var fetchTaskStart = function fetchTaskStart() {
  return {
    type: _actionTypes__WEBPACK_IMPORTED_MODULE_1__["FETCH_TASK"]
  };
};
var fetchTask = function fetchTask() {
  return function (dispatch) {
    dispatch(fetchTaskStart());

    try {
      var todoRef = _firebase__WEBPACK_IMPORTED_MODULE_2__["default"].database().ref('todo');
      todoRef.on('value', function (snapshot) {
        var todos = snapshot.val();
        var todoList = [];

        for (var id in todos) {
          todoList.push(_objectSpread({
            id: id
          }, todos[id]));
        }

        dispatch(fetchTaskSuccess(todoList));
      });
    } catch (err) {
      alert();
      dispatch(fetchTaskFail(err));
    }
  };
}; // delete task

var deleteTask = function deleteTask() {
  return {
    type: _actionTypes__WEBPACK_IMPORTED_MODULE_1__["DELETE_TASK"]
  };
};
var deleteTaskSuccess = function deleteTaskSuccess(tasks) {
  return {
    type: _actionTypes__WEBPACK_IMPORTED_MODULE_1__["DELETE_TASK_SUCESS"]
  };
};
var deleteTaskFail = function deleteTaskFail(err) {
  return {
    type: _actionTypes__WEBPACK_IMPORTED_MODULE_1__["DELETE_TASK_FAILED"]
  };
};
var initDeleteTask = function initDeleteTask(id) {
  return function (dispatch) {
    dispatch(deleteTask());

    try {
      var todoRef = _firebase__WEBPACK_IMPORTED_MODULE_2__["default"].database().ref('todo').child(id);
      todoRef.remove();
      dispatch(deleteTaskSuccess());
    } catch (err) {
      dispatch(deleteTaskFail(err));
    }
  };
}; // edit task start

var editTask = function editTask() {
  return {
    type: _actionTypes__WEBPACK_IMPORTED_MODULE_1__["EDIT_TASK"]
  };
};
var editTaskSuccess = function editTaskSuccess(tasks) {
  return {
    type: _actionTypes__WEBPACK_IMPORTED_MODULE_1__["EDIT_TASK_SUCESS"],
    name: tasks.name,
    date: tasks.date,
    id: tasks.id
  };
};
var editTaskFail = function editTaskFail(err) {
  return {
    type: _actionTypes__WEBPACK_IMPORTED_MODULE_1__["EDIT_TASK_FAILED"]
  };
};
var initEditTask = function initEditTask(id) {
  return function (dispatch) {
    try {
      dispatch(editTask());
      var todoRef = _firebase__WEBPACK_IMPORTED_MODULE_2__["default"].database().ref('todo').child(id);
      var task = {};
      todoRef.on('value', function (snapshot) {
        var todo = snapshot.val();
        task = {
          name: todo.name,
          date: todo.date,
          id: id
        };
      });
      dispatch(editTaskSuccess(task));
    } catch (err) {
      dispatch(editTaskFail(err));
    }
  };
}; // change status of active or complete

var changeStatusTask = function changeStatusTask() {
  return {
    type: _actionTypes__WEBPACK_IMPORTED_MODULE_1__["CHANGE_STATUS_TASK"]
  };
};
var changeStatusTaskSuccess = function changeStatusTaskSuccess(tasks) {
  return {
    type: _actionTypes__WEBPACK_IMPORTED_MODULE_1__["CHANGE_STATUS_TASK_SUCESS"]
  };
};
var changeStatusTaskFail = function changeStatusTaskFail(err) {
  return {
    type: _actionTypes__WEBPACK_IMPORTED_MODULE_1__["CHANGE_STATUS_TASK_FAILED"]
  };
};
var initChangeStatusTask = function initChangeStatusTask(id, status, val) {
  return function (dispatch) {
    try {
      dispatch(changeStatusTask());

      if (id !== undefined && id !== 0 && id != '') {
        var activeflag = val === 1 ? true : false;
        var completeflag = val === 1 ? true : false;
        var todoRef = _firebase__WEBPACK_IMPORTED_MODULE_2__["default"].database().ref('todo').child(id);

        if (status === 'complete') {
          todoRef.update({
            complete: completeflag
          });
        } else {
          todoRef.update({
            active: activeflag
          });
        }

        dispatch(changeStatusTaskSuccess());
      }
    } catch (err) {
      dispatch(changeStatusTaskFail());
    }
  };
}; // filter task by active/complete/all

var filterTask = function filterTask() {
  return {
    type: _actionTypes__WEBPACK_IMPORTED_MODULE_1__["FILTER_TASK"]
  };
};
var filterTaskSuccess = function filterTaskSuccess(tasks) {
  return {
    type: _actionTypes__WEBPACK_IMPORTED_MODULE_1__["FILTER_TASK_SUCESS"],
    data: tasks
  };
};
var filterTaskFail = function filterTaskFail(err) {
  return {
    type: _actionTypes__WEBPACK_IMPORTED_MODULE_1__["FILTER_TASK_FAILED"]
  };
};
var initFilterTask = function initFilterTask(filter) {
  return function (dispatch) {
    try {
      dispatch(filterTask());
      var ref = _firebase__WEBPACK_IMPORTED_MODULE_2__["default"].database().ref("todo");
      var todoList = [];

      if (filter === "1") {
        ref.orderByChild("active").equalTo(true).on("child_added", function (snapshot) {
          var todos = snapshot.val();
          var data = {};
          data = {
            name: todos.name,
            date: todos.date,
            active: todos.active,
            complete: todos.complete,
            id: snapshot.key
          };
          todoList.push(data);
        });
      } else if (filter === "2") {
        ref.orderByChild("complete").equalTo(true).on("child_added", function (snapshot) {
          var todos = snapshot.val();
          var data = {};
          data = {
            name: todos.name,
            date: todos.date,
            active: todos.active,
            complete: todos.complete,
            id: snapshot.key
          };
          todoList.push(data);
        });
      } else {
        ref.once('value', function (snapshot) {
          var todos = snapshot.val();

          for (var id in todos) {
            todoList.push(_objectSpread({
              id: id
            }, todos[id]));
          }
        });
      }

      dispatch(filterTaskSuccess(todoList));
    } catch (err) {
      dispatch(filterTaskFail());
    }
  };
};

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vc3RvcmUvYWN0aW9ucy90b2RvQWN0aW9uLmpzIl0sIm5hbWVzIjpbImFkZFRhc2siLCJuYW1lIiwiZGF0ZSIsInR5cGUiLCJhY3Rpb25UeXBlcyIsImFkZGluZ1Rhc2tTdGFydCIsIkFERF9UQVNLX1NUQVJUIiwiYWRkaW5nVGFza1N1Y2VzcyIsInRhc2siLCJBRERfVEFTS19TVUNFU1MiLCJhZGRpbmdUYXNrRmFpbGVkIiwiQUREX1RBU0tfRkFJTEVEIiwiaW5pdEFkZFRhc2siLCJkaXNwYXRjaCIsImlkIiwidW5kZWZpbmVkIiwidG9kb1JlZiIsImZpcmViYXNlIiwiZGF0YWJhc2UiLCJyZWYiLCJjaGlsZCIsInVwZGF0ZSIsImRhdGEiLCJlZGl0VGFza1N1Y2Nlc3MiLCJhZGREYXRhIiwicHVzaCIsImtleSIsImVyciIsImZldGNoVGFza1N1Y2Nlc3MiLCJ0YXNrcyIsImZldGNoVGFza0ZhaWwiLCJGRVRDSF9UQVNLX0ZBSUxFRCIsImZldGNoVGFza1N0YXJ0IiwiRkVUQ0hfVEFTSyIsImZldGNoVGFzayIsIm9uIiwic25hcHNob3QiLCJ0b2RvcyIsInZhbCIsInRvZG9MaXN0IiwiYWxlcnQiLCJkZWxldGVUYXNrIiwiREVMRVRFX1RBU0siLCJkZWxldGVUYXNrU3VjY2VzcyIsIkRFTEVURV9UQVNLX1NVQ0VTUyIsImRlbGV0ZVRhc2tGYWlsIiwiREVMRVRFX1RBU0tfRkFJTEVEIiwiaW5pdERlbGV0ZVRhc2siLCJyZW1vdmUiLCJlZGl0VGFzayIsIkVESVRfVEFTSyIsImVkaXRUYXNrRmFpbCIsIkVESVRfVEFTS19GQUlMRUQiLCJpbml0RWRpdFRhc2siLCJ0b2RvIiwiY2hhbmdlU3RhdHVzVGFzayIsIkNIQU5HRV9TVEFUVVNfVEFTSyIsImNoYW5nZVN0YXR1c1Rhc2tTdWNjZXNzIiwiQ0hBTkdFX1NUQVRVU19UQVNLX1NVQ0VTUyIsImNoYW5nZVN0YXR1c1Rhc2tGYWlsIiwiQ0hBTkdFX1NUQVRVU19UQVNLX0ZBSUxFRCIsImluaXRDaGFuZ2VTdGF0dXNUYXNrIiwic3RhdHVzIiwiYWN0aXZlZmxhZyIsImNvbXBsZXRlZmxhZyIsImNvbXBsZXRlIiwiYWN0aXZlIiwiZmlsdGVyVGFzayIsIkZJTFRFUl9UQVNLIiwiZmlsdGVyVGFza1N1Y2Nlc3MiLCJmaWx0ZXJUYXNrRmFpbCIsIkZJTFRFUl9UQVNLX0ZBSUxFRCIsImluaXRGaWx0ZXJUYXNrIiwiZmlsdGVyIiwib3JkZXJCeUNoaWxkIiwiZXF1YWxUbyIsIm9uY2UiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0NBR0E7O0FBRU8sSUFBTUEsT0FBTyxHQUFHLFNBQVZBLE9BQVUsQ0FBQ0MsSUFBRCxFQUFNQyxJQUFOLEVBQWU7QUFDbEMsU0FBTztBQUNIQyxRQUFJLEVBQUVDLHFEQURIO0FBRUhILFFBQUksRUFBRUEsSUFGSDtBQUdIQyxRQUFJLEVBQUdBO0FBSEosR0FBUDtBQUtILENBTk07QUFTQSxJQUFNRyxlQUFlLEdBQUcsU0FBbEJBLGVBQWtCLEdBQU07QUFDakMsU0FBTztBQUNIRixRQUFJLEVBQUVDLDJEQUEwQkU7QUFEN0IsR0FBUDtBQUdILENBSk07QUFNQSxJQUFNQyxnQkFBZ0IsR0FBRyxTQUFuQkEsZ0JBQW1CLENBQUNDLElBQUQsRUFBVTtBQUN0QyxTQUFPO0FBQ0hMLFFBQUksRUFBRUMsNERBQTJCSztBQUQ5QixHQUFQO0FBSUgsQ0FMTTtBQU9BLElBQU1DLGdCQUFnQixHQUFHLFNBQW5CQSxnQkFBbUIsR0FBTTtBQUNsQyxTQUFPO0FBQ0hQLFFBQUksRUFBRUMsNERBQTJCTztBQUQ5QixHQUFQO0FBR0gsQ0FKTTtBQU9BLElBQU1DLFdBQVcsR0FBRyxTQUFkQSxXQUFjLENBQUNKLElBQUQsRUFBVTtBQUNqQyxTQUFPLFVBQUFLLFFBQVEsRUFBSTtBQUNmQSxZQUFRLENBQUVSLGVBQWUsRUFBakIsQ0FBUjs7QUFDQSxRQUFHO0FBRUMsVUFBSUcsSUFBSSxDQUFDTSxFQUFMLEtBQVlDLFNBQVosSUFBeUJQLElBQUksQ0FBQ00sRUFBTCxLQUFZLENBQXJDLElBQTBDTixJQUFJLENBQUNNLEVBQUwsSUFBVyxFQUF6RCxFQUE0RDtBQUN4RCxZQUFNRSxPQUFPLEdBQUdDLGlEQUFRLENBQUNDLFFBQVQsR0FBb0JDLEdBQXBCLENBQXdCLE1BQXhCLEVBQWdDQyxLQUFoQyxDQUFzQ1osSUFBSSxDQUFDTSxFQUEzQyxDQUFoQjtBQUNBRSxlQUFPLENBQUNLLE1BQVIsQ0FBZTtBQUNicEIsY0FBSSxFQUFFTyxJQUFJLENBQUNQLElBREU7QUFFYkMsY0FBSSxFQUFFTSxJQUFJLENBQUNOO0FBRkUsU0FBZjtBQUlBLFlBQU1vQixJQUFJLEdBQUc7QUFDVFIsWUFBRSxFQUFFLEVBREs7QUFFVGIsY0FBSSxFQUFHLEVBRkU7QUFHVEMsY0FBSSxFQUFHO0FBSEUsU0FBYjtBQUtBVyxnQkFBUSxDQUFDVSxlQUFlLENBQUNELElBQUQsQ0FBaEIsQ0FBUjtBQUNBVCxnQkFBUSxDQUFDTixnQkFBZ0IsQ0FBQ2UsSUFBRCxDQUFqQixDQUFSO0FBQ0gsT0FiRCxNQWFLO0FBQ0RULGdCQUFRLENBQUViLE9BQU8sQ0FBQ1EsSUFBSSxDQUFDUCxJQUFOLEVBQVdPLElBQUksQ0FBQ04sSUFBaEIsQ0FBVCxDQUFSOztBQUNBLFlBQU1jLFFBQU8sR0FBR0MsaURBQVEsQ0FBQ0MsUUFBVCxHQUFvQkMsR0FBcEIsQ0FBd0IsTUFBeEIsQ0FBaEI7O0FBQ0EsWUFBTUssT0FBTyxHQUFHUixRQUFPLENBQUNTLElBQVIsQ0FBYWpCLElBQWIsQ0FBaEI7O0FBQ0EsWUFBTWMsS0FBSSxHQUFHO0FBQ1RSLFlBQUUsRUFBR1UsT0FBTyxDQUFDRSxHQURKO0FBRVR6QixjQUFJLEVBQUdPLElBQUksQ0FBQ1AsSUFGSDtBQUdUQyxjQUFJLEVBQUdNLElBQUksQ0FBQ047QUFISCxTQUFiO0FBS0FXLGdCQUFRLENBQUVOLGdCQUFnQixDQUFDZSxLQUFELENBQWxCLENBQVI7QUFDSDtBQUNKLEtBMUJELENBMkJBLE9BQU1LLEdBQU4sRUFBVTtBQUNOZCxjQUFRLENBQUVILGdCQUFnQixFQUFsQixDQUFSO0FBQ0g7QUFFSixHQWpDRDtBQWtDSCxDQW5DTSxDLENBc0NQOztBQUVPLElBQU1rQixnQkFBZ0IsR0FBRyxTQUFuQkEsZ0JBQW1CLENBQUVDLEtBQUYsRUFBYTtBQUN6QyxTQUFPO0FBQ0gxQixRQUFJLEVBQUVDLDhEQURIO0FBRUhrQixRQUFJLEVBQUVPO0FBRkgsR0FBUDtBQUlILENBTE07QUFPQSxJQUFNQyxhQUFhLEdBQUcsU0FBaEJBLGFBQWdCLEdBQU07QUFDL0IsU0FBTztBQUNIM0IsUUFBSSxFQUFFQyw4REFBNkIyQjtBQURoQyxHQUFQO0FBR0gsQ0FKTTtBQU1BLElBQU1DLGNBQWMsR0FBRyxTQUFqQkEsY0FBaUIsR0FBTTtBQUNoQyxTQUFPO0FBQ0g3QixRQUFJLEVBQUVDLHVEQUFzQjZCO0FBRHpCLEdBQVA7QUFHSCxDQUpNO0FBTUEsSUFBTUMsU0FBUyxHQUFHLFNBQVpBLFNBQVksR0FBTTtBQUMzQixTQUFPLFVBQUFyQixRQUFRLEVBQUk7QUFDZkEsWUFBUSxDQUFDbUIsY0FBYyxFQUFmLENBQVI7O0FBQ0EsUUFBRztBQUNDLFVBQU1oQixPQUFPLEdBQUdDLGlEQUFRLENBQUNDLFFBQVQsR0FBb0JDLEdBQXBCLENBQXdCLE1BQXhCLENBQWhCO0FBQ0FILGFBQU8sQ0FBQ21CLEVBQVIsQ0FBVyxPQUFYLEVBQW9CLFVBQUNDLFFBQUQsRUFBYztBQUM5QixZQUFNQyxLQUFLLEdBQUdELFFBQVEsQ0FBQ0UsR0FBVCxFQUFkO0FBQ0EsWUFBTUMsUUFBUSxHQUFHLEVBQWpCOztBQUNBLGFBQUssSUFBSXpCLEVBQVQsSUFBZXVCLEtBQWYsRUFBc0I7QUFDbEJFLGtCQUFRLENBQUNkLElBQVQ7QUFBZ0JYLGNBQUUsRUFBRkE7QUFBaEIsYUFBdUJ1QixLQUFLLENBQUN2QixFQUFELENBQTVCO0FBQ0g7O0FBQ0RELGdCQUFRLENBQUNlLGdCQUFnQixDQUFDVyxRQUFELENBQWpCLENBQVI7QUFDSCxPQVBEO0FBUUgsS0FWRCxDQVdBLE9BQU1aLEdBQU4sRUFBVTtBQUNOYSxXQUFLO0FBQ0wzQixjQUFRLENBQUNpQixhQUFhLENBQUNILEdBQUQsQ0FBZCxDQUFSO0FBQ0g7QUFDSixHQWpCRDtBQWtCSCxDQW5CTSxDLENBc0JQOztBQUVPLElBQU1jLFVBQVUsR0FBRyxTQUFiQSxVQUFhLEdBQU07QUFDNUIsU0FBTztBQUNIdEMsUUFBSSxFQUFFQyx3REFBdUJzQztBQUQxQixHQUFQO0FBR0gsQ0FKTTtBQU1BLElBQU1DLGlCQUFpQixHQUFHLFNBQXBCQSxpQkFBb0IsQ0FBRWQsS0FBRixFQUFhO0FBQzFDLFNBQU87QUFDSDFCLFFBQUksRUFBRUMsK0RBQThCd0M7QUFEakMsR0FBUDtBQUdILENBSk07QUFNQSxJQUFNQyxjQUFjLEdBQUcsU0FBakJBLGNBQWlCLENBQUNsQixHQUFELEVBQVM7QUFDbkMsU0FBTztBQUNIeEIsUUFBSSxFQUFFQywrREFBOEIwQztBQURqQyxHQUFQO0FBR0gsQ0FKTTtBQU1BLElBQU1DLGNBQWMsR0FBRyxTQUFqQkEsY0FBaUIsQ0FBQ2pDLEVBQUQsRUFBUTtBQUNsQyxTQUFPLFVBQUFELFFBQVEsRUFBSTtBQUNmQSxZQUFRLENBQUM0QixVQUFVLEVBQVgsQ0FBUjs7QUFDQSxRQUFHO0FBQ0MsVUFBTXpCLE9BQU8sR0FBR0MsaURBQVEsQ0FBQ0MsUUFBVCxHQUFvQkMsR0FBcEIsQ0FBd0IsTUFBeEIsRUFBZ0NDLEtBQWhDLENBQXNDTixFQUF0QyxDQUFoQjtBQUNBRSxhQUFPLENBQUNnQyxNQUFSO0FBQ0FuQyxjQUFRLENBQUM4QixpQkFBaUIsRUFBbEIsQ0FBUjtBQUNILEtBSkQsQ0FLQSxPQUFNaEIsR0FBTixFQUFVO0FBQ05kLGNBQVEsQ0FBQ2dDLGNBQWMsQ0FBQ2xCLEdBQUQsQ0FBZixDQUFSO0FBQ0g7QUFDSixHQVZEO0FBV0gsQ0FaTSxDLENBY1A7O0FBRU8sSUFBTXNCLFFBQVEsR0FBRyxTQUFYQSxRQUFXLEdBQU07QUFDMUIsU0FBTztBQUNIOUMsUUFBSSxFQUFFQyxzREFBcUI4QztBQUR4QixHQUFQO0FBR0gsQ0FKTTtBQU1BLElBQU0zQixlQUFlLEdBQUcsU0FBbEJBLGVBQWtCLENBQUVNLEtBQUYsRUFBYTtBQUN4QyxTQUFPO0FBQ0gxQixRQUFJLEVBQUVDLDZEQURIO0FBRUhILFFBQUksRUFBRzRCLEtBQUssQ0FBQzVCLElBRlY7QUFHSEMsUUFBSSxFQUFHMkIsS0FBSyxDQUFDM0IsSUFIVjtBQUlIWSxNQUFFLEVBQUdlLEtBQUssQ0FBQ2Y7QUFKUixHQUFQO0FBTUgsQ0FQTTtBQVNBLElBQU1xQyxZQUFZLEdBQUcsU0FBZkEsWUFBZSxDQUFDeEIsR0FBRCxFQUFTO0FBQ2pDLFNBQU87QUFDSHhCLFFBQUksRUFBRUMsNkRBQTRCZ0Q7QUFEL0IsR0FBUDtBQUdILENBSk07QUFNQSxJQUFNQyxZQUFZLEdBQUcsU0FBZkEsWUFBZSxDQUFDdkMsRUFBRCxFQUFRO0FBQ2hDLFNBQU8sVUFBQUQsUUFBUSxFQUFJO0FBQ2YsUUFBRztBQUNDQSxjQUFRLENBQUNvQyxRQUFRLEVBQVQsQ0FBUjtBQUNBLFVBQU1qQyxPQUFPLEdBQUdDLGlEQUFRLENBQUNDLFFBQVQsR0FBb0JDLEdBQXBCLENBQXdCLE1BQXhCLEVBQWdDQyxLQUFoQyxDQUFzQ04sRUFBdEMsQ0FBaEI7QUFDQSxVQUFJTixJQUFJLEdBQUcsRUFBWDtBQUNBUSxhQUFPLENBQUNtQixFQUFSLENBQVcsT0FBWCxFQUFvQixVQUFDQyxRQUFELEVBQWM7QUFDaEMsWUFBT2tCLElBQUksR0FBRWxCLFFBQVEsQ0FBQ0UsR0FBVCxFQUFiO0FBQ0E5QixZQUFJLEdBQUU7QUFDSlAsY0FBSSxFQUFHcUQsSUFBSSxDQUFDckQsSUFEUjtBQUVKQyxjQUFJLEVBQUdvRCxJQUFJLENBQUNwRCxJQUZSO0FBR0pZLFlBQUUsRUFBR0E7QUFIRCxTQUFOO0FBS0QsT0FQRDtBQVFBRCxjQUFRLENBQUNVLGVBQWUsQ0FBQ2YsSUFBRCxDQUFoQixDQUFSO0FBQ0gsS0FiRCxDQWNBLE9BQU1tQixHQUFOLEVBQVU7QUFDTmQsY0FBUSxDQUFDc0MsWUFBWSxDQUFDeEIsR0FBRCxDQUFiLENBQVI7QUFDSDtBQUNKLEdBbEJEO0FBbUJILENBcEJNLEMsQ0FzQlA7O0FBRU8sSUFBTTRCLGdCQUFnQixHQUFHLFNBQW5CQSxnQkFBbUIsR0FBTTtBQUNsQyxTQUFPO0FBQ0hwRCxRQUFJLEVBQUVDLCtEQUE4Qm9EO0FBRGpDLEdBQVA7QUFHSCxDQUpNO0FBTUEsSUFBTUMsdUJBQXVCLEdBQUcsU0FBMUJBLHVCQUEwQixDQUFFNUIsS0FBRixFQUFhO0FBQ2hELFNBQU87QUFDSDFCLFFBQUksRUFBRUMsc0VBQXFDc0Q7QUFEeEMsR0FBUDtBQUdILENBSk07QUFNQSxJQUFNQyxvQkFBb0IsR0FBRyxTQUF2QkEsb0JBQXVCLENBQUNoQyxHQUFELEVBQVM7QUFDekMsU0FBTztBQUNIeEIsUUFBSSxFQUFFQyxzRUFBcUN3RDtBQUR4QyxHQUFQO0FBR0gsQ0FKTTtBQU1BLElBQU1DLG9CQUFvQixHQUFHLFNBQXZCQSxvQkFBdUIsQ0FBQy9DLEVBQUQsRUFBSWdELE1BQUosRUFBV3hCLEdBQVgsRUFBbUI7QUFDbkQsU0FBTyxVQUFBekIsUUFBUSxFQUFJO0FBQ2YsUUFBRztBQUNDQSxjQUFRLENBQUUwQyxnQkFBZ0IsRUFBbEIsQ0FBUjs7QUFDQSxVQUFJekMsRUFBRSxLQUFLQyxTQUFQLElBQW9CRCxFQUFFLEtBQUssQ0FBM0IsSUFBZ0NBLEVBQUUsSUFBSSxFQUExQyxFQUE2QztBQUN6QyxZQUFJaUQsVUFBVSxHQUFHekIsR0FBRyxLQUFLLENBQVIsR0FBWSxJQUFaLEdBQW1CLEtBQXBDO0FBQ0EsWUFBSTBCLFlBQVksR0FBRzFCLEdBQUcsS0FBSyxDQUFSLEdBQVksSUFBWixHQUFtQixLQUF0QztBQUNBLFlBQU10QixPQUFPLEdBQUdDLGlEQUFRLENBQUNDLFFBQVQsR0FBb0JDLEdBQXBCLENBQXdCLE1BQXhCLEVBQWdDQyxLQUFoQyxDQUFzQ04sRUFBdEMsQ0FBaEI7O0FBQ0EsWUFBR2dELE1BQU0sS0FBSyxVQUFkLEVBQXlCO0FBQ3JCOUMsaUJBQU8sQ0FBQ0ssTUFBUixDQUFlO0FBQ2I0QyxvQkFBUSxFQUFHRDtBQURFLFdBQWY7QUFHSCxTQUpELE1BSUs7QUFDRGhELGlCQUFPLENBQUNLLE1BQVIsQ0FBZTtBQUNiNkMsa0JBQU0sRUFBR0g7QUFESSxXQUFmO0FBR0g7O0FBRURsRCxnQkFBUSxDQUFFNEMsdUJBQXVCLEVBQXpCLENBQVI7QUFDSDtBQUNKLEtBbEJELENBbUJBLE9BQU05QixHQUFOLEVBQVU7QUFDTmQsY0FBUSxDQUFFOEMsb0JBQW9CLEVBQXRCLENBQVI7QUFDSDtBQUVKLEdBeEJEO0FBeUJILENBMUJNLEMsQ0E4QlA7O0FBR08sSUFBTVEsVUFBVSxHQUFHLFNBQWJBLFVBQWEsR0FBTTtBQUM1QixTQUFPO0FBQ0hoRSxRQUFJLEVBQUVDLHdEQUF1QmdFO0FBRDFCLEdBQVA7QUFHSCxDQUpNO0FBTUEsSUFBTUMsaUJBQWlCLEdBQUcsU0FBcEJBLGlCQUFvQixDQUFFeEMsS0FBRixFQUFhO0FBQzFDLFNBQU87QUFDSDFCLFFBQUksRUFBRUMsK0RBREg7QUFFSGtCLFFBQUksRUFBQ087QUFGRixHQUFQO0FBSUgsQ0FMTTtBQU9BLElBQU15QyxjQUFjLEdBQUcsU0FBakJBLGNBQWlCLENBQUMzQyxHQUFELEVBQVM7QUFDbkMsU0FBTztBQUNIeEIsUUFBSSxFQUFFQywrREFBOEJtRTtBQURqQyxHQUFQO0FBR0gsQ0FKTTtBQU1BLElBQU1DLGNBQWMsR0FBRyxTQUFqQkEsY0FBaUIsQ0FBQ0MsTUFBRCxFQUFZO0FBQ3RDLFNBQU8sVUFBQTVELFFBQVEsRUFBSTtBQUNmLFFBQUc7QUFDQ0EsY0FBUSxDQUFFc0QsVUFBVSxFQUFaLENBQVI7QUFDQSxVQUFJaEQsR0FBRyxHQUFHRixpREFBUSxDQUFDQyxRQUFULEdBQW9CQyxHQUFwQixDQUF3QixNQUF4QixDQUFWO0FBQ0EsVUFBTW9CLFFBQVEsR0FBRyxFQUFqQjs7QUFFQSxVQUFHa0MsTUFBTSxLQUFLLEdBQWQsRUFBa0I7QUFDZHRELFdBQUcsQ0FBQ3VELFlBQUosQ0FBaUIsUUFBakIsRUFBMkJDLE9BQTNCLENBQW1DLElBQW5DLEVBQXlDeEMsRUFBekMsQ0FBNEMsYUFBNUMsRUFBMkQsVUFBU0MsUUFBVCxFQUFtQjtBQUMxRSxjQUFJQyxLQUFLLEdBQUdELFFBQVEsQ0FBQ0UsR0FBVCxFQUFaO0FBQ0EsY0FBSWhCLElBQUksR0FBRSxFQUFWO0FBRUFBLGNBQUksR0FBRztBQUNIckIsZ0JBQUksRUFBRW9DLEtBQUssQ0FBQ3BDLElBRFQ7QUFFSEMsZ0JBQUksRUFBRW1DLEtBQUssQ0FBQ25DLElBRlQ7QUFHSGdFLGtCQUFNLEVBQUU3QixLQUFLLENBQUM2QixNQUhYO0FBSUhELG9CQUFRLEVBQUU1QixLQUFLLENBQUM0QixRQUpiO0FBS0huRCxjQUFFLEVBQUdzQixRQUFRLENBQUNWO0FBTFgsV0FBUDtBQU9BYSxrQkFBUSxDQUFDZCxJQUFULENBQWNILElBQWQ7QUFFSCxTQWJEO0FBY0gsT0FmRCxNQWVNLElBQUdtRCxNQUFNLEtBQUssR0FBZCxFQUFrQjtBQUNwQnRELFdBQUcsQ0FBQ3VELFlBQUosQ0FBaUIsVUFBakIsRUFBNkJDLE9BQTdCLENBQXFDLElBQXJDLEVBQTJDeEMsRUFBM0MsQ0FBOEMsYUFBOUMsRUFBNkQsVUFBU0MsUUFBVCxFQUFtQjtBQUM1RSxjQUFJQyxLQUFLLEdBQUdELFFBQVEsQ0FBQ0UsR0FBVCxFQUFaO0FBQ0EsY0FBSWhCLElBQUksR0FBRSxFQUFWO0FBRUFBLGNBQUksR0FBRztBQUNIckIsZ0JBQUksRUFBRW9DLEtBQUssQ0FBQ3BDLElBRFQ7QUFFSEMsZ0JBQUksRUFBRW1DLEtBQUssQ0FBQ25DLElBRlQ7QUFHSGdFLGtCQUFNLEVBQUU3QixLQUFLLENBQUM2QixNQUhYO0FBSUhELG9CQUFRLEVBQUU1QixLQUFLLENBQUM0QixRQUpiO0FBS0huRCxjQUFFLEVBQUdzQixRQUFRLENBQUNWO0FBTFgsV0FBUDtBQU9BYSxrQkFBUSxDQUFDZCxJQUFULENBQWNILElBQWQ7QUFDSCxTQVpEO0FBYUgsT0FkSyxNQWNEO0FBQ0RILFdBQUcsQ0FBQ3lELElBQUosQ0FBUyxPQUFULEVBQWtCLFVBQUN4QyxRQUFELEVBQWM7QUFDNUIsY0FBTUMsS0FBSyxHQUFHRCxRQUFRLENBQUNFLEdBQVQsRUFBZDs7QUFDQSxlQUFLLElBQUl4QixFQUFULElBQWV1QixLQUFmLEVBQXNCO0FBQ2pCRSxvQkFBUSxDQUFDZCxJQUFUO0FBQWdCWCxnQkFBRSxFQUFGQTtBQUFoQixlQUF1QnVCLEtBQUssQ0FBQ3ZCLEVBQUQsQ0FBNUI7QUFDSjtBQUNKLFNBTEQ7QUFNSDs7QUFDREQsY0FBUSxDQUFDd0QsaUJBQWlCLENBQUM5QixRQUFELENBQWxCLENBQVI7QUFDSCxLQTNDRCxDQTRDQSxPQUFNWixHQUFOLEVBQVU7QUFDTmQsY0FBUSxDQUFFeUQsY0FBYyxFQUFoQixDQUFSO0FBQ0g7QUFFSixHQWpERDtBQWtESCxDQW5ETSIsImZpbGUiOiJzdGF0aWMvd2VicGFjay9wYWdlcy9saXN0LjNkOWMyODJhYjBhYTM5MDM5ZGMzLmhvdC11cGRhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBhY3Rpb25UeXBlcyBmcm9tICcuL2FjdGlvblR5cGVzJztcclxuaW1wb3J0IGZpcmViYXNlIGZyb20gJy4uLy4uL2ZpcmViYXNlJztcclxuXHJcbi8vIGFkZCB0YXNrXHJcblxyXG5leHBvcnQgY29uc3QgYWRkVGFzayA9IChuYW1lLGRhdGUpID0+IHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgdHlwZTogYWN0aW9uVHlwZXMuQUREX1RBU0ssXHJcbiAgICAgICAgbmFtZTogbmFtZSxcclxuICAgICAgICBkYXRlIDogZGF0ZVxyXG4gICAgfTtcclxufTtcclxuXHJcblxyXG5leHBvcnQgY29uc3QgYWRkaW5nVGFza1N0YXJ0ID0gKCkgPT4ge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgICB0eXBlOiBhY3Rpb25UeXBlcy5BRERfVEFTS19TVEFSVFxyXG4gICAgfTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBhZGRpbmdUYXNrU3VjZXNzID0gKHRhc2spID0+IHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgdHlwZTogYWN0aW9uVHlwZXMuQUREX1RBU0tfU1VDRVNTXHJcbiAgICB9O1xyXG5cclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBhZGRpbmdUYXNrRmFpbGVkID0gKCkgPT4ge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgICB0eXBlOiBhY3Rpb25UeXBlcy5BRERfVEFTS19GQUlMRURcclxuICAgIH07XHJcbn07XHJcblxyXG5cclxuZXhwb3J0IGNvbnN0IGluaXRBZGRUYXNrID0gKHRhc2spID0+IHtcclxuICAgIHJldHVybiBkaXNwYXRjaCA9PiB7XHJcbiAgICAgICAgZGlzcGF0Y2goIGFkZGluZ1Rhc2tTdGFydCgpICk7XHJcbiAgICAgICAgdHJ5e1xyXG5cclxuICAgICAgICAgICAgaWYoIHRhc2suaWQgIT09IHVuZGVmaW5lZCAmJiB0YXNrLmlkICE9PSAwICYmIHRhc2suaWQgIT0gJycpe1xyXG4gICAgICAgICAgICAgICAgY29uc3QgdG9kb1JlZiA9IGZpcmViYXNlLmRhdGFiYXNlKCkucmVmKCd0b2RvJykuY2hpbGQodGFzay5pZCk7XHJcbiAgICAgICAgICAgICAgICB0b2RvUmVmLnVwZGF0ZSh7XHJcbiAgICAgICAgICAgICAgICAgIG5hbWU6IHRhc2submFtZSxcclxuICAgICAgICAgICAgICAgICAgZGF0ZTogdGFzay5kYXRlXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGRhdGEgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWQgOicnLFxyXG4gICAgICAgICAgICAgICAgICAgIG5hbWUgOiAnJyxcclxuICAgICAgICAgICAgICAgICAgICBkYXRlIDogJydcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGRpc3BhdGNoKGVkaXRUYXNrU3VjY2VzcyhkYXRhKSk7XHJcbiAgICAgICAgICAgICAgICBkaXNwYXRjaChhZGRpbmdUYXNrU3VjZXNzKGRhdGEpKTtcclxuICAgICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgICAgICBkaXNwYXRjaCggYWRkVGFzayh0YXNrLm5hbWUsdGFzay5kYXRlKSApO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgdG9kb1JlZiA9IGZpcmViYXNlLmRhdGFiYXNlKCkucmVmKCd0b2RvJyk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBhZGREYXRhID0gdG9kb1JlZi5wdXNoKHRhc2spO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZGF0YSA9IHtcclxuICAgICAgICAgICAgICAgICAgICBpZCA6IGFkZERhdGEua2V5LFxyXG4gICAgICAgICAgICAgICAgICAgIG5hbWUgOiB0YXNrLm5hbWUsXHJcbiAgICAgICAgICAgICAgICAgICAgZGF0ZSA6IHRhc2suZGF0ZVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgZGlzcGF0Y2goIGFkZGluZ1Rhc2tTdWNlc3MoZGF0YSkgKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBjYXRjaChlcnIpe1xyXG4gICAgICAgICAgICBkaXNwYXRjaCggYWRkaW5nVGFza0ZhaWxlZCgpICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH07XHJcbn07XHJcblxyXG5cclxuLy8gZ2V0IHRhc2sgbGlzdFxyXG5cclxuZXhwb3J0IGNvbnN0IGZldGNoVGFza1N1Y2Nlc3MgPSAoIHRhc2tzICkgPT4ge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgICB0eXBlOiBhY3Rpb25UeXBlcy5GRVRDSF9UQVNLX1NVQ0VTUyxcclxuICAgICAgICBkYXRhOiB0YXNrc1xyXG4gICAgfTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBmZXRjaFRhc2tGYWlsID0gKCkgPT4ge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgICB0eXBlOiBhY3Rpb25UeXBlcy5GRVRDSF9UQVNLX0ZBSUxFRCxcclxuICAgIH07XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgZmV0Y2hUYXNrU3RhcnQgPSAoKSA9PiB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICAgIHR5cGU6IGFjdGlvblR5cGVzLkZFVENIX1RBU0tcclxuICAgIH07XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgZmV0Y2hUYXNrID0gKCkgPT4ge1xyXG4gICAgcmV0dXJuIGRpc3BhdGNoID0+IHtcclxuICAgICAgICBkaXNwYXRjaChmZXRjaFRhc2tTdGFydCgpKTtcclxuICAgICAgICB0cnl7XHJcbiAgICAgICAgICAgIGNvbnN0IHRvZG9SZWYgPSBmaXJlYmFzZS5kYXRhYmFzZSgpLnJlZigndG9kbycpO1xyXG4gICAgICAgICAgICB0b2RvUmVmLm9uKCd2YWx1ZScsIChzbmFwc2hvdCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgdG9kb3MgPSBzbmFwc2hvdC52YWwoKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHRvZG9MaXN0ID0gW107XHJcbiAgICAgICAgICAgICAgICBmb3IgKGxldCBpZCBpbiB0b2Rvcykge1xyXG4gICAgICAgICAgICAgICAgICAgIHRvZG9MaXN0LnB1c2goeyBpZCwgLi4udG9kb3NbaWRdIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgZGlzcGF0Y2goZmV0Y2hUYXNrU3VjY2Vzcyh0b2RvTGlzdCkpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgY2F0Y2goZXJyKXtcclxuICAgICAgICAgICAgYWxlcnQoKTtcclxuICAgICAgICAgICAgZGlzcGF0Y2goZmV0Y2hUYXNrRmFpbChlcnIpKTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG59O1xyXG5cclxuXHJcbi8vIGRlbGV0ZSB0YXNrXHJcblxyXG5leHBvcnQgY29uc3QgZGVsZXRlVGFzayA9ICgpID0+IHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgdHlwZTogYWN0aW9uVHlwZXMuREVMRVRFX1RBU0tcclxuICAgIH07XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgZGVsZXRlVGFza1N1Y2Nlc3MgPSAoIHRhc2tzICkgPT4ge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgICB0eXBlOiBhY3Rpb25UeXBlcy5ERUxFVEVfVEFTS19TVUNFU1NcclxuICAgIH07XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgZGVsZXRlVGFza0ZhaWwgPSAoZXJyKSA9PiB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICAgIHR5cGU6IGFjdGlvblR5cGVzLkRFTEVURV9UQVNLX0ZBSUxFRFxyXG4gICAgfTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBpbml0RGVsZXRlVGFzayA9IChpZCkgPT4ge1xyXG4gICAgcmV0dXJuIGRpc3BhdGNoID0+IHtcclxuICAgICAgICBkaXNwYXRjaChkZWxldGVUYXNrKCkpO1xyXG4gICAgICAgIHRyeXtcclxuICAgICAgICAgICAgY29uc3QgdG9kb1JlZiA9IGZpcmViYXNlLmRhdGFiYXNlKCkucmVmKCd0b2RvJykuY2hpbGQoaWQpO1xyXG4gICAgICAgICAgICB0b2RvUmVmLnJlbW92ZSgpO1xyXG4gICAgICAgICAgICBkaXNwYXRjaChkZWxldGVUYXNrU3VjY2VzcygpKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgY2F0Y2goZXJyKXtcclxuICAgICAgICAgICAgZGlzcGF0Y2goZGVsZXRlVGFza0ZhaWwoZXJyKSk7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxufTtcclxuXHJcbi8vIGVkaXQgdGFzayBzdGFydFxyXG5cclxuZXhwb3J0IGNvbnN0IGVkaXRUYXNrID0gKCkgPT4ge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgICB0eXBlOiBhY3Rpb25UeXBlcy5FRElUX1RBU0tcclxuICAgIH07XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgZWRpdFRhc2tTdWNjZXNzID0gKCB0YXNrcyApID0+IHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgdHlwZTogYWN0aW9uVHlwZXMuRURJVF9UQVNLX1NVQ0VTUyxcclxuICAgICAgICBuYW1lIDogdGFza3MubmFtZSxcclxuICAgICAgICBkYXRlIDogdGFza3MuZGF0ZSxcclxuICAgICAgICBpZCA6IHRhc2tzLmlkXHJcbiAgICB9O1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGVkaXRUYXNrRmFpbCA9IChlcnIpID0+IHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgdHlwZTogYWN0aW9uVHlwZXMuRURJVF9UQVNLX0ZBSUxFRFxyXG4gICAgfTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBpbml0RWRpdFRhc2sgPSAoaWQpID0+IHtcclxuICAgIHJldHVybiBkaXNwYXRjaCA9PiB7XHJcbiAgICAgICAgdHJ5e1xyXG4gICAgICAgICAgICBkaXNwYXRjaChlZGl0VGFzaygpKTtcclxuICAgICAgICAgICAgY29uc3QgdG9kb1JlZiA9IGZpcmViYXNlLmRhdGFiYXNlKCkucmVmKCd0b2RvJykuY2hpbGQoaWQpO1xyXG4gICAgICAgICAgICBsZXQgdGFzayA9IHt9O1xyXG4gICAgICAgICAgICB0b2RvUmVmLm9uKCd2YWx1ZScsIChzbmFwc2hvdCkgPT4ge1xyXG4gICAgICAgICAgICAgIGNvbnN0ICB0b2RvID1zbmFwc2hvdC52YWwoKTtcclxuICAgICAgICAgICAgICB0YXNrID17XHJcbiAgICAgICAgICAgICAgICBuYW1lIDogdG9kby5uYW1lLFxyXG4gICAgICAgICAgICAgICAgZGF0ZSA6IHRvZG8uZGF0ZSxcclxuICAgICAgICAgICAgICAgIGlkIDogaWRcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBkaXNwYXRjaChlZGl0VGFza1N1Y2Nlc3ModGFzaykpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjYXRjaChlcnIpe1xyXG4gICAgICAgICAgICBkaXNwYXRjaChlZGl0VGFza0ZhaWwoZXJyKSk7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxufTtcclxuXHJcbi8vIGNoYW5nZSBzdGF0dXMgb2YgYWN0aXZlIG9yIGNvbXBsZXRlXHJcblxyXG5leHBvcnQgY29uc3QgY2hhbmdlU3RhdHVzVGFzayA9ICgpID0+IHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgdHlwZTogYWN0aW9uVHlwZXMuQ0hBTkdFX1NUQVRVU19UQVNLXHJcbiAgICB9O1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGNoYW5nZVN0YXR1c1Rhc2tTdWNjZXNzID0gKCB0YXNrcyApID0+IHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgdHlwZTogYWN0aW9uVHlwZXMuQ0hBTkdFX1NUQVRVU19UQVNLX1NVQ0VTU1xyXG4gICAgfTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBjaGFuZ2VTdGF0dXNUYXNrRmFpbCA9IChlcnIpID0+IHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgdHlwZTogYWN0aW9uVHlwZXMuQ0hBTkdFX1NUQVRVU19UQVNLX0ZBSUxFRFxyXG4gICAgfTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBpbml0Q2hhbmdlU3RhdHVzVGFzayA9IChpZCxzdGF0dXMsdmFsKSA9PiB7XHJcbiAgICByZXR1cm4gZGlzcGF0Y2ggPT4ge1xyXG4gICAgICAgIHRyeXtcclxuICAgICAgICAgICAgZGlzcGF0Y2goIGNoYW5nZVN0YXR1c1Rhc2soKSApO1xyXG4gICAgICAgICAgICBpZiggaWQgIT09IHVuZGVmaW5lZCAmJiBpZCAhPT0gMCAmJiBpZCAhPSAnJyl7XHJcbiAgICAgICAgICAgICAgICBsZXQgYWN0aXZlZmxhZyA9IHZhbCA9PT0gMSA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICAgICAgICAgIGxldCBjb21wbGV0ZWZsYWcgPSB2YWwgPT09IDEgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgICAgICAgICBjb25zdCB0b2RvUmVmID0gZmlyZWJhc2UuZGF0YWJhc2UoKS5yZWYoJ3RvZG8nKS5jaGlsZChpZCk7XHJcbiAgICAgICAgICAgICAgICBpZihzdGF0dXMgPT09ICdjb21wbGV0ZScpe1xyXG4gICAgICAgICAgICAgICAgICAgIHRvZG9SZWYudXBkYXRlKHtcclxuICAgICAgICAgICAgICAgICAgICAgIGNvbXBsZXRlIDogY29tcGxldGVmbGFnXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgICAgICAgICB0b2RvUmVmLnVwZGF0ZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgICBhY3RpdmUgOiBhY3RpdmVmbGFnXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgZGlzcGF0Y2goIGNoYW5nZVN0YXR1c1Rhc2tTdWNjZXNzKCkgKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBjYXRjaChlcnIpe1xyXG4gICAgICAgICAgICBkaXNwYXRjaCggY2hhbmdlU3RhdHVzVGFza0ZhaWwoKSApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICB9O1xyXG59O1xyXG5cclxuXHJcblxyXG4vLyBmaWx0ZXIgdGFzayBieSBhY3RpdmUvY29tcGxldGUvYWxsXHJcblxyXG5cclxuZXhwb3J0IGNvbnN0IGZpbHRlclRhc2sgPSAoKSA9PiB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICAgIHR5cGU6IGFjdGlvblR5cGVzLkZJTFRFUl9UQVNLXHJcbiAgICB9O1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGZpbHRlclRhc2tTdWNjZXNzID0gKCB0YXNrcyApID0+IHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgdHlwZTogYWN0aW9uVHlwZXMuRklMVEVSX1RBU0tfU1VDRVNTLFxyXG4gICAgICAgIGRhdGE6dGFza3NcclxuICAgIH07XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgZmlsdGVyVGFza0ZhaWwgPSAoZXJyKSA9PiB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICAgIHR5cGU6IGFjdGlvblR5cGVzLkZJTFRFUl9UQVNLX0ZBSUxFRFxyXG4gICAgfTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBpbml0RmlsdGVyVGFzayA9IChmaWx0ZXIpID0+IHtcclxuICAgIHJldHVybiBkaXNwYXRjaCA9PiB7XHJcbiAgICAgICAgdHJ5e1xyXG4gICAgICAgICAgICBkaXNwYXRjaCggZmlsdGVyVGFzaygpICk7XHJcbiAgICAgICAgICAgIHZhciByZWYgPSBmaXJlYmFzZS5kYXRhYmFzZSgpLnJlZihcInRvZG9cIik7XHJcbiAgICAgICAgICAgIGNvbnN0IHRvZG9MaXN0ID0gW107XHJcblxyXG4gICAgICAgICAgICBpZihmaWx0ZXIgPT09IFwiMVwiKXtcclxuICAgICAgICAgICAgICAgIHJlZi5vcmRlckJ5Q2hpbGQoXCJhY3RpdmVcIikuZXF1YWxUbyh0cnVlKS5vbihcImNoaWxkX2FkZGVkXCIsIGZ1bmN0aW9uKHNuYXBzaG90KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IHRvZG9zID0gc25hcHNob3QudmFsKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IGRhdGEgPXt9O1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBkYXRhID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiB0b2Rvcy5uYW1lLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkYXRlOiB0b2Rvcy5kYXRlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBhY3RpdmU6IHRvZG9zLmFjdGl2ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29tcGxldGU6IHRvZG9zLmNvbXBsZXRlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZCA6IHNuYXBzaG90LmtleVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB0b2RvTGlzdC5wdXNoKGRhdGEpO1xyXG5cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9ZWxzZSBpZihmaWx0ZXIgPT09IFwiMlwiKXtcclxuICAgICAgICAgICAgICAgIHJlZi5vcmRlckJ5Q2hpbGQoXCJjb21wbGV0ZVwiKS5lcXVhbFRvKHRydWUpLm9uKFwiY2hpbGRfYWRkZWRcIiwgZnVuY3Rpb24oc25hcHNob3QpIHtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgdG9kb3MgPSBzbmFwc2hvdC52YWwoKTtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgZGF0YSA9e307XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGRhdGEgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IHRvZG9zLm5hbWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGU6IHRvZG9zLmRhdGUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFjdGl2ZTogdG9kb3MuYWN0aXZlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb21wbGV0ZTogdG9kb3MuY29tcGxldGUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkIDogc25hcHNob3Qua2V5XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIHRvZG9MaXN0LnB1c2goZGF0YSk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgICAgICByZWYub25jZSgndmFsdWUnLCAoc25hcHNob3QpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCB0b2RvcyA9IHNuYXBzaG90LnZhbCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIGZvciAobGV0IGlkIGluIHRvZG9zKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICB0b2RvTGlzdC5wdXNoKHsgaWQsIC4uLnRvZG9zW2lkXSB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBkaXNwYXRjaChmaWx0ZXJUYXNrU3VjY2Vzcyh0b2RvTGlzdCkpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjYXRjaChlcnIpe1xyXG4gICAgICAgICAgICBkaXNwYXRjaCggZmlsdGVyVGFza0ZhaWwoKSApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICB9O1xyXG59O1xyXG4iXSwic291cmNlUm9vdCI6IiJ9