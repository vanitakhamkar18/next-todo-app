webpackHotUpdate_N_E("pages/list",{

/***/ "./components/TodoList.js":
/*!********************************!*\
  !*** ./components/TodoList.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_bootstrap_Table__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap/Table */ "./node_modules/react-bootstrap/esm/Table.js");
/* harmony import */ var react_bootstrap_ButtonGroup__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-bootstrap/ButtonGroup */ "./node_modules/react-bootstrap/esm/ButtonGroup.js");
/* harmony import */ var react_bootstrap_Button__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-bootstrap/Button */ "./node_modules/react-bootstrap/esm/Button.js");
/* harmony import */ var react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-bootstrap/Form */ "./node_modules/react-bootstrap/esm/Form.js");
/* harmony import */ var react_bootstrap_Container__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-bootstrap/Container */ "./node_modules/react-bootstrap/esm/Container.js");
/* harmony import */ var react_bootstrap_Row__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-bootstrap/Row */ "./node_modules/react-bootstrap/esm/Row.js");
/* harmony import */ var react_bootstrap_Col__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-bootstrap/Col */ "./node_modules/react-bootstrap/esm/Col.js");


var _jsxFileName = "D:\\PERSONAL\\Vanita\\React\\test\\todo-next\\components\\TodoList.js",
    _this = undefined;










var TodoList = function TodoList(props) {
  var tableData = props.data.map(function (todo, i) {
    var activeStatus = 0;
    var completeStatus = 0;
    var classes = []; // classes = todo.complete ?  classes.push('cls-complete') : [];
    // classes = todo.active ?  classes.push('task-active') : [];

    if (typeof todo.active === 'undefined' || !todo.active) {
      activeStatus = 1;
    } else {// classes.push('task-active');
    }

    if (typeof todo.complete === 'undefined' || !todo.complete) {
      completeStatus = 1;
    } else {
      classes.push('cls-complete');
    }

    return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tr", {
      className: classes,
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
        children: i + 1
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 32,
        columnNumber: 17
      }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
        children: [todo.name, "          ||              ", todo.date]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 33,
        columnNumber: 17
      }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap_ButtonGroup__WEBPACK_IMPORTED_MODULE_3__["default"], {
          "aria-label": "Basic example",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap_Button__WEBPACK_IMPORTED_MODULE_4__["default"], {
            className: "btn-color-2",
            onClick: function onClick() {
              return props["delete"](todo.id);
            },
            children: "Delete"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 37,
            columnNumber: 23
          }, _this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 35,
          columnNumber: 21
        }, _this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 34,
        columnNumber: 17
      }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_5__["default"].Check, {
          type: "checkbox",
          name: "active_status",
          onClick: function onClick() {
            return props.change(todo.id, 'active', activeStatus);
          },
          checked: typeof todo.active === 'undefined' || !todo.active ? false : true
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 41,
          columnNumber: 21
        }, _this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 40,
        columnNumber: 17
      }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap_Button__WEBPACK_IMPORTED_MODULE_4__["default"], {
          variant: "outline-primary",
          onClick: function onClick() {
            return props.change(todo.id, 'complete', completeStatus);
          },
          children: todo.complete ? 'Incomplete' : 'Complete'
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 44,
          columnNumber: 21
        }, _this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 43,
        columnNumber: 17
      }, _this)]
    }, i, true, {
      fileName: _jsxFileName,
      lineNumber: 31,
      columnNumber: 13
    }, _this);
  });
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, {
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap_Container__WEBPACK_IMPORTED_MODULE_6__["default"], {
      className: "mt-3",
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap_Row__WEBPACK_IMPORTED_MODULE_7__["default"], {
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap_Col__WEBPACK_IMPORTED_MODULE_8__["default"], {
          className: "table-title",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
            children: "List of Tasks: "
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 54,
            columnNumber: 46
          }, _this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 54,
          columnNumber: 17
        }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap_Col__WEBPACK_IMPORTED_MODULE_8__["default"], {
          className: "table-filter",
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
            className: "filter-title",
            children: "Filter Task By :"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 56,
            columnNumber: 21
          }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_5__["default"].Control, {
            size: "sm",
            as: "select",
            onChange: props.filter,
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("option", {
              value: "0",
              children: "All"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 58,
              columnNumber: 25
            }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("option", {
              value: "1",
              children: "Active"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 59,
              columnNumber: 25
            }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("option", {
              value: "2",
              children: "Complete"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 60,
              columnNumber: 25
            }, _this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 57,
            columnNumber: 21
          }, _this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 55,
          columnNumber: 17
        }, _this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 53,
        columnNumber: 17
      }, _this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 52,
      columnNumber: 13
    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
      className: "list-table",
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap_Table__WEBPACK_IMPORTED_MODULE_2__["default"], {
        responsive: true,
        bordered: true,
        hover: true,
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("thead", {
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tr", {
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("th", {
              children: "Sr No"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 69,
              columnNumber: 23
            }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("th", {
              children: "Task Name"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 70,
              columnNumber: 23
            }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("th", {
              children: "Action"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 71,
              columnNumber: 23
            }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("th", {
              children: "Active"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 72,
              columnNumber: 23
            }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("th", {
              children: "Status"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 73,
              columnNumber: 23
            }, _this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 68,
            columnNumber: 21
          }, _this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 67,
          columnNumber: 19
        }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tbody", {
          children: tableData
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 76,
          columnNumber: 19
        }, _this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 66,
        columnNumber: 17
      }, _this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 65,
      columnNumber: 13
    }, _this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 51,
    columnNumber: 9
  }, _this);
};

_c = TodoList;
/* harmony default export */ __webpack_exports__["default"] = (TodoList);

var _c;

$RefreshReg$(_c, "TodoList");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vY29tcG9uZW50cy9Ub2RvTGlzdC5qcyJdLCJuYW1lcyI6WyJUb2RvTGlzdCIsInByb3BzIiwidGFibGVEYXRhIiwiZGF0YSIsIm1hcCIsInRvZG8iLCJpIiwiYWN0aXZlU3RhdHVzIiwiY29tcGxldGVTdGF0dXMiLCJjbGFzc2VzIiwiYWN0aXZlIiwiY29tcGxldGUiLCJwdXNoIiwibmFtZSIsImRhdGUiLCJpZCIsImNoYW5nZSIsImZpbHRlciJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxJQUFNQSxRQUFRLEdBQUcsU0FBWEEsUUFBVyxDQUFDQyxLQUFELEVBQVc7QUFFeEIsTUFBTUMsU0FBUyxHQUFHRCxLQUFLLENBQUNFLElBQU4sQ0FBV0MsR0FBWCxDQUFlLFVBQUNDLElBQUQsRUFBTUMsQ0FBTixFQUFXO0FBQ3hDLFFBQUlDLFlBQVksR0FBRSxDQUFsQjtBQUNBLFFBQUlDLGNBQWMsR0FBRyxDQUFyQjtBQUNBLFFBQUlDLE9BQU8sR0FBRyxFQUFkLENBSHdDLENBSXpDO0FBQ0M7O0FBQ0EsUUFBRyxPQUFPSixJQUFJLENBQUNLLE1BQVosS0FBd0IsV0FBeEIsSUFBdUMsQ0FBQ0wsSUFBSSxDQUFDSyxNQUFoRCxFQUF3RDtBQUNwREgsa0JBQVksR0FBRyxDQUFmO0FBQ0gsS0FGRCxNQUdJLENBQ0Q7QUFDRjs7QUFDRCxRQUFHLE9BQU9GLElBQUksQ0FBQ00sUUFBWixLQUEwQixXQUExQixJQUF5QyxDQUFDTixJQUFJLENBQUNNLFFBQWxELEVBQTREO0FBQ3hESCxvQkFBYyxHQUFHLENBQWpCO0FBQ0gsS0FGRCxNQUVLO0FBQ0RDLGFBQU8sQ0FBQ0csSUFBUixDQUFhLGNBQWI7QUFDSDs7QUFFRCx3QkFDSTtBQUFZLGVBQVMsRUFBRUgsT0FBdkI7QUFBQSw4QkFDSTtBQUFBLGtCQUFLSCxDQUFDLEdBQUM7QUFBUDtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBREosZUFFSTtBQUFBLG1CQUFLRCxJQUFJLENBQUNRLElBQVYsZ0NBQTBDUixJQUFJLENBQUNTLElBQS9DO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQUZKLGVBR0k7QUFBQSwrQkFDSSxxRUFBQyxtRUFBRDtBQUFhLHdCQUFXLGVBQXhCO0FBQUEsaUNBRUUscUVBQUMsOERBQUQ7QUFBUSxxQkFBUyxFQUFDLGFBQWxCO0FBQWdDLG1CQUFPLEVBQUU7QUFBQSxxQkFBTWIsS0FBSyxVQUFMLENBQWFJLElBQUksQ0FBQ1UsRUFBbEIsQ0FBTjtBQUFBLGFBQXpDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFISixlQVNJO0FBQUEsK0JBQ0kscUVBQUMsNERBQUQsQ0FBTSxLQUFOO0FBQVksY0FBSSxFQUFDLFVBQWpCO0FBQTRCLGNBQUksRUFBQyxlQUFqQztBQUFpRCxpQkFBTyxFQUFFO0FBQUEsbUJBQU1kLEtBQUssQ0FBQ2UsTUFBTixDQUFhWCxJQUFJLENBQUNVLEVBQWxCLEVBQXFCLFFBQXJCLEVBQThCUixZQUE5QixDQUFOO0FBQUEsV0FBMUQ7QUFBNkcsaUJBQU8sRUFBRyxPQUFPRixJQUFJLENBQUNLLE1BQVosS0FBd0IsV0FBeEIsSUFBdUMsQ0FBQ0wsSUFBSSxDQUFDSyxNQUE5QyxHQUEwRCxLQUExRCxHQUFrRTtBQUF4TDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQVRKLGVBWUk7QUFBQSwrQkFDSSxxRUFBQyw4REFBRDtBQUFRLGlCQUFPLEVBQUMsaUJBQWhCO0FBQWtDLGlCQUFPLEVBQUU7QUFBQSxtQkFBTVQsS0FBSyxDQUFDZSxNQUFOLENBQWFYLElBQUksQ0FBQ1UsRUFBbEIsRUFBcUIsVUFBckIsRUFBZ0NQLGNBQWhDLENBQU47QUFBQSxXQUEzQztBQUFBLG9CQUFtR0gsSUFBSSxDQUFDTSxRQUFMLEdBQWdCLFlBQWhCLEdBQStCO0FBQWxJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBWko7QUFBQSxPQUFTTCxDQUFUO0FBQUE7QUFBQTtBQUFBO0FBQUEsYUFESjtBQWtCSCxHQXBDaUIsQ0FBbEI7QUFzQ0Esc0JBQ0kscUVBQUMsNENBQUQsQ0FBTyxRQUFQO0FBQUEsNEJBQ0kscUVBQUMsaUVBQUQ7QUFBVyxlQUFTLEVBQUMsTUFBckI7QUFBQSw2QkFDSSxxRUFBQywyREFBRDtBQUFBLGdDQUNBLHFFQUFDLDJEQUFEO0FBQUssbUJBQVMsRUFBQyxhQUFmO0FBQUEsaUNBQTZCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQTdCO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBREEsZUFFQSxxRUFBQywyREFBRDtBQUFLLG1CQUFTLEVBQUMsY0FBZjtBQUFBLGtDQUNJO0FBQUsscUJBQVMsRUFBQyxjQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQURKLGVBRUkscUVBQUMsNERBQUQsQ0FBTSxPQUFOO0FBQWMsZ0JBQUksRUFBQyxJQUFuQjtBQUF3QixjQUFFLEVBQUMsUUFBM0I7QUFBb0Msb0JBQVEsRUFBRUwsS0FBSyxDQUFDZ0IsTUFBcEQ7QUFBQSxvQ0FDSTtBQUFRLG1CQUFLLEVBQUMsR0FBZDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFESixlQUVJO0FBQVEsbUJBQUssRUFBQyxHQUFkO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQUZKLGVBR0k7QUFBUSxtQkFBSyxFQUFDLEdBQWQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBSEo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGFBREosZUFjSTtBQUFLLGVBQVMsRUFBQyxZQUFmO0FBQUEsNkJBQ0kscUVBQUMsNkRBQUQ7QUFBTyxrQkFBVSxNQUFqQjtBQUFrQixnQkFBUSxNQUExQjtBQUEyQixhQUFLLE1BQWhDO0FBQUEsZ0NBQ0U7QUFBQSxpQ0FDRTtBQUFBLG9DQUNFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQURGLGVBRUU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBRkYsZUFHRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFIRixlQUlFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQUpGLGVBS0U7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBTEY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFERixlQVVFO0FBQUEsb0JBQ0NmO0FBREQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFWRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGFBZEo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFdBREo7QUFpQ0gsQ0F6RUQ7O0tBQU1GLFE7QUEyRVNBLHVFQUFmIiwiZmlsZSI6InN0YXRpYy93ZWJwYWNrL3BhZ2VzL2xpc3QuZWE2ZjExZDAwZjcyNzJmZGQ3N2EuaG90LXVwZGF0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCAgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgVGFibGUgZnJvbSAncmVhY3QtYm9vdHN0cmFwL1RhYmxlJztcclxuaW1wb3J0IEJ1dHRvbkdyb3VwIGZyb20gJ3JlYWN0LWJvb3RzdHJhcC9CdXR0b25Hcm91cCc7XHJcbmltcG9ydCBCdXR0b24gZnJvbSAncmVhY3QtYm9vdHN0cmFwL0J1dHRvbic7XHJcbmltcG9ydCBGb3JtIGZyb20gJ3JlYWN0LWJvb3RzdHJhcC9Gb3JtJztcclxuaW1wb3J0IENvbnRhaW5lciBmcm9tICdyZWFjdC1ib290c3RyYXAvQ29udGFpbmVyJztcclxuaW1wb3J0IFJvdyBmcm9tICdyZWFjdC1ib290c3RyYXAvUm93JztcclxuaW1wb3J0IENvbCBmcm9tICdyZWFjdC1ib290c3RyYXAvQ29sJztcclxuXHJcbmNvbnN0IFRvZG9MaXN0ID0gKHByb3BzKSA9PiB7XHJcblxyXG4gICAgY29uc3QgdGFibGVEYXRhID0gcHJvcHMuZGF0YS5tYXAoKHRvZG8saSkgPT57XHJcbiAgICAgICAgbGV0IGFjdGl2ZVN0YXR1cyA9MDtcclxuICAgICAgICBsZXQgY29tcGxldGVTdGF0dXMgPSAwO1xyXG4gICAgICAgIGxldCBjbGFzc2VzID0gW107XHJcbiAgICAgICAvLyBjbGFzc2VzID0gdG9kby5jb21wbGV0ZSA/ICBjbGFzc2VzLnB1c2goJ2Nscy1jb21wbGV0ZScpIDogW107XHJcbiAgICAgICAgLy8gY2xhc3NlcyA9IHRvZG8uYWN0aXZlID8gIGNsYXNzZXMucHVzaCgndGFzay1hY3RpdmUnKSA6IFtdO1xyXG4gICAgICAgIGlmKHR5cGVvZiB0b2RvLmFjdGl2ZSA9PT0gICd1bmRlZmluZWQnIHx8ICF0b2RvLmFjdGl2ZSApe1xyXG4gICAgICAgICAgICBhY3RpdmVTdGF0dXMgPSAxO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNle1xyXG4gICAgICAgICAgIC8vIGNsYXNzZXMucHVzaCgndGFzay1hY3RpdmUnKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYodHlwZW9mIHRvZG8uY29tcGxldGUgPT09ICAndW5kZWZpbmVkJyB8fCAhdG9kby5jb21wbGV0ZSApe1xyXG4gICAgICAgICAgICBjb21wbGV0ZVN0YXR1cyA9IDE7XHJcbiAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgIGNsYXNzZXMucHVzaCgnY2xzLWNvbXBsZXRlJyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4oXHJcbiAgICAgICAgICAgIDx0ciBrZXk9e2l9IGNsYXNzTmFtZT17Y2xhc3Nlc30+XHJcbiAgICAgICAgICAgICAgICA8dGQ+e2krMX08L3RkPlxyXG4gICAgICAgICAgICAgICAgPHRkPnt0b2RvLm5hbWV9ICAgICAgICAgIHx8ICAgICAgICAgICAgICB7dG9kby5kYXRlfTwvdGQ+XHJcbiAgICAgICAgICAgICAgICA8dGQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPEJ1dHRvbkdyb3VwIGFyaWEtbGFiZWw9XCJCYXNpYyBleGFtcGxlXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICB7Lyo8QnV0dG9uIGNsYXNzTmFtZT1cImJ0bi1jb2xvci0xXCIgb25DbGljaz17KCkgPT4gcHJvcHMuZWRpdCh0b2RvLmlkKX0+RWRpdDwvQnV0dG9uPiovfVxyXG4gICAgICAgICAgICAgICAgICAgICAgPEJ1dHRvbiBjbGFzc05hbWU9XCJidG4tY29sb3ItMlwiIG9uQ2xpY2s9eygpID0+IHByb3BzLmRlbGV0ZSh0b2RvLmlkKX0+RGVsZXRlPC9CdXR0b24+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9CdXR0b25Hcm91cD5cclxuICAgICAgICAgICAgICAgIDwvdGQ+XHJcbiAgICAgICAgICAgICAgICA8dGQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPEZvcm0uQ2hlY2sgdHlwZT1cImNoZWNrYm94XCIgbmFtZT1cImFjdGl2ZV9zdGF0dXNcIiBvbkNsaWNrPXsoKSA9PiBwcm9wcy5jaGFuZ2UodG9kby5pZCwnYWN0aXZlJyxhY3RpdmVTdGF0dXMpfSBjaGVja2VkPXsodHlwZW9mIHRvZG8uYWN0aXZlID09PSAgJ3VuZGVmaW5lZCcgfHwgIXRvZG8uYWN0aXZlICkgID8gZmFsc2UgOiB0cnVlfS8+XHJcbiAgICAgICAgICAgICAgICA8L3RkPlxyXG4gICAgICAgICAgICAgICAgPHRkPlxyXG4gICAgICAgICAgICAgICAgICAgIDxCdXR0b24gdmFyaWFudD1cIm91dGxpbmUtcHJpbWFyeVwiIG9uQ2xpY2s9eygpID0+IHByb3BzLmNoYW5nZSh0b2RvLmlkLCdjb21wbGV0ZScsY29tcGxldGVTdGF0dXMpfT57dG9kby5jb21wbGV0ZSA/ICdJbmNvbXBsZXRlJyA6ICdDb21wbGV0ZSd9PC9CdXR0b24+XHJcbiAgICAgICAgICAgICAgICA8L3RkPlxyXG4gICAgICAgICAgICA8L3RyPlxyXG4gICAgICAgIClcclxuICAgIH0pO1xyXG5cclxuICAgIHJldHVybihcclxuICAgICAgICA8UmVhY3QuRnJhZ21lbnQ+XHJcbiAgICAgICAgICAgIDxDb250YWluZXIgY2xhc3NOYW1lPVwibXQtM1wiPlxyXG4gICAgICAgICAgICAgICAgPFJvdz5cclxuICAgICAgICAgICAgICAgIDxDb2wgY2xhc3NOYW1lPVwidGFibGUtdGl0bGVcIj48aDU+TGlzdCBvZiBUYXNrczogPC9oNT48L0NvbD5cclxuICAgICAgICAgICAgICAgIDxDb2wgY2xhc3NOYW1lPVwidGFibGUtZmlsdGVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmaWx0ZXItdGl0bGVcIj5GaWx0ZXIgVGFzayBCeSA6PC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPEZvcm0uQ29udHJvbCBzaXplPVwic21cIiBhcz1cInNlbGVjdFwiIG9uQ2hhbmdlPXtwcm9wcy5maWx0ZXJ9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uIHZhbHVlPVwiMFwiPkFsbDwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uIHZhbHVlPVwiMVwiPkFjdGl2ZTwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uIHZhbHVlPVwiMlwiPkNvbXBsZXRlPC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9Gb3JtLkNvbnRyb2w+XHJcbiAgICAgICAgICAgICAgICA8L0NvbD5cclxuICAgICAgICAgICAgICAgIDwvUm93PlxyXG4gICAgICAgICAgICA8L0NvbnRhaW5lcj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJsaXN0LXRhYmxlXCI+XHJcbiAgICAgICAgICAgICAgICA8VGFibGUgcmVzcG9uc2l2ZSBib3JkZXJlZCBob3ZlciA+XHJcbiAgICAgICAgICAgICAgICAgIDx0aGVhZD5cclxuICAgICAgICAgICAgICAgICAgICA8dHI+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8dGg+U3IgTm88L3RoPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPHRoPlRhc2sgTmFtZTwvdGg+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8dGg+QWN0aW9uPC90aD5cclxuICAgICAgICAgICAgICAgICAgICAgIDx0aD5BY3RpdmU8L3RoPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPHRoPlN0YXR1czwvdGg+XHJcbiAgICAgICAgICAgICAgICAgICAgPC90cj5cclxuICAgICAgICAgICAgICAgICAgPC90aGVhZD5cclxuICAgICAgICAgICAgICAgICAgPHRib2R5PlxyXG4gICAgICAgICAgICAgICAgICB7dGFibGVEYXRhfVxyXG4gICAgICAgICAgICAgICAgICA8L3Rib2R5PlxyXG4gICAgICAgICAgICAgICAgPC9UYWJsZT5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9SZWFjdC5GcmFnbWVudD5cclxuICAgICk7XHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IFRvZG9MaXN0O1xyXG5cclxuXHJcblxyXG4iXSwic291cmNlUm9vdCI6IiJ9