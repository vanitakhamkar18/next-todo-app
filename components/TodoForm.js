import React  from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

const TodoForm = (props) => {
    console.log(props);
    return(
        <Form id="todo-form" onSubmit={props.handleSubmit}>
        <Form.Group controlId="fromTodo">
            <Form.Label>Add Task :</Form.Label>
          <Row>
            <Col>
            <input type="hidden" id="taskId" value={props.data.id} />
              <Form.Control type="text"  required id="name" defaultValue={typeof props.data.name ===  'undefined' ? '' : props.data.name} placeholder="Task Name" onChange={props.handleChange} />
                {props.validate.length > 0 &&
                <span className='error'>{props.validate}</span>}
              <Form.Text id="taskHelp" muted>
                Task name should contains only alphabets
              </Form.Text>
            </Col>
            <Col>
              <Form.Control type="date" required id="date" defaultValue={typeof props.data.date ===  'undefined' ? '' : props.data.date} onChange={props.handleChange} />
            </Col>
          </Row>
        </Form.Group>
          <Row>
            <Button variant="primary" type="submit" className="ml-3">
                Submit
            </Button>
          </Row>
        </Form>
    );
}

export default TodoForm;
