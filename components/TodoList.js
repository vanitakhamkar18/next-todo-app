import React  from 'react';
import Table from 'react-bootstrap/Table';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

const TodoList = (props) => {

    const tableData = props.data.map((todo,i) =>{
        let activeStatus =0;
        let completeStatus = 0;
        let classes = [];
       // classes = todo.complete ?  classes.push('cls-complete') : [];
        // classes = todo.active ?  classes.push('task-active') : [];
        if(typeof todo.active ===  'undefined' || !todo.active ){
            activeStatus = 1;
        }
        else{
           // classes.push('task-active');
        }
        if(typeof todo.complete ===  'undefined' || !todo.complete ){
            completeStatus = 1;
        }else{
            classes.push('cls-complete');
        }

        return(
            <tr key={i} className={classes}>
                <td>{i+1}</td>
                <td>{todo.name}          ||              {todo.date}</td>
                <td>
                    <ButtonGroup aria-label="Basic example">
                      {/*<Button className="btn-color-1" onClick={() => props.edit(todo.id)}>Edit</Button>*/}
                      <Button className="btn-color-2" onClick={() => props.delete(todo.id)}>Delete</Button>
                    </ButtonGroup>
                </td>
                <td>
                    <Form.Check type="checkbox" name="active_status" onClick={() => props.change(todo.id,'active',activeStatus)} checked={(typeof todo.active ===  'undefined' || !todo.active )  ? false : true}/>
                </td>
                <td>
                    <Button variant="outline-primary" onClick={() => props.change(todo.id,'complete',completeStatus)}>{todo.complete ? 'Incomplete' : 'Complete'}</Button>
                </td>
            </tr>
        )
    });

    return(
        <React.Fragment>
            <Container className="mt-3">
                <Row>
                <Col className="table-title"><h5>List of Tasks: </h5></Col>
                <Col className="table-filter">
                    <div className="filter-title">Filter Task By :</div>
                    <Form.Control size="sm" as="select" onChange={props.filter}>
                        <option value="0">All</option>
                        <option value="1">Active</option>
                        <option value="2">Complete</option>
                    </Form.Control>
                </Col>
                </Row>
            </Container>
            <div className="list-table">
                <Table responsive bordered hover >
                  <thead>
                    <tr>
                      <th>Sr No</th>
                      <th>Task Name</th>
                      <th>Action</th>
                      <th>Active</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                  {tableData}
                  </tbody>
                </Table>
            </div>
        </React.Fragment>
    );
}

export default TodoList;



