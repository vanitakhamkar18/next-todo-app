import React , { Component } from 'react';
import { connect } from 'react-redux';
import TodoForm from '../components/TodoForm';
import Message from '../components/Message';
import * as actions from '../store/actions/index';



class TodoAppBuilder extends Component {
    state = {
        name : '',
        date : '',
        loading : false,
        error : false,
        showMsg : true,
        validate : false,
        errorNameMsg :''
    }

    componentDidMount () {
        if(this.props.edited){
            this.setState({
                ...this.state,
                name : this.props.name,
                date : this.props.date,
                showMsg : true
            });
        }
    }

    componentDidUpdate () {
        if(this.props.edited){
            document.body.scrollTop = 0;
        }
    }

    handleSubmit = (event) => {
        console.log('== here in Builder===');
        event.preventDefault();

        let task = {
            name : this.state.name,
            date : this.state.date,
            complete : false,
            active: true
        };
        if(document.getElementById("taskId").value != ''){
            task = {...task,id : document.getElementById("taskId").value};
        }
         this.setState({
                ...this.state,
                showMsg : true
            });
         if(this.state.validate){
            this.props.onSubmitTask(task);
         }

    }

    handleChange = (event) => {
        event.preventDefault();
        const { type, value } = event.target;
        let letters = /^[A-Za-z]+$/;

        if(event.target.type == 'text') {
            /*if(!value.match(letters)){
                this.setState({
                     ...this.state,
                    validate : false,
                    errorNameMsg : 'Please Enter Letters Only.'
                });
            }else{*/
                this.setState({
                    ...this.state,
                    validate : true,
                    name : event.target.value
                });
           // }
        }
        if(event.target.type === 'date'){
            this.setState({
                ...this.state,
                date : event.target.value
            });
        }
    }

    handleMsg = () => {
        this.setState({
                ...this.state,
                showMsg : false
        });
        document.getElementById("todo-form").reset();
    }

    render(){
        let msg = (this.props.added ) ? (<Message show={this.state.showMsg} msg="Task Added Successfully ... !!!" setShow={this.handleMsg} />) : '';
        let data = {};
        if(this.props.id !== 0){
            data = {
                name : this.props.name,
                date : this.props.date,
                id : this.props.id
            }
        }

        return (
            <React.Fragment>

            <TodoForm data={data} validate={this.state.errorNameMsg} handleSubmit={this.handleSubmit} handleChange={this.handleChange}/>
            {msg}
            </React.Fragment>
        );
    }
}


const mapStateToProps = state => {
    console.log(state);
    return {
        name: state.todoTask.name,
        date : state.todoTask.date,
        id : state.todoTask.id,
        added : state.todoTask.added,
        edited : state.todoTask.edited,
        error: state.todoTask.error,
        loading: state.todoTask.loading
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onSubmitTask: (task) => dispatch(actions.initAddTask(task))
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(TodoAppBuilder);
// export default (TodoAppBuilder);
