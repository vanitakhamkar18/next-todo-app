import React , { Component } from 'react';
import { connect } from 'react-redux';
import TodoList from '../components/TodoList';
import ErrorBoundary from '../hoc/ErrorBoundary';
import Spinner from 'react-bootstrap/Spinner';
import Alert from 'react-bootstrap/Alert';
import * as actions from '../store/actions/index';


class TodoListBuilder extends Component {

    componentDidMount () {
        this.props.onFetchTask();
    }

    handleSubmit = (event) => {
        event.preventDefault();
    }

    handleChange = (id,status,val) => {
        this.props.onChangeStatusTask(id,status,val);
    }

    handleFilter = (event) => {
        this.props.onFilterTask(event.target.value);
    }

    handleEdit = (id) => {
        this.props.onEditTask(id);
    }

    handleDelete = (id) => {
        this.props.onDeleteTask(id);
    }

    render(){
        let todoList = <Alert variant="info" className="mt-3">
                          <p>
                            No Task to show, Please add task to show list of the task added. ..!!
                          </p>
                        </Alert>;
        if(this.props.error){
            todoList = <Alert variant="info" className="mt-3">
                          <p>
                           SomeThing Went Wrong. Please Try Again Later ..!!
                          </p>
                        </Alert>;
        }else if(this.props.data && this.props.data.length > 0){
            todoList = <TodoList
                        data={this.props.data}
                        edit={this.handleEdit}
                        delete={this.handleDelete}
                        change={this.handleChange}
                        filter={this.handleFilter}
                        />;
        }
        let tpl = this.props.loading ?  (<Spinner className="mt-60" animation="border" variant="info" />) : todoList ;

        return (
        <ErrorBoundary>
            {tpl}
        </ErrorBoundary>
        );
    }
}

const mapStateToProps = state => {
    console.log(state);
    return {
        data : state.todoTask.data,
        error: state.todoTask.error,
        loading: state.todoTask.loading
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchTask : () => dispatch(actions.fetchTask()),
        onDeleteTask : (id) => dispatch(actions.initDeleteTask(id)),
        onEditTask : (id) => dispatch(actions.initEditTask(id)),
        onChangeStatusTask : (id,status,val) => dispatch(actions.initChangeStatusTask(id,status,val)),
        onFilterTask : (filter) => dispatch(actions.initFilterTask(filter)),
    };
};

export default connect(mapStateToProps,mapDispatchToProps)(TodoListBuilder);
//export default (TodoListBuilder);
