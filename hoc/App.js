import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';

// import '../index.css';
import TodoAppReducer from '../store/reducers/TodoAppReducer';

const composeEnhancers =  compose;

const rootReducer = combineReducers({
    todoTask : TodoAppReducer
});

const store = createStore(rootReducer, composeEnhancers(
    applyMiddleware(thunk)
));


const app = (props) => {

     <Provider store={store}>
        {props.children}
     </Provider>
};


export default app;


