import React, { Component } from "react";
import Link from "next/link";
import Router from "next/router";
import Layout from '../hoc/Layout';
import App from '../hoc/App';
import 'bootstrap/dist/css/bootstrap.min.css';
// import '../App.css';
import TodoAppBuilder from '../containers/TodoAppBuilder';
import TodoListBuilder from '../containers/TodoListBuilder';

class IndexPage extends Component {
  static getInitialProps(context) {
    const promise = new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({ appName: "Todo App" });
      }, 1000);
    });
    return promise;
  }

  render() {
    return (
      <div>
        <h1>The Main Page of {this.props.appName}</h1>

        <Layout>
        <button onClick={() => Router.push("/list")}>Go to Listing</button>
        <TodoAppBuilder />
            {/*
            <TodoListBuilder />*/}
        </Layout>
      </div>
    );
  }
}

export default IndexPage;
