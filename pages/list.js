import React, { Component } from "react";
import Link from "next/link";
import Router from "next/router";
import Layout from '../hoc/Layout';
import App from '../hoc/App';
import 'bootstrap/dist/css/bootstrap.min.css';
// import '../App.css';
import TodoListBuilder from '../containers/TodoListBuilder';

class ListPage extends Component {
  static getInitialProps(context) {
    const promise = new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({ appName: "Super App" });
      }, 1000);
    });
    return promise;
  }

  render() {
    return (
      <div>
        <h1>The Main Page of {this.props.appName}</h1>
        <p>
          Go to{" "}
          <Link href="/">
            <a>Home</a>
          </Link>
        </p>
        <Layout>
        <TodoListBuilder />
            {/*
            <TodoListBuilder />*/}
        </Layout>
      </div>
    );
  }
}

export default ListPage;
