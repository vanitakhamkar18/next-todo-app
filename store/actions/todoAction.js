import * as actionTypes from './actionTypes';
import firebase from '../../firebase';

// add task

export const addTask = (name,date) => {
    return {
        type: actionTypes.ADD_TASK,
        name: name,
        date : date
    };
};


export const addingTaskStart = () => {
    return {
        type: actionTypes.ADD_TASK_START
    };
};

export const addingTaskSucess = (task) => {
    return {
        type: actionTypes.ADD_TASK_SUCESS
    };

};

export const addingTaskFailed = () => {
    return {
        type: actionTypes.ADD_TASK_FAILED
    };
};


export const initAddTask = (task) => {
    return dispatch => {
        dispatch( addingTaskStart() );
        try{

            if( task.id !== undefined && task.id !== 0 && task.id != ''){
                const todoRef = firebase.database().ref('todo').child(task.id);
                todoRef.update({
                  name: task.name,
                  date: task.date
                });
                const data = {
                    id :'',
                    name : '',
                    date : ''
                }
                dispatch(editTaskSuccess(data));
                dispatch(addingTaskSucess(data));
            }else{
                dispatch( addTask(task.name,task.date) );
                const todoRef = firebase.database().ref('todo');
                const addData = todoRef.push(task);
                const data = {
                    id : addData.key,
                    name : task.name,
                    date : task.date
                }
                dispatch( addingTaskSucess(data) );
            }
        }
        catch(err){
            dispatch( addingTaskFailed() );
        }

    };
};


// get task list

export const fetchTaskSuccess = ( tasks ) => {
    return {
        type: actionTypes.FETCH_TASK_SUCESS,
        data: tasks
    };
};

export const fetchTaskFail = () => {
    return {
        type: actionTypes.FETCH_TASK_FAILED,
    };
};

export const fetchTaskStart = () => {
    return {
        type: actionTypes.FETCH_TASK
    };
};

export const fetchTask = () => {
    return dispatch => {
        dispatch(fetchTaskStart());
        try{
            const todoRef = firebase.database().ref('todo');
            todoRef.on('value', (snapshot) => {
                const todos = snapshot.val();
                const todoList = [];
                for (let id in todos) {
                    todoList.push({ id, ...todos[id] });
                }
                dispatch(fetchTaskSuccess(todoList));
            });
        }
        catch(err){
            alert();
            dispatch(fetchTaskFail(err));
        }
    };
};


// delete task

export const deleteTask = () => {
    return {
        type: actionTypes.DELETE_TASK
    };
};

export const deleteTaskSuccess = ( tasks ) => {
    return {
        type: actionTypes.DELETE_TASK_SUCESS
    };
};

export const deleteTaskFail = (err) => {
    return {
        type: actionTypes.DELETE_TASK_FAILED
    };
};

export const initDeleteTask = (id) => {
    return dispatch => {
        dispatch(deleteTask());
        try{
            const todoRef = firebase.database().ref('todo').child(id);
            todoRef.remove();
            dispatch(deleteTaskSuccess());
        }
        catch(err){
            dispatch(deleteTaskFail(err));
        }
    };
};

// edit task start

export const editTask = () => {
    return {
        type: actionTypes.EDIT_TASK
    };
};

export const editTaskSuccess = ( tasks ) => {
    return {
        type: actionTypes.EDIT_TASK_SUCESS,
        name : tasks.name,
        date : tasks.date,
        id : tasks.id
    };
};

export const editTaskFail = (err) => {
    return {
        type: actionTypes.EDIT_TASK_FAILED
    };
};

export const initEditTask = (id) => {
    return dispatch => {
        try{
            dispatch(editTask());
            const todoRef = firebase.database().ref('todo').child(id);
            let task = {};
            todoRef.on('value', (snapshot) => {
              const  todo =snapshot.val();
              task ={
                name : todo.name,
                date : todo.date,
                id : id
              }
            });
            dispatch(editTaskSuccess(task));
        }
        catch(err){
            dispatch(editTaskFail(err));
        }
    };
};

// change status of active or complete

export const changeStatusTask = () => {
    return {
        type: actionTypes.CHANGE_STATUS_TASK
    };
};

export const changeStatusTaskSuccess = ( tasks ) => {
    return {
        type: actionTypes.CHANGE_STATUS_TASK_SUCESS
    };
};

export const changeStatusTaskFail = (err) => {
    return {
        type: actionTypes.CHANGE_STATUS_TASK_FAILED
    };
};

export const initChangeStatusTask = (id,status,val) => {
    return dispatch => {
        try{
            dispatch( changeStatusTask() );
            if( id !== undefined && id !== 0 && id != ''){
                let activeflag = val === 1 ? true : false;
                let completeflag = val === 1 ? true : false;
                const todoRef = firebase.database().ref('todo').child(id);
                if(status === 'complete'){
                    todoRef.update({
                      complete : completeflag
                    });
                }else{
                    todoRef.update({
                      active : activeflag
                    });
                }

                dispatch( changeStatusTaskSuccess() );
            }
        }
        catch(err){
            dispatch( changeStatusTaskFail() );
        }

    };
};



// filter task by active/complete/all


export const filterTask = () => {
    return {
        type: actionTypes.FILTER_TASK
    };
};

export const filterTaskSuccess = ( tasks ) => {
    return {
        type: actionTypes.FILTER_TASK_SUCESS,
        data:tasks
    };
};

export const filterTaskFail = (err) => {
    return {
        type: actionTypes.FILTER_TASK_FAILED
    };
};

export const initFilterTask = (filter) => {
    return dispatch => {
        try{
            dispatch( filterTask() );
            var ref = firebase.database().ref("todo");
            const todoList = [];

            if(filter === "1"){
                ref.orderByChild("active").equalTo(true).on("child_added", function(snapshot) {
                    let todos = snapshot.val();
                    let data ={};

                    data = {
                        name: todos.name,
                        date: todos.date,
                        active: todos.active,
                        complete: todos.complete,
                        id : snapshot.key
                    }
                    todoList.push(data);

                });
            }else if(filter === "2"){
                ref.orderByChild("complete").equalTo(true).on("child_added", function(snapshot) {
                    let todos = snapshot.val();
                    let data ={};

                    data = {
                        name: todos.name,
                        date: todos.date,
                        active: todos.active,
                        complete: todos.complete,
                        id : snapshot.key
                    }
                    todoList.push(data);
                });
            }else{
                ref.once('value', (snapshot) => {
                    const todos = snapshot.val();
                    for (let id in todos) {
                         todoList.push({ id, ...todos[id] });
                    }
                });
            }
            dispatch(filterTaskSuccess(todoList));
        }
        catch(err){
            dispatch( filterTaskFail() );
        }

    };
};
