import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    data: [],
    name : '',
    date : '',
    id: 0,
    added : false,
    loading: false,
    error: false,
    edited : false
};

const taskInit = ( state, action ) => {
    return updateObject( state, { loading: true } );
};

const taskAdd = ( state, action ) => {
    return updateObject( state, { loading: false } );
};


const taskAddedSucces = ( state, action ) => {
    return updateObject( state, {
        loading: false,
        added : true
    } );
};

const taskAddedFalied = ( state, action ) => {
    return updateObject( state, { error: true } );
};


const fetchtaskStart = ( state, action ) => {
    return updateObject( state, { loading: true } );
};

const fetchtaskSuccess = ( state, action ) => {
    return updateObject( state, {
        data: action.data,
        loading: false
    } );
};

const fetchtaskFail = ( state, action ) => {
    return updateObject( state, { error: true } );
};


const reducer = ( state = initialState, action ) => {
        console.log(action);
    switch ( action.type ) {
        case actionTypes.ADD_TASK_START:
            return taskInit( state, action );
        case actionTypes.ADD_TASK:
            return taskAdd( state, action );
        case actionTypes.ADD_TASK_FAILED:
            return taskAddedFalied( state, action );
        case actionTypes.ADD_TASK_SUCESS:
            return taskAddedSucces( state, action );

        case actionTypes.FETCH_TASK:
            return fetchtaskStart( state, action );
        case actionTypes.FETCH_TASK_FAILED:
            return fetchtaskFail( state, action );
        case actionTypes.FETCH_TASK_SUCESS:
            return fetchtaskSuccess( state, action );

        case actionTypes.EDIT_TASK:
            return {
                ...state,
                loading:true
            };
        case actionTypes.EDIT_TASK_FAILED:
            return {
                ...state,
                loading:false,
                error:true
            };
        case actionTypes.EDIT_TASK_SUCESS:
            return {
                ...state,
                name : action.name,
                date : action.date,
                id : action.id,
                loading:false,
                error:false,
                edited :true
            };

        case actionTypes.DELETE_TASK:
            return {
                ...state,
                loading:true
            };

        case actionTypes.DELETE_TASK_FAILED:
            return {
                ...state,
                loading:false,
                error:true
            };

        case actionTypes.DELETE_TASK_SUCESS:
            return {
                ...state,
                loading:false,
                error:false
            };

        case actionTypes.CHANGE_STATUS_TASK:
            return {
                ...state,
                loading:true
            };

        case actionTypes.CHANGE_STATUS_TASK_FAILED:
            return {
                ...state,
                loading:false,
                error:true
            };

        case actionTypes.CHANGE_STATUS_TASK_SUCESS:
            return {
                ...state,
                edited : true,
                loading:false,
                error:false
            };

        case actionTypes.FILTER_TASK:
            return {
                ...state,
                loading:true
            };
        case actionTypes.FILTER_TASK_FAILED:
             return {
                ...state,
                loading:false,
                error:true
            };
        case actionTypes.FILTER_TASK_SUCESS:
            return {
                ...state,
                loading:false,
                error : false,
                data : action.data
            };

        default: return state;
    }
}





export default reducer;
